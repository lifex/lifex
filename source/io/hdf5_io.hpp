/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_UTILS_HDF5_HPP_
#define LIFEX_UTILS_HDF5_HPP_

#include "source/core.hpp"

#include <deal.II/base/hdf5.h>

/**
 * @brief Utilities for reading and writing data to HDF5 files.
 */
namespace lifex::utils::hdf5
{
  /**
   * @brief Write a vector to an HDF5 file.
   *
   * The order in which the vector is written to file depends on its parallel
   * partitioning. Therefore, when reading the vector from file (see
   * @ref read_vector), one should always use the same parallel partitioning
   * that was used for writing.
   */
  void
  write_vector(HDF5::Group &group, const LinAlg::MPI::Vector &vector);

  /**
   * @brief Read a vector from an HDF5 file.
   *
   * @param[in] group The HDF5 group the vector should be read from.
   * @param[out] vector The vector that will be read from the HDF5 file. The
   * vector must be already initialized, and this function assumes that the
   * parallel partitioning is consistent with the one used for writing the HDF5
   * file.
   */
  void
  read_vector(const HDF5::Group &group, LinAlg::MPI::Vector &vector);

  /**
   * @brief Write a block vector to an HDF5 file.
   *
   * Calls the overload that takes a single vector in input on every block.
   */
  void
  write_vector(HDF5::Group &group, const LinAlg::MPI::BlockVector &vector);

  /**
   * @brief Read a block vector from an HDF5 file.
   *
   * Calls the overload that takes a single vector in input on every block.
   */
  void
  read_vector(const HDF5::Group &group, LinAlg::MPI::BlockVector &vector);

  /**
   * @brief Write a sparse matrix to an HDF5 file.
   *
   * This writes the contents of a sparse matrix to an HDF5 file, from which
   * the matrix can be retrieved using the function @ref read_matrix. The matrix
   * must be written and read using the same number of processes.
   *
   * This adds severak HDF5 datasets to the provided group. Since the names of
   * those datasets are the same for every serialized matrix, different matrices
   * should always be saved to different groups.
   *
   * @param[in] group The HDF5 group (of which HDF5::File is a derived class)
   * that the matrix should be written to.
   * @param[in] matrix The matrix to be written.
   */
  void
  write_matrix(HDF5::Group &group, const LinAlg::MPI::SparseMatrix &matrix);

  /**
   * @brief Read a sparse matrix from an HDF5 file.
   *
   * The matrix must be written and read using the same number of processes.
   *
   * @param[in] group The HDF5 group (of which HDF5::File is a derived class)
   * that the matrix should be read from.
   * @param[in] matrix The matrix to be read.
   * @param[in] owned_rows The indices of locally owned matrix rows.
   * @param[in] relevant_rows The indices of locally relevant matrix rows.
   * @param[in] owned_cols The indices of locally owned matrix columns.
   * @param[in] mpi_comm The MPI communicator used to distribute the matrix.
   */
  void
  read_matrix(const HDF5::Group         &group,
              LinAlg::MPI::SparseMatrix &matrix,
              const IndexSet            &owned_rows,
              const IndexSet            &relevant_rows,
              const IndexSet            &owned_cols,
              const MPI_Comm            &mpi_comm);
} // namespace lifex::utils::hdf5

#endif