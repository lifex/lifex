/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/io/restart_handler.hpp"

#include <deal.II/base/hdf5.h>

namespace lifex::utils
{
  RestartHandler::RestartHandler(const std::string &subsection_path,
                                 const std::string &default_basename_)
    : CoreModel(subsection_path)
    , default_basename(default_basename_)
  {}

  void
  RestartHandler::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);

    params.enter_subsection("Serialization");
    {
      params.declare_entry(
        "Enable",
        "false",
        Patterns::Bool(),
        "Toggle serialization of the solution for successive restart.");

      params.declare_entry(
        "Serialization basename",
        default_basename,
        Patterns::FileName(Patterns::FileName::FileType::output),
        "Basename for the output files. It will be suffixed with the timestep "
        "index and the appropriate extension.");

      params.declare_entry("Serialize every n timesteps",
                           "1",
                           Patterns::Integer(1),
                           "Only serialize every this many timesteps.");
    }
    params.leave_subsection();

    params.enter_subsection("Restart");
    {
      params.declare_entry("Enable",
                           "false",
                           Patterns::Bool(),
                           "Enable reloading the simulation state from "
                           "previously serialized files.");

      params.declare_entry(
        "Restart basename",
        default_basename,
        Patterns::FileName(Patterns::FileName::FileType::input),
        "Basename for the input files. It will be suffixed with the restart "
        "timestep index and the appropriate extension.");

      params.declare_entry("Restart timestep index",
                           "0",
                           Patterns::Integer(0),
                           "Timestep index to load when restarting.");
    }
    params.leave_subsection();

    params.leave_subsection_path();
  }

  void
  RestartHandler::parse_parameters(ParamHandler &params)
  {
    params.parse();

    params.enter_subsection_path(prm_subsection_path);

    params.enter_subsection("Serialization");
    {
      prm_enable_serialization   = params.get_bool("Enable");
      prm_serialization_basename = params.get("Serialization basename");
      prm_serialize_every_n_timesteps =
        params.get_integer("Serialize every n timesteps");
    }
    params.leave_subsection();

    params.enter_subsection("Restart");
    {
      prm_enable_restart           = params.get_bool("Enable");
      prm_deserialization_basename = params.get("Restart basename");
      prm_restart_timestep_index = params.get_integer("Restart timestep index");
    }
    params.leave_subsection();

    params.leave_subsection_path();
  }

  void
  RestartHandler::serialize(const double       &time,
                            const unsigned int &timestep_number) const
  {
    if (!prm_enable_serialization ||
        timestep_number % prm_serialize_every_n_timesteps != 0)
      return;

    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Serialize");

    // Open the output file.
    const std::string file_name =
      prm_output_directory + "/" + prm_serialization_basename + "_" +
      utils::timestep_to_string(timestep_number) + ".h5";
    HDF5::File file(file_name, HDF5::File::FileAccessMode::create, mpi_comm);

    // Metadata.
    {
      file.set_attribute("mpi_size", mpi_size);

      file.set_attribute("time", time);
      file.set_attribute("timestep_number", timestep_number);
    }

    // Scalars.
    for (unsigned int i = 0; i < attached_scalars.size(); ++i)
      {
        const std::string name  = "scalar_" + std::to_string(i);
        const auto       &value = attached_scalars[i];

        if (std::holds_alternative<double *>(value))
          file.set_attribute<double>(name, *std::get<double *>(value));
        else if (std::holds_alternative<int *>(value))
          file.set_attribute<int>(name, *std::get<int *>(value));
        else if (std::holds_alternative<unsigned int *>(value))
          file.set_attribute<unsigned int>(name,
                                           *std::get<unsigned int *>(value));
      }

    // Data vectors.
    for (unsigned int i = 0; i < attached_vectors.size(); ++i)
      {
        const std::string group_name = "vector_" + std::to_string(i);
        const auto       &vec        = attached_vectors[i];

        if (vec.has_type<LinAlg::MPI::Vector>())
          {
            utils::serialize(file,
                             group_name,
                             vec.get_owned<LinAlg::MPI::Vector>());
          }
        else if (vec.has_type<LinAlg::MPI::BlockVector>())
          {
            utils::serialize(file,
                             group_name,
                             vec.get_owned<LinAlg::MPI::BlockVector>());
          }
      }
  }

  void
  RestartHandler::deserialize()
  {
    // Open the input file.
    const std::string file_name =
      prm_deserialization_basename + "_" +
      utils::timestep_to_string(prm_restart_timestep_index) + ".h5";

    if (standalone)
      {
        pcout << utils::log::separator_section << std::endl;
        pcout << "Restarting simulation" << std::endl;
        pcout << "  File: " << file_name << std::endl;
      }

    assert_file_exists(file_name);
    HDF5::File file(file_name, HDF5::File::FileAccessMode::open, mpi_comm);

    // Metadata.
    {
      const unsigned int mpi_size_serialization =
        file.get_attribute<unsigned int>("mpi_size");

      AssertThrow(mpi_size == mpi_size_serialization,
                  ExcMessage("Serialization and restart must use the same "
                             "number of cores. Serialization used " +
                             std::to_string(mpi_size_serialization) +
                             " cores, restart is using " +
                             std::to_string(mpi_size) + "."));

      time            = file.get_attribute<double>("time");
      timestep_number = file.get_attribute<unsigned int>("timestep_number");
    }

    // The remanining data is only read in standalone mode.
    if (!standalone)
      return;

    pcout << "  Restart timestep: " << timestep_number << "\n"
          << "  Restart time:     " << time << std::endl;

    // Scalars.
    for (unsigned int i = 0; i < attached_scalars.size(); ++i)
      {
        const std::string name  = "scalar_" + std::to_string(i);
        const auto       &value = attached_scalars[i];

        if (std::holds_alternative<double *>(value))
          *std::get<double *>(value) = file.get_attribute<double>(name);
        else if (std::holds_alternative<int *>(value))
          *std::get<int *>(value) = file.get_attribute<int>(name);
        else if (std::holds_alternative<unsigned int *>(value))
          *std::get<unsigned int *>(value) =
            file.get_attribute<unsigned int>(name);
      }

    // Data vectors.
    for (unsigned int i = 0; i < attached_vectors.size(); ++i)
      {
        const std::string group_name = "vector_" + std::to_string(i);
        auto             &vec        = attached_vectors[i];

        if (vec.has_type<LinAlg::MPI::Vector>())
          {
            utils::deserialize(file,
                               group_name,
                               vec.get_owned<LinAlg::MPI::Vector>());
          }
        else if (vec.has_type<LinAlg::MPI::BlockVector>())
          {
            utils::deserialize(file,
                               group_name,
                               vec.get_owned<LinAlg::MPI::BlockVector>());
          }

        // Update ghosted vector, if necessary.
        vec.update_ghost();
      }

    if (standalone)
      pcout << utils::log::separator_section << std::endl;
  }

  std::pair<double, unsigned int>
  RestartHandler::restart()
  {
    if (prm_enable_restart)
      {
        deserialize();
        return std::make_pair(time, timestep_number);
      }

    return std::make_pair(0.0, 0);
  }

  void
  RestartHandler::inherit_parameters_from_parent(
    const utils::RestartHandler &other)
  {
    standalone = false;

    prm_enable_serialization        = other.prm_enable_serialization;
    prm_serialization_basename      = other.prm_serialization_basename;
    prm_serialize_every_n_timesteps = other.prm_serialize_every_n_timesteps;
    prm_enable_restart              = other.prm_enable_restart;
    prm_deserialization_basename    = other.prm_deserialization_basename;
    prm_restart_timestep_index      = other.prm_restart_timestep_index;
  }
} // namespace lifex::utils