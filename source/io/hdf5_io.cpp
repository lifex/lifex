/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/io/hdf5_io.hpp"

#include "source/numerics/tools.hpp"

#include <algorithm>
#include <vector>

namespace lifex::utils::hdf5
{
  namespace
  {
    unsigned int
    compute_partitioning_offset(const IndexSet &owned_indices,
                                const MPI_Comm &mpi_comm)
    {
      auto mpi_rank = Utilities::MPI::this_mpi_process(mpi_comm);

      const unsigned int              n_local = owned_indices.n_elements();
      const std::vector<unsigned int> n_global =
        Utilities::MPI::all_gather(mpi_comm, n_local);
      unsigned int offset = 0;

      for (unsigned int rank = 0; rank < mpi_rank; ++rank)
        offset += n_global[rank];

      return offset;
    }
  } // namespace

  void
  write_vector(HDF5::Group &group, const LinAlg::MPI::Vector &vector)
  {
    using value_type = LinAlg::MPI::Vector::value_type;

    auto mpi_comm = vector.get_mpi_communicator();
    auto mpi_rank = Utilities::MPI::this_mpi_process(mpi_comm);
    auto mpi_size = Utilities::MPI::n_mpi_processes(mpi_comm);

    const IndexSet owned_dofs = vector.locally_owned_elements();

    // Compute the offset for the local rank.
    const unsigned int n_local = owned_dofs.n_elements();
    const unsigned int offset =
      compute_partitioning_offset(owned_dofs, mpi_comm);

    // Convert the vector entries to an std::vector for writing.

    std::vector<value_type> vals(n_local);
    vector.extract_subvector_to(owned_dofs.get_index_vector(), vals);

    auto all_offsets = Utilities::MPI::gather(mpi_comm, offset, 0);
    auto all_vals    = Utilities::MPI::gather(mpi_comm, vals, 0);

    HDF5::DataSet vals_dataset =
      group.create_dataset<value_type>("vals", {vector.size()});

    if (mpi_rank == 0)
      {
        for (unsigned int rank = 0; rank < mpi_size; ++rank)
          vals_dataset.write_hyperslab(all_vals[rank],
                                       {all_offsets[rank]},
                                       {all_vals[rank].size()});
      }
    else
      {
        for (unsigned int rank = 0; rank < mpi_size; ++rank)
          vals_dataset.write_none<value_type>();
      }
  }

  void
  read_vector(const HDF5::Group &group, LinAlg::MPI::Vector &vector)
  {
    using value_type = LinAlg::MPI::Vector::value_type;

    auto           mpi_comm   = vector.get_mpi_communicator();
    const IndexSet owned_dofs = vector.locally_owned_elements();

    // Compute the offset for the local rank.
    const unsigned int n_local = owned_dofs.n_elements();
    const unsigned int offset =
      compute_partitioning_offset(owned_dofs, mpi_comm);

    HDF5::DataSet vals_dataset = group.open_dataset("vals");
    const auto    vals =
      vals_dataset.read_hyperslab<std::vector<value_type>>({offset}, {n_local});

    unsigned int i = 0;
    for (const auto &idx : owned_dofs)
      {
        set_vector_entry(vector, idx, vals[i]);
        ++i;
      }

    vector.compress(VectorOperation::insert);
  }

  void
  write_vector(HDF5::Group &group, const LinAlg::MPI::BlockVector &vector)
  {
    const unsigned int n_blocks = vector.n_blocks();

    for (unsigned int i = 0; i < n_blocks; ++i)
      {
        HDF5::Group subgroup = group.create_group("block_" + std::to_string(i));
        write_vector(subgroup, vector.block(i));
      }
  }

  /**
   * @brief Read a block vector from an HDF5 file.
   */
  void
  read_vector(const HDF5::Group &group, LinAlg::MPI::BlockVector &vector)
  {
    const unsigned int n_blocks = vector.n_blocks();

    for (unsigned int i = 0; i < n_blocks; ++i)
      {
        HDF5::Group subgroup = group.open_group("block_" + std::to_string(i));
        read_vector(subgroup, vector.block(i));
      }
  }

  void
  write_matrix(HDF5::Group &group, const LinAlg::MPI::SparseMatrix &matrix)
  {
    // We retrieve the number of locally-stored non-zero elements.
    unsigned int local_nnz = 0;
#if defined(LIN_ALG_TRILINOS)
    local_nnz = matrix.trilinos_matrix().NumMyNonzeros();
#elif defined(LIN_ALG_PETSC)
    MatInfo mat_info;

    // The PETScWrappers::SparseMatrix interface only allows accessing a
    // non-const reference to the underlying PETSc matrix. We cast away const
    // since we only read the local number of non-zeros.
    MatGetInfo(const_cast<LinAlg::MPI::SparseMatrix &>(matrix).petsc_matrix(),
               MAT_LOCAL,
               &mat_info);
    local_nnz                = static_cast<unsigned int>(mat_info.nz_used);
#else
    AssertThrow(false, ExcMessage("Unsupported linear algebra backend."));
#endif

    const unsigned int local_rows =
      matrix.locally_owned_range_indices().n_elements();

    std::vector<unsigned int> row_lengths(local_rows, 0);
    std::vector<unsigned int> cols(local_nnz, 0);
    std::vector<double>       vals(local_nnz, 0.0);

#if defined(LIN_ALG_TRILINOS)
    // If working with Trilinos, we build the CSR representation of the matrix
    // directly from the internal CSR data, instead of iterating through the
    // matrix with the deal.II interface. This is much (much!) faster, although
    // we might need to update it for future versions of Trilinos.

    int    *index_offsets;
    int    *indices;
    double *values;

    matrix.trilinos_matrix().ExtractCrsDataPointers(index_offsets,
                                                    indices,
                                                    values);

    for (unsigned int row = 0; row < local_rows; ++row)
      row_lengths[row] = index_offsets[row + 1] - index_offsets[row];

    cols.assign(indices, indices + local_nnz);
    vals.assign(values, values + local_nnz);

    for (auto &col : cols)
      col = matrix.trilinos_matrix().GCID(col);

#elif defined(LIN_ALG_PETSC)
    unsigned int current_nnz = 0; // Local index of the current non-zero entry.
    unsigned int current_row = 0; // Local index of the current row.

    for (const auto &row : matrix.locally_owned_range_indices())
      {
        row_lengths[current_row] = matrix.row_length(row);

        for (auto entry = matrix.begin(row); entry != matrix.end(row); ++entry)
          {
            cols[current_nnz] = entry->column();
            vals[current_nnz] = entry->value();

            ++current_nnz;
          }

        ++current_row;
      }
#endif

    auto mpi_comm = matrix.get_mpi_communicator();
    auto mpi_rank = Utilities::MPI::this_mpi_process(mpi_comm);
    auto mpi_size = Utilities::MPI::n_mpi_processes(mpi_comm);

    const std::vector<unsigned int> nnz =
      Utilities::MPI::all_gather(mpi_comm, local_nnz);
    const std::vector<unsigned int> n_rows =
      Utilities::MPI::all_gather(mpi_comm, local_rows);

    unsigned int offset_nnz = 0;
    unsigned int offset_row = 0;

    for (unsigned int rank = 0; rank < mpi_rank; ++rank)
      {
        offset_nnz += nnz[rank];
        offset_row += n_rows[rank];
      }

    const unsigned int global_nnz  = matrix.n_nonzero_elements();
    const unsigned int global_rows = matrix.m();

    HDF5::DataSet nnz_partitioning_dataset =
      group.create_dataset<unsigned int>("partitioning_nnz", {mpi_size});
    nnz_partitioning_dataset.write(nnz);

    HDF5::DataSet row_partitioning_dataset =
      group.create_dataset<unsigned int>("partitioning_rows", {mpi_size});
    row_partitioning_dataset.write(n_rows);

    HDF5::DataSet row_lengths_dataset =
      group.create_dataset<unsigned int>("row_lengths", {global_rows});
    row_lengths_dataset.write_hyperslab(row_lengths,
                                        {offset_row},
                                        {local_rows});

    HDF5::DataSet cols_dataset =
      group.create_dataset<unsigned int>("cols", {global_nnz});
    cols_dataset.write_hyperslab(cols, {offset_nnz}, {local_nnz});

    HDF5::DataSet vals_dataset =
      group.create_dataset<double>("vals", {global_nnz});
    vals_dataset.write_hyperslab(vals, {offset_nnz}, {local_nnz});
  }

  void
  read_matrix(const HDF5::Group         &group,
              LinAlg::MPI::SparseMatrix &matrix,
              const IndexSet            &owned_rows,
              const IndexSet            &relevant_rows,
              const IndexSet            &owned_cols,
              const MPI_Comm            &mpi_comm)
  {
    auto mpi_rank = Utilities::MPI::this_mpi_process(mpi_comm);

    HDF5::DataSet nnz_partitioning_dataset =
      group.open_dataset("partitioning_nnz");
    const std::vector<unsigned int> nnz =
      nnz_partitioning_dataset.read<std::vector<unsigned int>>();

    HDF5::DataSet row_partitioning_dataset =
      group.open_dataset("partitioning_rows");
    const std::vector<unsigned int> n_rows =
      row_partitioning_dataset.read<std::vector<unsigned int>>();

    const unsigned int local_nnz  = nnz[mpi_rank];
    const unsigned int local_rows = n_rows[mpi_rank];

    unsigned int offset_nnz = 0;
    unsigned int offset_row = 0;
    for (unsigned int rank = 0; rank < mpi_rank; ++rank)
      {
        offset_nnz += nnz[rank];
        offset_row += n_rows[rank];
      }

    HDF5::DataSet row_lengths_dataset = group.open_dataset("row_lengths");
    const auto    row_lengths =
      row_lengths_dataset.read_hyperslab<std::vector<unsigned int>>(
        {offset_row}, {local_rows});

    HDF5::DataSet cols_dataset = group.open_dataset("cols");
    const auto    cols =
      cols_dataset.read_hyperslab<std::vector<unsigned int>>({offset_nnz},
                                                             {local_nnz});

    HDF5::DataSet vals_dataset = group.open_dataset("vals");
    const auto    vals =
      vals_dataset.read_hyperslab<std::vector<double>>({offset_nnz},
                                                       {local_nnz});

    DynamicSparsityPattern dsp(owned_rows.size(),
                               owned_cols.size(),
                               owned_rows);

    for (unsigned int r = 0, i = 0; r < row_lengths.size(); ++r)
      {
        dsp.add_entries(owned_rows.nth_index_in_set(r),
                        cols.begin() + i,
                        cols.begin() + i + row_lengths[r]);

        i += row_lengths[r];
      }

    SparsityTools::distribute_sparsity_pattern(dsp,
                                               owned_rows,
                                               Core::mpi_comm,
                                               relevant_rows);

    matrix.reinit(owned_rows, owned_cols, dsp, mpi_comm);

#if defined(LIN_ALG_TRILINOS)
    // If working with Trilinos, we directly fill the underlying CSR vectors.
    // This is much (much!) faster than going through the matrix with deal.II's
    // interface, although we might need to update it for future versions of
    // Trilinos.

    int    *index_offsets;
    int    *indices;
    double *values;

    matrix.trilinos_matrix().ExtractCrsDataPointers(index_offsets,
                                                    indices,
                                                    values);

    std::copy(vals.begin(), vals.end(), values);
#else
    for (unsigned int r = 0, i = 0; r < row_lengths.size(); ++r)
      {
        matrix.set(owned_rows.nth_index_in_set(r),
                   row_lengths[r],
                   &cols[i],
                   &vals[i]);

        i += row_lengths[r];
      }
#endif

    matrix.compress(VectorOperation::insert);
  }
} // namespace lifex::utils::hdf5