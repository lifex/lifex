/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/io/serialization.hpp"
#include "source/io/vtk_importer.hpp"

namespace lifex::utils
{
  VTKImporter::VTKImporter(const std::string              &subsection,
                           const std::vector<std::string> &field_labels_)
    : CoreModel(subsection)
    , field_labels(field_labels_)
  {}

  void
  VTKImporter::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);

    params.declare_entry_selection("Input file format",
                                   "VTK",
                                   "VTK | Serialized file",
                                   "Format of the input file.");

    params.enter_subsection("VTK");
    {
      params.declare_entry("Filename",
                           "",
                           Patterns::FileName(
                             Patterns::FileName::FileType::input),
                           "Name of the input VTK file.");

      params.declare_entry(
        "Geometry scaling factor",
        "1.0",
        Patterns::Double(),
        "Scaling factor applied to the geometry of the imported file.");

      for (const auto &label : field_labels)
        {
          params.declare_entry(label + " array name",
                               "",
                               Patterns::Anything(),
                               "Name of the input array for " + label +
                                 " in the VTK file.");

          params.declare_entry(label + " scaling factor",
                               "1.0",
                               Patterns::Double(),
                               "Scaling factor of the imported array for " +
                                 label + ".");
        }

      params.declare_entry_selection("VTK data type",
                                     "UnstructuredGrid",
                                     "UnstructuredGrid | PolyData");

      params.declare_entry_selection("VTK array data type",
                                     "PointData",
                                     "PointData | CellData");

      params.declare_entry_selection(
        "Mode",
        "Closest point",
        "Closest point | Linear projection | Signed distance");

      params.enter_subsection("Serialization");
      {
        params.declare_entry(
          "Enable",
          "true",
          Patterns::Bool(),
          "Enable serialization of the imported array, for future reuse.");

        params.declare_entry("Serialization filename",
                             "",
                             Patterns::FileName(
                               Patterns::FileName::FileType::output),
                             "Basename of the serialized file.");
      }
      params.leave_subsection();
    }
    params.leave_subsection();

    params.enter_subsection("Serialized file");
    {
      params.declare_entry(
        "Deserialization filename",
        "",
        Patterns::FileName(Patterns::FileName::FileType::input),
        "Serialized file where to read the imported vector from.");
    }
    params.leave_subsection();

    params.leave_subsection_path();
  }

  void
  VTKImporter::parse_parameters(ParamHandler &params)
  {
    params.parse();

    params.enter_subsection_path(prm_subsection_path);

    const std::string input_format_str = params.get("Input file format");
    if (input_format_str == "VTK")
      prm_input_format = InputFormat::VTK;
    else // if (input_format_str == "Serialized file")
      prm_input_format = InputFormat::SerializedFile;

    if (prm_input_format == InputFormat::VTK)
      {
        params.enter_subsection("VTK");

        prm_vtk_filename = params.get("Filename");
        prm_geometry_scaling_factor =
          params.get_double("Geometry scaling factor");

        for (const auto &label : field_labels)
          {
            prm_vtk_arrayname[label] = params.get(label + " array name");
            prm_array_scaling_factor[label] =
              params.get_double(label + " scaling factor");
          }

        const std::string data_type_str = params.get("VTK data type");
        if (data_type_str == "UnstructuredGrid")
          prm_vtk_datatype = VTKDataType::UnstructuredGrid;
        else // if (data_type_str == "PolyData")
          prm_vtk_datatype = VTKDataType::PolyData;

        const std::string array_type_str = params.get("VTK array data type");
        if (array_type_str == "PointData")
          prm_vtk_arraydatatype = VTKArrayDataType::PointData;
        else // if (array_type_str == "CellData")
          prm_vtk_arraydatatype = VTKArrayDataType::CellData;

        const std::string mode_str = params.get("Mode");
        if (mode_str == "Closest point")
          prm_mode = VTKFunction::Mode::ClosestPointProjection;
        else if (mode_str == "Linear projection")
          prm_mode = VTKFunction::Mode::LinearProjection;
        else // if (mode_str == "Signed distance")
          prm_mode = VTKFunction::Mode::SignedDistance;

        params.enter_subsection("Serialization");
        {
          prm_enable_serialization   = params.get_bool("Enable");
          prm_serialization_filename = params.get("Serialization filename");
        }
        params.leave_subsection();

        params.leave_subsection();
      }
    else // if (prm_input_format == InputFormat::SerializedFile)
      {
        params.enter_subsection("Serialized file");
        prm_deserialization_filename = params.get("Deserialization filename");
        params.leave_subsection();
      }

    params.leave_subsection_path();
  }

  void
  VTKImporter::initialize(const MeshHandler<dim> &triangulation,
                          const DoFHandler<dim>  &dof_handler)
  {
    const IndexSet owned_dofs = dof_handler.locally_owned_dofs();

    for (const auto &label : field_labels)
      data_owned[label].reinit(owned_dofs, mpi_comm);

    // Data vectors with ghost elements (only needed for serialization).
    std::map<std::string, LinAlg::MPI::Vector> data;
    if (prm_enable_serialization)
      {
        const IndexSet relevant_dofs =
          DoFTools::extract_locally_relevant_dofs(dof_handler);

        for (const auto &label : field_labels)
          data[label].reinit(owned_dofs, relevant_dofs, mpi_comm);
      }

    if (prm_input_format == InputFormat::VTK)
      {
        const FiniteElement<dim> &fe = dof_handler.get_fe();

        for (const auto &label : field_labels)
          {
            VTKFunction vtk_function(
              prm_vtk_filename,
              prm_vtk_datatype,
              prm_array_scaling_factor.at(label),
              prm_geometry_scaling_factor,
              /* data_is_vectorial = */ fe.n_components() > 1);

            if (prm_mode == VTKFunction::Mode::ClosestPointProjection)
              {
                vtk_function.setup_as_closest_point_projection(
                  prm_vtk_arrayname.at(label), prm_vtk_arraydatatype);
              }
            else if (prm_mode == VTKFunction::Mode::LinearProjection)
              {
                vtk_function.setup_as_linear_projection(
                  prm_vtk_arrayname.at(label));
              }
            else // if (prm_mode == VTKFunction::Mode::SignedDistance)
              {
                vtk_function.setup_as_signed_distance();
              }

            VectorTools::interpolate(dof_handler,
                                     vtk_function,
                                     data_owned.at(label));

            if (prm_enable_serialization)
              data.at(label) = data_owned.at(label);
          }

        if (prm_enable_serialization)
          {
            std::vector<const LinAlg::MPI::Vector *> out_vectors;
            for (const auto &label : field_labels)
              out_vectors.push_back(&data.at(label));

            utils::serialize(prm_serialization_filename,
                             out_vectors,
                             triangulation,
                             dof_handler);
          }
      }
    else // if (prm_input_format == SerializedFile)
      {
        std::vector<LinAlg::MPI::Vector *> in_vectors;
        for (const auto &label : field_labels)
          in_vectors.push_back(&data_owned.at(label));

        utils::deserialize(prm_deserialization_filename,
                           in_vectors,
                           triangulation,
                           dof_handler);
      }
  }

  const LinAlg::MPI::Vector &
  VTKImporter::get_owned(const std::string &label) const
  {
    return data_owned.at(label);
  }
} // namespace lifex::utils