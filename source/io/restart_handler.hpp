/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_UTILS_RESTART_HANDLER_HPP_
#define LIFEX_UTILS_RESTART_HANDLER_HPP_

#include "source/core_model.hpp"

#include "source/io/data_writer.hpp"
#include "source/io/serialization.hpp"

#include "source/numerics/time_handler.hpp"

#include <map>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace lifex::utils
{
  /**
   * @brief Helper class to manage simulation restart.
   *
   * This class takes care of the serialization of one or more finite element
   * solutions to file, and of their subsequent deserialization when restarting.
   *
   * See the time-dependent @ref tutorials for examples of use.
   *
   * ## Use for standalone models
   *
   * Consider a class `A` (derived from `CoreModel`) implementing a solver for a
   * time-dependent PDE problem, uncoupled from other models. Restart can be
   * implemented with the following steps.
   *
   * 1. Add to the class `A` a member of type `RestartHandler`, say
   * `A::restart_handler`.
   *
   * 2. Declare and parse the parameters of `restart_handler` within
   * `A::declare_parameters` and `A::parse_parameters`.
   *
   * 3. At the end of the method `A::setup_system` (the method that takes care
   * of initializing the finite element spaces and the vectors needed by `A`),
   * call the methods `restart_handler.attach_*` to attach data to the restart
   * handler. The methods can be used to attach finite element vectors, block
   * vectors, scalars, and objects of type @ref BDFHandler.
   *
   * 4. When setting up the initial conditions for `A` (typically in a method
   * called `A::setup_initial_conditions`), add the lines
   *     @code{.cpp}
   *     if (restart_handler.restart_enabled())
   *       std::tie(time, timestep_number) = restart_handler.restart();
   *     @endcode
   * This will read the serialized data from file as specified by the restart
   * parameters, and will also read the initial time and timestep number
   * (returned as a pair by the `RestartHandler::restart` method).
   *
   * 5. Call the method `restart_handler.serialize(time, timestep_number)`
   * within the `A::output_serialize` method to write serialized data to file as
   * specified by serialization the parameters.
   *
   * **Important**: the `attach_*` methods take non-const references of the
   * attached data. These references are stored inside this class, and they are
   * used both for writing serialized data and for reading. Therefore, the
   * referenced objects must outlive the instance of `RestartHandler` to which
   * they are attached.
   *
   * This use of the class is exemplified in @ref lifex::tutorials::Tutorial02,
   * @ref lifex::tutorials::Tutorial04, @ref lifex::tutorials::Tutorial05,
   * @ref lifex::tutorials::Tutorial06 and @ref lifex::tutorials::Tutorial07_AD.
   *
   * ## Use for coupled models
   *
   * Assume that two classes `A` and `B` (both derived from `CoreModel`)
   * implement two solvers for time-dependent PDE problems. The two models are
   * coupled by introducing a third class `C`, owning an instance of `A` and one
   * of `B`. Assume that both classes `A` and `B` can be run both as standalone
   * (uncoupled) or coupled through `C`, and that both have a boolean member
   * `standalone` that distinguishes the two situations.
   *
   * In this situation, restart for `A`, `B` and `C` can be implemented with the
   * following steps.
   *
   * 1. Add a member of type `RestartHandler` to all three classes `A`, `B` and
   * `C` (named e.g. `restart_handler`).
   *
   * 2. Declare and parse the parameters of `restart_handler` within the methods
   * `{declare,parse}_parameters` of `A`, `B` and `C`. For `A` and `B`, the
   * parameters should only be declared and parsed if `standalone == true`.
   *
   * 3. Both `A` and `B` should expose a method to forward the restart
   * parameters (say, `inherit_restart_from_parent`), falling back onto
   * `RestartHandler::inherit_parameters_from_parent`). This makes sure that the
   * submodels know whether restart is enabled or not, and from what file
   * restart data should be read from. This method should be called after
   * parsing paramters in `C`.
   *
   * 4. Both `A` and `B` should declare a method
   * `attach_restart(utils::RestartHandler &)` that registers data to the given
   * restart handler.
   *
   * 5. At the end of the initialization of `C`, after having called both
   * `A::setup_system` and `B::setup_system`, call the methods
   * `A::attach_restart` and `B::attach_restart` to register data from the
   * submodels to the restart handler.
   *
   * 6. Within `C::setup_initial_conditions`, add the lines
   *     @code{.cpp}
   *     if (restart_handler.restart_enabled())
   *       std::tie(time, timestep_number) = restart_handler.restart();
   *     @endcode
   * This will read the serialized data for all submodels, as well as the
   * initial time and timestep number. Then, call the methods
   * `A::setup_initial_conditions` and `B::setup_initial_conditions`. Both these
   * methods should also call `restart_handler.restart()` to retrieve initial
   * time and timestep number.
   *
   * 7. Call the method `restart_handler.serialize(time, timestep_number)`
   * within the `C::output_serialize` method to write serialized data to file as
   * specified by serialization the parameters. There is no need to call
   * `A::output_serialize` or `B::output_serialize`.
   */
  class RestartHandler : public CoreModel
  {
  protected:
    /// Alias for a wrapper storing one of the supported vector types.
    using VectorVariant =
      std::variant<LinAlg::MPI::Vector, LinAlg::MPI::BlockVector>;

    /// Alias for a wrapper storing a pointer to one of the supported vector
    /// types.
    using VectorPtrVariant =
      std::variant<LinAlg::MPI::Vector *, LinAlg::MPI::BlockVector *>;

    /// Class representing a vector attached to a RestartHandler. Bundles the
    /// ghosted and non-ghosted versions of the vector.
    class AttachedVector
    {
    public:
      /// Constructor taking in input both the owned and ghosted vectors.
      template <class VectorType>
      AttachedVector(VectorType &vector_owned_, VectorType &vector_ghosted_)
        : has_ghost(true)
        , vector_owned(&vector_owned_)
        , vector_ghosted(&vector_ghosted_)
      {}

      /// Constructor taking in input only the owned vector.
      template <class VectorType>
      AttachedVector(VectorType &vector_owned_)
        : has_ghost(false)
        , vector_owned(&vector_owned_)
        , vector_ghosted(static_cast<VectorType *>(nullptr))
      {}

      /// @name Access the vectors.
      /// @{

      /// Retrieve the vector without ghost entries.
      template <class VectorType>
      VectorType &
      get_owned() const
      {
        AssertThrow(has_type<VectorType>(), ExcLifexInternalError());
        return *std::get<std::remove_const_t<VectorType> *>(vector_owned);
      }

      /// Update the vector with ghost entries, if present.
      void
      update_ghost() const
      {
        if (!has_ghost)
          return;

        if (has_type<LinAlg::MPI::Vector>())
          {
            *std::get<LinAlg::MPI::Vector *>(vector_ghosted) =
              *std::get<LinAlg::MPI::Vector *>(vector_owned);
          }
        else if (has_type<LinAlg::MPI::BlockVector>())
          {
            *std::get<LinAlg::MPI::BlockVector *>(vector_ghosted) =
              *std::get<LinAlg::MPI::BlockVector *>(vector_owned);
          }
      }

      /// @}

      /// @name Inspect the underlying vector type.
      /// @{

      /// Check the underlying vector type.
      template <class VectorType>
      bool
      has_type() const
      {
        return std::holds_alternative<std::remove_const_t<VectorType> *>(
          vector_owned);
      }

      /// @}

    protected:
      /// Flag indicating whether a ghost vector was supplied at construction.
      bool has_ghost;

      /// Pointer to the vector, without ghost elements.
      VectorPtrVariant vector_owned;

      /// Pointer to the vector, including ghost elements.
      VectorPtrVariant vector_ghosted;
    };

    /// Alias for a wrapper storing one of the supported scalar types.
    using ScalarVariant = std::variant<double, unsigned int>;

    /// Alias for a wrapper storing a pointer to one of the supported scalar
    /// types.
    using ScalarPtrVariant = std::variant<double *, int *, unsigned int *>;

  public:
    /**
     * @brief Constructor.
     */
    RestartHandler(const std::string &subsection_path,
                   const std::string &default_basename_ = "restart");

    /**
     * @brief Declare parameters.
     */
    void
    declare_parameters(ParamHandler &params) const override;

    /**
     * @brief Parse parameters.
     */
    void
    parse_parameters(ParamHandler &params) override;

    /**
     * @brief Attach a vector for serialization/deserialization.
     *
     * When deserializing, the ghosted vector will be updated with the values
     * read into the owned vector.
     */
    template <class VectorType>
    void
    attach_vector(VectorType &vector_owned, VectorType &vector_ghosted)
    {
      static_assert(std::is_constructible_v<VectorVariant, VectorType>,
                    "Unsupported vector type.");

      attached_vectors.emplace_back(vector_owned, vector_ghosted);
    }

    /**
     * @brief Attach a vector for serialization/deserialization.
     */
    template <class VectorType>
    void
    attach_vector(VectorType &vector_owned)
    {
      static_assert(std::is_constructible_v<VectorVariant, VectorType>,
                    "Unsupported vector type.");

      attached_vectors.emplace_back(vector_owned);
    }

    /**
     * @brief Attach a BDF handler.
     */
    template <class VectorType>
    void
    attach_bdf_handler(utils::BDFHandler<VectorType> &bdf_handler)
    {
      static_assert(std::is_constructible_v<VectorVariant, VectorType>,
                    "Unsupported vector type.");

      for (auto &v : bdf_handler.get_solutions())
        attach_vector(v);
    }

    /**
     * @brief Attach a named scalar value.
     */
    template <class ScalarType>
    void
    attach_scalar(ScalarType &scalar)
    {
      static_assert(std::is_constructible_v<ScalarVariant, ScalarType>,
                    "Unsupported scalar type.");

      attached_scalars.emplace_back(&scalar);
    }

    /**
     * @brief Serialize the registered data to file.
     */
    void
    serialize(const double &time, const unsigned int &timestep_number) const;

    /**
     * @brief Restart.
     *
     * @return A pair of the restart time and timestep number.
     */
    std::pair<double, unsigned int>
    restart();

    /**
     * @brief Forward serialization and restart settings from parent.
     *
     * This method should be used when a parent physical model is responsible
     * for managing serialization and restart for one or more sub-models in a
     * centralized way, and the settings of the parent model should be forwarded
     * to the sub-models.
     */
    void
    inherit_parameters_from_parent(const utils::RestartHandler &other);

    /**
     * @brief Get whether restart is enabled.
     */
    bool
    restart_enabled() const
    {
      return prm_enable_restart;
    }

  protected:
    /**
     * @brief Deserialize the registered data from file.
     */
    void
    deserialize();

    /// Standalone flag.
    bool standalone = true;

    /// Default filename.
    std::string default_basename;

    /// Attached vectors.
    std::vector<AttachedVector> attached_vectors;

    /// Attached scalars.
    std::vector<ScalarPtrVariant> attached_scalars;

    /// Current time.
    double time;

    /// Current timestep number.
    unsigned int timestep_number;

    /// @name Parameters read from file.
    /// @{

    /// Toggle serialization.
    bool prm_enable_serialization;

    /// File basename for serialization.
    std::string prm_serialization_basename;

    /// Serialized files will be written only every this many timesteps.
    unsigned int prm_serialize_every_n_timesteps;

    /// Toggle restart.
    bool prm_enable_restart;

    /// File basename for deserialization.
    std::string prm_deserialization_basename;

    /// Index of the timestep to restart from.
    unsigned int prm_restart_timestep_index;

    /// @}
  };
} // namespace lifex::utils

#endif