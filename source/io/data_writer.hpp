/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 */

#ifndef LIFEX_UTILS_DATA_WRITER_HPP_
#define LIFEX_UTILS_DATA_WRITER_HPP_

#include "source/core_model.hpp"

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/data_out_faces.h>

#include <memory>
#include <string>
#include <vector>

namespace lifex::utils
{
  /// Convert timestep index to a string of length
  /// @ref Core::output_n_digits, with leading zeros.
  inline std::string
  timestep_to_string(const unsigned int &timestep)
  {
    return Utilities::int_to_string(timestep, Core::output_n_digits);
  }

  /**
   * @brief Write the content of a DataOut object in the HDF5/XDMF format.
   *
   * @param[in] data_out The object whose content is to be written.
   * @param[in] basename The basename of the output file, relative to the output
   * directory and without extension. The file will be suffixed with the
   * extensions .h5 and .xdmf when writing the HDF5 and XDMF files.
   * @param[in] filter_duplicate_vertices Filter duplicate vertices and
   * associated values. To be disabled if cellwise data are exported.
   */
  template <class DataOutType>
  void
  dataout_write_hdf5(const DataOutType &data_out,
                     const std::string &basename,
                     const bool        &filter_duplicate_vertices = true);

  /**
   * @brief Write the content of a DataOut object in HDF5/XDMF format.
   *
   * @param[in] data_out The object whose content is to be written.
   * @param[in] basename The basename of the output file, relative to the output
   * directory and without extension. The file will be suffixed with the
   * timestep index and with the extensions .h5 and .xdmf when writing the HDF5
   * and XDMF files.
   * @param[in] time_step The index of the timestep to be saved.
   * @param[in] time_step_mesh The index of the timeste where the mesh is saved.
   * If equal to time_step, the mesh will be saved in the output file.
   * @param[in] time The time to be stored in the XDMF entry.
   * @param[in] filter_duplicate_vertices Filter duplicate vertices and
   * associated values. To be disabled if cellwise data are exported.
   */
  template <class DataOutType>
  void
  dataout_write_hdf5(const DataOutType  &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const unsigned int &time_step_mesh,
                     const double       &time,
                     const bool         &filter_duplicate_vertices = true);

  /**
   * @brief Write the content of a DataOut object in PVTU format.
   *
   * @param[in] data_out The object whose content is to be written.This
   * argument is not const since we need to call data_out.set_flags().
   * @param[in] basename The basename of the output file, relative to the output
   * directory and without extension. The file will be suffixed with the .vtu
   * and .pvtu extensions.
   * @param[in] high_order If true, enables the output of high order (more than
   * linear) data.
   */
  template <class DataOutType>
  void
  dataout_write_pvtu(DataOutType       &data_out,
                     const std::string &basename,
                     const bool        &high_order = false);

  /**
   * @brief Write the content of a DataOut object in PVTU format.
   *
   * @param[in] data_out The object whose content is to be written. This
   * argument is not const since we need to call data_out.set_flags().
   * @param[in] basename The basename of the output file, relative to the output
   * directory and without extension. The file will be suffixed with the .vtu
   * and .pvtu extensions.
   * @param[in] time_step The index of the timestep to be saved.
   * @param[in] time The time to be stored in the VTU files.
   * @param[in] high_order If true, enables the output of high order (more than
   * linear) data.
   */
  template <class DataOutType>
  void
  dataout_write_pvtu(DataOutType        &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const double       &time,
                     const bool         &high_order = false);

  /**
   * @brief Handler class to manage data output.
   *
   * Provides a standardized interface for data output to physical models,
   * offering several options to control the frequency, type and format of the
   * output and managing the associated parameters.
   *
   * The template argument <kbd>DataOutType</kbd> must be one of
   * <kbd>DataOut<dim></kbd> and <kbd>DataOutFaces<dim></kbd>, corresponding to
   * volumetric and boundary output respectively. The two aliases
   * @ref OutputHandler and @ref OutputHandlerFaces are provided for
   * conciseness.
   *
   * See the @ref tutorials for basic examples of use.
   *
   * ## Available options
   *
   * #### Output filename
   *
   * The output basename can be specified in the parameter file by setting the
   * value of <kbd>Output basename</kbd>. For time-dependent output (enabled
   * with the corresponding constructor argument), the basename will be suffixed
   * with the timestep index (padded with zeros to six digits). Then, it will be
   * suffixed with the appropriate extension.
   *
   * Optionally, the basename can be suffixed with a custom string, provided
   * through @ref set_output_filename_suffix. This is meant for situations when
   * multiple files must be written using the same configuration (and thus using
   * the same instance of OutputHandler).
   *
   * The output filename will thus have the format
   * <kbd>\<basename\>\<suffix\>_\<timestep index\>.\<extension\></kbd>.
   *
   * #### Output frequency and initial output time
   *
   * For time-dependent simulations, one does not usually need to output data
   * for all time steps, mostly to avoid storing large datasets. This class
   * allows to start writing results only from a user-specified time, by setting
   * the value of <kbd>Start saving from time</kbd>, and to save only every few
   * timesteps, by setting the value of <kbd>Save output every n
   * timesteps</kbd>.
   *
   * For these to work correctly, the user class (i.e. the class that uses
   * <kbd>OutputHandlerBase</kbd> must:
   * 1. provide the current time and timestep number before doing output,
   * through the method @ref set_time;
   * 2. check that the output is active at current time, through the method
   * @ref is_active, before calling @ref write.
   *
   * See e.g. @ref tutorials::Tutorial02 for a basic example.
   *
   * #### Output format
   *
   * The class supports writing output in XDMF/HDF5 and VTU/PVTU formats, as
   * configured by the <kbd>Output format</kbd> parameter.
   *
   * The former is generally preferrable, as it produces significantly smaller
   * files than PVTU/VTU. However, it relies on parallel MPI I/O, which has been
   * reported to cause issues on some (few) systems.
   *
   * The alternative option, VTU/PVTU, causes every parallel process to write
   * its own VTU file (which is suffixed with the MPI rank), and an additional
   * PVTU file is written that links them all together (and can be opened e.g.
   * in ParaView). This format requires much less communication, and can be a
   * valid workaround to issues of slow HDF5 output.
   *
   * #### HDF5-specific options
   *
   * To reduce output file size for time-dependent simulations, by default, only
   * the first exported file contains the information about the mesh, and the
   * subsequent ones just refer to the first file for that. This behavior may be
   * undesirable, e.g. if the mesh changes over time and the user wants the
   * output to track that, or if the user does not want output files to depend
   * on one another. This behavior can be turned on by setting <kbd>HDF5 format
   * / Export mesh at every output = true</kbd>.
   *
   * Furthermore, by default, the output data is filtered to remove duplicate
   * vertices (i.e. to only write once vertices that are shared by multiple
   * cells). This has the effect of considerably reducing the output file size.
   * However, it also prevents from correctly visualizing data that is
   * discontinuous across cells. This behavior can be disabled by setting
   * <kbd>HDF5 format / Filter duplicate vertices = false</kbd>.
   *
   * #### PVTU specific options
   *
   * VTU files support high-order output. Although disabled by default, it can
   * be turned on by setting <kbd>PVTU format / High-order output = true</kbd>.
   */
  template <class DoFHandlerType,
            class DataOutType = DataOut<DoFHandlerType::dimension,
                                        DoFHandlerType::space_dimension>>
  class OutputHandlerBase : public CoreModel
  {
  public:
    /**
     * @brief Enumeration of supported output formats.
     */
    enum class OutputFormat
    {
      /**
       * @brief HDF5/XDMF format.
       *
       * This format will create a pair of files with extensions .h5 and .xdmf.
       * The former contains the data (and possibly the mesh), whereas the
       * latter contains metadata and links to the h5 file(s). If using the
       * visualization software ParaView, the .xdmf file is the one to open.
       */
      HDF5,

      /**
       * @brief Parallel VTU format.
       *
       * This format will write one file with extension .pvtu, and a set of
       * files with extensions .vtu. One .vtu file will be created per process.
       */
      PVTU
    };

    /**
     * @brief Constructor.
     *
     * @param[in] subsection_path Parameter subsection path.
     * @param[in] enable_time_dependent_ Set to true if the data written through
     * this class is time dependent. If so, the output file names will be
     * suffixed with the timestep index, which should be updated through
     * @ref set_time.
     * @param[in] standalone_ If set to false, this class only declares and
     * parses the enable/disable output flag.
     * @param[in] default_filename_ This string will be used as the default
     * output basename when generating parameters.
     * @param[in] default_enable_output_ This flag determines the default value
     * of the "Enable output" parameter.
     */
    OutputHandlerBase(const std::string &subsection_path,
                      const bool        &enable_time_dependent_,
                      const bool        &standalone_            = true,
                      const std::string &default_filename_      = "solution",
                      const bool        &default_enable_output_ = true);

    /**
     * @brief Declare parameters.
     */
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /**
     * @brief Parse parameters.
     */
    virtual void
    parse_parameters(ParamHandler &params) override;

    /**
     * @brief Forward output settings from parent.
     *
     * This method should be used when a parent physical model is responsible
     * for gathering the output of one or more sub-models, and the output
     * settings of the parent model should be forwarded to the sub-models.
     *
     * After calling this method, the configuration of the current OutputHandler
     * is copied from that of the one provided as input, except for the
     * enable/disable output flag. This allows to selectively toggle the output
     * of individual sub-models.
     *
     * @param[in] other OutputHandler whose parameters need to be copied into
     * the current instance.
     * @param[in] basename_suffix Optional suffix to be appended to the basename
     * of the current instance after copying it from that ot the other
     * OutputHandler.
     */
    void
    inherit_parameters_from_parent(
      const OutputHandlerBase<DoFHandlerType, DataOutType> &other,
      const std::string &basename_suffix = "");

    /// @name Time-dependent output.
    /// @{

    /**
     * @brief Set time and timestep number for writing time-dependent output.
     */
    void
    set_time(const double &time_, const unsigned int &timestep_number_);

    /// @}

    /// @name 3D output.
    /// @{

    /**
     * @brief Check if 3D output is active, possibly accounting for current time.
     */
    bool
    is_active() const;

    /**
     * @brief Check if 3D output is active.
     */
    bool
    is_enabled() const
    {
      return prm_enable_output;
    }

    /**
     * @brief Get the underlying DataOutType instance.
     */
    DataOutType &
    data_out() const
    {
      Assert(standalone, ExcStandaloneOnly());

      return *data_out_;
    }

    /**
     * @brief Add a data vector for 3D output.
     *
     * The data component interpretation is deduced from the DoF handler: if the
     * underlying finite element space has only one component, then the data is
     * assumed to be scalar. Otherwise, it is assumed to be a vector.
     *
     * For more control over the variable names and data component
     * interpretation, you can use one of the overloads of
     * data_out().add_data_vector().
     */
    template <class VectorType>
    void
    add_data_vector(const DoFHandlerType &dof_handler,
                    const VectorType     &vector,
                    const std::string    &solution_name) const
    {
      Assert(standalone, ExcStandaloneOnly());

      const unsigned int n_components = dof_handler.get_fe().n_components();

      const std::vector<std::string> solution_names(n_components,
                                                    solution_name);
      const std::vector<
        DataComponentInterpretation::DataComponentInterpretation>
        data_component_interpretation(
          n_components,
          n_components == 1 ?
            DataComponentInterpretation::component_is_scalar :
            DataComponentInterpretation::component_is_part_of_vector);

      data_out_->add_data_vector(dof_handler,
                                 vector,
                                 solution_names,
                                 data_component_interpretation);
    }

    /**
     * @brief Write the 3D output to file.
     *
     * Notice that, for the sake of memory occupation, this method clears the
     * internal data storing the output. Therefore, immediately calling this
     * method again will write no data.
     */
    void
    write(const Mapping<DoFHandlerType::dimension,
                        DoFHandlerType::space_dimension> &mapping,
          const unsigned int &n_subdivisions = 0) const
    {
      Assert(standalone, ExcStandaloneOnly());

      if constexpr (std::is_same_v<DataOutType, DataOut<dim>>)
        {
          data_out_->build_patches(mapping,
                                   n_subdivisions,
                                   DataOut<dim>::curved_inner_cells);
        }
      else // if constexpr (std::is_same_v<DataOutType, DataOutFaces<dim>>)
        {
          data_out_->build_patches(mapping, n_subdivisions);
        }

      write_internal();
    }

    /**
     * @brief Write the 3D output to file.
     *
     * This does the same as the method above, except that it assumes the
     * default linear mapping.
     */
    void
    write(const unsigned int &n_subdivisions = 0) const;

    /// @}

    /// @name Getters.
    /// @{

    /**
     * @brief Get the output basename.
     */
    const std::string &
    get_output_basename() const
    {
      return prm_output_basename;
    }

    /**
     * @brief Get the flag to export mesh at every output.
     */
    bool
    get_mesh_at_every_output() const
    {
      return prm_hdf5_mesh_at_every_output;
    }

    /**
     * @brief Get the frequency of output.
     */
    unsigned int
    get_output_every_n_timesteps() const
    {
      Assert(enable_time_dependent,
             ExcMessage("This method requires time-dependent output."));

      return prm_output_every_n_timesteps;
    }

    /// @}

    /// @name Setters.
    /// @{

    /**
     * @brief Set the output basename.
     */
    void
    set_output_basename(const std::string &basename_)
    {
      prm_output_basename = basename_;
    }

    /**
     * @brief Set the output filename suffix.
     */
    void
    set_output_filename_suffix(const std::string &suffix_)
    {
      filename_suffix = suffix_;
    }

    /**
     * @brief Set the flag to export mesh at every output.
     */
    void
    set_mesh_at_every_output(const bool &mesh_at_every_output)
    {
      prm_hdf5_mesh_at_every_output = mesh_at_every_output;
    }

    /// @}

  protected:
    /// Standalone flag.
    const bool standalone;

    /// @name Time-dependent output.
    /// @{

    /// Toggle time-dependent output.
    const bool enable_time_dependent;

    /// Output time.
    double time;

    /// Output timestep number.
    unsigned int timestep_number;

    /// Timestep at which the first output occurred. An empty value means that
    /// no output has happened yet.
    mutable std::optional<unsigned int> first_output_timestep = {};

    /// @}

    /// @name 3D output.
    /// @{

    /// Default filename for 3D output.
    const std::string default_filename;

    /// Default flag enabling/disabling 3D output.
    const bool default_enable_output;

    /// Output filename suffix. It is appendend to the parameter-defined
    /// filename, before possible timestep counters.
    std::string filename_suffix = "";

    /// Data writer instance.
    mutable std::unique_ptr<DataOutType> data_out_;

    /// @}

    /// @name Parameters read from file.
    /// @{

    /// Output basename.
    std::string prm_output_basename = "";

    /// Toggle output.
    bool prm_enable_output = true;

    /// Save output every n timesteps.
    unsigned int prm_output_every_n_timesteps = 1;

    /// Start saving output after a given time.
    double prm_output_from_time = 0.0;

    /// Output format.
    OutputFormat prm_output_format = OutputFormat::HDF5;

    /**
     * @brief Toggle exporting the mesh at every output for the HDF5 output of
     * time-dependent problems.
     *
     * If the mesh deforms over time, enabling this flag means that the 3D
     * output is saved in the deformed configuration, and every HDF5 file
     * contains the mesh. Conversely, setting this flag to false means that only
     * the first file will contain the mesh, and all subsequent files will refer
     * to the mesh saved in the initial output. Therefore, the mesh displacement
     * will not be visible in the output.
     *
     * The flag is false by default, sice that is the most reasonable choice for
     * problems where the mesh does not move.
     */
    bool prm_hdf5_mesh_at_every_output = false;

    /**
     * @brief Toggle filtering duplicate vertices in HDF5 output.
     *
     * If enabled, output file size will be made smaller by removing duplicated
     * vertices across cells. The downside is that it will not be possible to
     * represent cell data anymore, and any cell data will be converted to point
     * data.
     */
    bool prm_hdf5_filter_duplicate_vertices = true;

    /**
     * @brief Toggle high-order output for PVTU files.
     */
    bool prm_pvtu_high_order_output = false;

    /// @}

  private:
    /**
     * @brief Internal method for writing 3D data to file.
     */
    void
    write_internal() const;
  };

  /// Output handler for volumetric output.
  using OutputHandler = OutputHandlerBase<DoFHandler<dim>>;

  /// Output handler for 1D output.
  template <int spacedim = dim>
  using OutputHandler1D = OutputHandlerBase<DoFHandler<1, spacedim>>;

  /// Output handler for boundary output.
  using OutputHandlerFaces =
    OutputHandlerBase<DoFHandler<dim>, DataOutFaces<dim>>;
} // namespace lifex::utils

#endif /* LIFEX_UTILS_DATA_WRITER_HPP_ */
