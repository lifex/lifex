/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 */

#include "source/io/data_writer.hpp"

#include "source/numerics/numbers.hpp"

#include <vector>

namespace lifex::utils
{
  template <class DataOutType>
  void
  dataout_write_hdf5(const DataOutType &data_out,
                     const std::string &basename,
                     const bool        &filter_duplicate_vertices)
  {
    const std::string filename_h5   = basename + ".h5";
    const std::string filename_xdmf = basename + ".xdmf";

    DataOutBase::DataOutFilter data_filter(
      DataOutBase::DataOutFilterFlags(filter_duplicate_vertices, true));

    data_out.write_filtered_data(data_filter);
    data_out.write_hdf5_parallel(data_filter,
                                 Core::prm_output_directory + filename_h5,
                                 Core::mpi_comm);

    std::vector<XDMFEntry> xdmf_entries({data_out.create_xdmf_entry(
      data_filter, filename_h5, 0.0, Core::mpi_comm)});
    data_out.write_xdmf_file(xdmf_entries,
                             Core::prm_output_directory + filename_xdmf,
                             Core::mpi_comm);
  }

  template <class DataOutType>
  void
  dataout_write_hdf5(const DataOutType  &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const unsigned int &time_step_mesh,
                     const double       &time,
                     const bool         &filter_duplicate_vertices)
  {
    const bool export_mesh = (time_step == time_step_mesh);

    const std::string filename_h5 =
      basename + "_" + utils::timestep_to_string(time_step) + ".h5";
    const std::string filename_xdmf =
      basename + "_" + utils::timestep_to_string(time_step) + ".xdmf";
    const std::string filename_mesh =
      basename + "_" + utils::timestep_to_string(time_step_mesh) + ".h5";

    DataOutBase::DataOutFilter data_filter(
      DataOutBase::DataOutFilterFlags(filter_duplicate_vertices, true));

    data_out.write_filtered_data(data_filter);
    data_out.write_hdf5_parallel(data_filter,
                                 export_mesh,
                                 Core::prm_output_directory + filename_mesh,
                                 Core::prm_output_directory + filename_h5,
                                 Core::mpi_comm);

    std::vector<XDMFEntry> xdmf_entries({data_out.create_xdmf_entry(
      data_filter, filename_mesh, filename_h5, time, Core::mpi_comm)});

    data_out.write_xdmf_file(xdmf_entries,
                             Core::prm_output_directory + filename_xdmf,
                             Core::mpi_comm);
  }

  template <class DataOutType>
  void
  dataout_write_pvtu(DataOutType       &data_out,
                     const std::string &basename,
                     const bool        &high_order)
  {
    DataOutBase::VtkFlags vtk_flags;
    vtk_flags.write_higher_order_cells = high_order;
    data_out.set_flags(vtk_flags);

    data_out.write_vtu_with_pvtu_record(Core::prm_output_directory,
                                        basename,
                                        0,
                                        Core::mpi_comm,
                                        Core::output_n_digits);
  }

  template <class DataOutType>
  void
  dataout_write_pvtu(DataOutType        &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const double       &time,
                     const bool         &high_order)
  {
    DataOutBase::VtkFlags vtk_flags;
    vtk_flags.time                     = time;
    vtk_flags.write_higher_order_cells = high_order;
    data_out.set_flags(vtk_flags);

    data_out.write_vtu_with_pvtu_record(Core::prm_output_directory,
                                        basename,
                                        time_step,
                                        Core::mpi_comm,
                                        Core::output_n_digits);
  }

  /// @cond DOXYGEN_SKIP

  template void
  dataout_write_hdf5(const DataOut<dim> &data_out,
                     const std::string  &basename,
                     const bool         &filter_duplicate_vertices);
  template void
  dataout_write_hdf5(const DataOut<dim> &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const unsigned int &time_step_mesh,
                     const double       &time,
                     const bool         &filter_duplicate_vertices);

  template void
  dataout_write_hdf5(const DataOutFaces<dim> &data_out,
                     const std::string       &basename,
                     const bool              &filter_duplicate_vertices);

  template void
  dataout_write_hdf5(const DataOutFaces<dim> &data_out,
                     const std::string       &basename,
                     const unsigned int      &time_step,
                     const unsigned int      &time_step_mesh,
                     const double            &time,
                     const bool              &filter_duplicate_vertices);

  template void
  dataout_write_pvtu(DataOut<dim>      &data_out,
                     const std::string &basename,
                     const bool        &high_order);

  template void
  dataout_write_pvtu(DataOutFaces<dim> &data_out,
                     const std::string &basename,
                     const bool        &high_order);

  template void
  dataout_write_pvtu(DataOut<dim>       &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const double       &time,
                     const bool         &high_order);

  template void
  dataout_write_pvtu(DataOutFaces<dim>  &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const double       &time,
                     const bool         &high_order);

#if LIFEX_DIM > 2
  template void
  dataout_write_hdf5(const DataOut<2, dim> &data_out,
                     const std::string     &basename,
                     const unsigned int    &time_step,
                     const unsigned int    &time_step_mesh,
                     const double          &time,
                     const bool            &filter_duplicate_vertices);

  template void
  dataout_write_pvtu(DataOut<2, dim>   &data_out,
                     const std::string &basename,
                     const bool        &high_order);
  template void
  dataout_write_pvtu(DataOut<2, dim>    &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const double       &time,
                     const bool         &high_order);
#endif

#if LIFEX_DIM > 1
  template void
  dataout_write_hdf5(const DataOut<1, dim> &data_out,
                     const std::string     &basename,
                     const bool            &filter_duplicate_vertices);
  template void
  dataout_write_hdf5(const DataOut<1, dim> &data_out,
                     const std::string     &basename,
                     const unsigned int    &time_step,
                     const unsigned int    &time_step_mesh,
                     const double          &time,
                     const bool            &filter_duplicate_vertices);

  template void
  dataout_write_pvtu(DataOut<1, dim>   &data_out,
                     const std::string &basename,
                     const bool        &high_order);
  template void
  dataout_write_pvtu(DataOut<1, dim>    &data_out,
                     const std::string  &basename,
                     const unsigned int &time_step,
                     const double       &time,
                     const bool         &high_order);
#endif

  /// @endcond

  template <class DoFHandlerType, class DataOutType>
  OutputHandlerBase<DoFHandlerType, DataOutType>::OutputHandlerBase(
    const std::string &subsection_path,
    const bool        &enable_time_dependent_,
    const bool        &standalone_,
    const std::string &default_filename_,
    const bool        &default_enable_output_)
    : CoreModel(subsection_path)
    , standalone(standalone_)
    , enable_time_dependent(enable_time_dependent_)
    , time(0.0)
    , timestep_number(0)
    , default_filename(default_filename_)
    , default_enable_output(default_enable_output_)
    , prm_output_basename(default_filename_)
  {
    data_out_ = std::make_unique<DataOutType>();
  }

  template <class DoFHandlerType, class DataOutType>
  void
  OutputHandlerBase<DoFHandlerType, DataOutType>::declare_parameters(
    ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);

    params.declare_entry("Enable output",
                         default_enable_output ? "true" : "false",
                         Patterns::Bool(),
                         "Toggle output.");

    if (standalone)
      {
        params.declare_entry(
          "Output basename",
          default_filename,
          Patterns::FileName(Patterns::FileName::FileType::output),
          "Basename for the output files. It will be suffixed with " +
            (enable_time_dependent ? std::string("the timestep index and ") :
                                     std::string("")) +
            "the appropriate extension.");

        if (enable_time_dependent)
          {
            params.declare_entry(
              "Save output every n timesteps",
              "1",
              Patterns::Integer(1),
              "3D output will be saved only every n timesteps.");

            params.declare_entry(
              "Start saving from time",
              "0",
              Patterns::Double(),
              "Save results saving from a user-specified time [s].");
          }

        params.set_verbosity(VerbosityParam::Full);
        params.declare_entry_selection(
          "Output format",
          "HDF5",
          "HDF5 | PVTU",
          "Select the output format. The HDF5 output is generally preferable "
          "in terms of memory occupation. However, it may be slow on some "
          "systems due to issues with MPI I/O. In that case, parallel VTU "
          "(PVTU) output may be a valid workaround.");

        params.enter_subsection("HDF5 format");
        {
          if (enable_time_dependent)
            {
              params.declare_entry(
                "Export mesh at every output",
                "false",
                Patterns::Bool(),
                "For HDF5 output, this flag determines whether the mesh is "
                "saved "
                "in every file or only in the first one. It is advisable to "
                "set "
                "this flag to false if the mesh does not displace over time.");
            }

          params.declare_entry(
            "Filter duplicate vertices",
            "true",
            Patterns::Bool(),
            "If true, reduces the file size by eliminating duplicate "
            "vertices across cells. Notice that this prevents from "
            "representing data that is discontinuous across cells.");
        }
        params.leave_subsection();

        params.enter_subsection("PVTU format");
        {
          params.declare_entry("High-order output",
                               "false",
                               Patterns::Bool(),
                               "Enables high-order output.");
        }
        params.leave_subsection();

        params.reset_verbosity();
      }

    params.leave_subsection_path();
  }

  template <class DoFHandlerType, class DataOutType>
  void
  OutputHandlerBase<DoFHandlerType, DataOutType>::parse_parameters(
    ParamHandler &params)
  {
    params.parse();

    params.enter_subsection_path(prm_subsection_path);

    prm_enable_output = params.get_bool("Enable output");

    if (standalone)
      {
        prm_output_basename = params.get("Output basename");

        if (enable_time_dependent)
          {
            prm_output_every_n_timesteps =
              params.get_integer("Save output every n timesteps");
            prm_output_from_time = params.get_double("Start saving from time");
          }

        const std::string tmp = params.get("Output format");
        if (tmp == "HDF5")
          prm_output_format = OutputFormat::HDF5;
        else // if (tmp == "PVTU")
          prm_output_format = OutputFormat::PVTU;

        if (prm_output_format == OutputFormat::HDF5)
          {
            params.enter_subsection("HDF5 format");
            if (enable_time_dependent)
              {
                prm_hdf5_mesh_at_every_output =
                  params.get_bool("Export mesh at every output");
              }
            prm_hdf5_filter_duplicate_vertices =
              params.get_bool("Filter duplicate vertices");
            params.leave_subsection();
          }
        else // if (prm_output_format == OutputFormat::PVTU)
          {
            params.enter_subsection("PVTU format");
            prm_pvtu_high_order_output = params.get_bool("High-order output");
            params.leave_subsection();
          }
      }

    params.leave_subsection_path();
  }

  template <class DoFHandlerType, class DataOutType>
  void
  OutputHandlerBase<DoFHandlerType, DataOutType>::
    inherit_parameters_from_parent(
      const OutputHandlerBase<DoFHandlerType, DataOutType> &other,
      const std::string                                    &basename_suffix)
  {
    prm_output_basename = other.prm_output_basename + basename_suffix;

    if (enable_time_dependent && other.enable_time_dependent)
      {
        prm_output_every_n_timesteps = other.prm_output_every_n_timesteps;
        prm_output_from_time         = other.prm_output_from_time;
      }

    prm_output_format = other.prm_output_format;

    if (enable_time_dependent && other.enable_time_dependent)
      {
        prm_hdf5_mesh_at_every_output = other.prm_hdf5_mesh_at_every_output;
      }

    prm_hdf5_filter_duplicate_vertices =
      other.prm_hdf5_filter_duplicate_vertices;
    prm_pvtu_high_order_output = other.prm_pvtu_high_order_output;
  }

  template <class DoFHandlerType, class DataOutType>
  void
  OutputHandlerBase<DoFHandlerType, DataOutType>::set_time(
    const double       &time_,
    const unsigned int &timestep_number_)
  {
    Assert(enable_time_dependent,
           ExcMessage("This method requires time-dependent output."));

    time            = time_;
    timestep_number = timestep_number_;
  }

  template <class DoFHandlerType, class DataOutType>
  bool
  OutputHandlerBase<DoFHandlerType, DataOutType>::is_active() const
  {
    if (!standalone || !enable_time_dependent)
      {
        return prm_enable_output;
      }
    else
      {
        bool do_output =
          prm_enable_output &&
          !utils::is_definitely_less_than(time, prm_output_from_time);

        if (first_output_timestep.has_value())
          {
            do_output =
              do_output && ((timestep_number - first_output_timestep.value()) %
                              prm_output_every_n_timesteps ==
                            0);
          }

        return do_output;
      }
  }

  template <class DoFHandlerType, class DataOutType>
  void
  OutputHandlerBase<DoFHandlerType, DataOutType>::write(
    const unsigned int &n_subdivisions) const
  {
    Assert(standalone, ExcStandaloneOnly());

    data_out_->build_patches(n_subdivisions);
    write_internal();
  }

  template <class DoFHandlerType, class DataOutType>
  void
  OutputHandlerBase<DoFHandlerType, DataOutType>::write_internal() const
  {
    Assert(standalone, ExcStandaloneOnly());

    if (!first_output_timestep.has_value())
      first_output_timestep = timestep_number;

    if (prm_output_format == OutputFormat::HDF5)
      {
        if (enable_time_dependent)
          {
            unsigned int timestep_mesh = 0;

            if (prm_hdf5_mesh_at_every_output)
              timestep_mesh = timestep_number;
            else
              timestep_mesh = first_output_timestep.value();

            dataout_write_hdf5(*data_out_,
                               prm_output_basename + filename_suffix,
                               timestep_number,
                               timestep_mesh,
                               time,
                               prm_hdf5_filter_duplicate_vertices);
          }
        else
          {
            dataout_write_hdf5(*data_out_,
                               prm_output_basename + filename_suffix,
                               prm_hdf5_filter_duplicate_vertices);
          }
      }
    else // if (prm_output_format_== OutputFormat::PVTU)
      {
        if (enable_time_dependent)
          {
            dataout_write_pvtu(*data_out_,
                               prm_output_basename + filename_suffix,
                               timestep_number,
                               time);
          }
        else
          {
            dataout_write_pvtu(*data_out_,
                               prm_output_basename + filename_suffix);
          }
      }

    // Release memory.
    data_out_->clear();

    // We create an entirely new instance of DataOut. This is needed to allow
    // subsequent writes to work on different triangulations without having to
    // create an entirely new output handler.
    data_out_ = std::make_unique<DataOutType>();
  }

  /// Explicit instantiation.
  template class OutputHandlerBase<DoFHandler<dim>>;

  /// Explicit instantiation.
  template class OutputHandlerBase<DoFHandler<1, dim>>;

  /// Explicit instantiation.
  template class OutputHandlerBase<DoFHandler<dim>, DataOutFaces<dim>>;
} // namespace lifex::utils
