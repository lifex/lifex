/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_UTILS_VTK_IMPORTER_HPP_
#define LIFEX_UTILS_VTK_IMPORTER_HPP_

#include "source/lifex.hpp"

#include "source/core_model.hpp"

#include "source/geometry/mesh_handler.hpp"

#include "source/io/vtk_function.hpp"

#include <map>
#include <string>
#include <vector>

namespace lifex::utils
{
  /**
   * @brief Utility class to import data from VTK files.
   *
   * This class allows to read data from a VTK file (e.g. in VTU format) and
   * evaluate it on a mesh (e.g. through a closest-point or linear projection,
   * see @ref VTKFunction). The resulting finite element vectors can be then
   * requested from this class by its users.
   *
   * Optionally, the vectors can be serialized to file for later use. The user
   * can then reload previously serialized vectors by setting the parameter
   * <kbd>Input file format = Serialized file</kbd> and providing the path to
   * the serialized file in the dedicated parameter.
   */
  class VTKImporter : public CoreModel
  {
  public:
    /// Input file format.
    enum class InputFormat
    {
      VTK,           ///< Input from a VTK file (VTK, VTU or VTP format).
      SerializedFile ///< Input from a previously serialized file.
    };

    /// Constructor.
    VTKImporter(const std::string              &subsection,
                const std::vector<std::string> &field_labels_);

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Initialize the importer for a given DoF handler.
    void
    initialize(const MeshHandler<dim> &mesh_handler,
               const DoFHandler<dim>  &dof_handler);

    /// Retrieve the interpolated vector (without ghost elements).
    const LinAlg::MPI::Vector &
    get_owned(const std::string &label) const;

    /// Get array data type.
    VTKArrayDataType
    get_array_data_type() const
    {
      AssertThrow(prm_input_format == InputFormat::VTK,
                  ExcMessage("VTK array data type is not available when "
                             "importing serialized files."));

      return prm_vtk_arraydatatype;
    }

  protected:
    /// Labels identifying the fields that must be imported.
    std::vector<std::string> field_labels;

    /// For each field imported from the VTK file, its interpolation onto the
    /// finite element space.
    std::map<std::string, LinAlg::MPI::Vector> data_owned;

    /// @name Parameters read from file.
    /// @{

    /// Input format.
    InputFormat prm_input_format;

    /// VTK filename.
    std::string prm_vtk_filename;

    /// Scaling factor for the input geometry.
    double prm_geometry_scaling_factor;

    /// For each field label provided to the constructor, the name of the
    /// imported array within the VTK file.
    std::map<std::string, std::string> prm_vtk_arrayname;

    /// For each field label provided to the constructor, the scaling factor for
    /// the corresponding input array.
    std::map<std::string, double> prm_array_scaling_factor;

    /// Data type (unstructured grid or surface).
    VTKDataType prm_vtk_datatype;

    /// Array type (cell data or point data).
    VTKArrayDataType prm_vtk_arraydatatype;

    /// Evaluation mode (linear projection or closest-point).
    VTKFunction::Mode prm_mode;

    /// Toggle serialization of the interpolated vector.
    bool prm_enable_serialization;

    /// Serialization filename.
    std::string prm_serialization_filename;

    /// Deserialization filename.
    std::string prm_deserialization_filename;

    /// @}
  };
} // namespace lifex::utils

#endif /* LIFEX_UTILS_VTK_IMPORTER_HPP_ */
