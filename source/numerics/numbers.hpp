/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_UTILS_NUMBERS_HPP_
#define LIFEX_UTILS_NUMBERS_HPP_

#include "source/lifex.hpp"

#include <deal.II/lac/lapack_templates.h>

#include <deal.II/physics/elasticity/kinematics.h>

#include <algorithm>
#include <cmath>
#include <complex>
#include <limits>
#include <map>
#include <set>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace lifex::utils
{
  /// Helper template variable: this is true whenever
  /// type @p T represents a floating point type
  /// (since C++17).
  template <class T>
  inline constexpr bool is_floating_v = (std::is_floating_point_v<T>);

  /// Check if two floating point numbers <kbd>a</kbd> and <kbd>b</kbd>
  /// are equal, up to a given tolerance.
  template <class T>
  inline constexpr bool
  is_equal(const T a,
           const T b,
           const T tolerance = std::numeric_limits<T>::epsilon())
  {
    static_assert(is_floating_v<T>,
                  "is_equal: template parameter does not represent a floating "
                  "point number.");

    Assert(tolerance >= 0, ExcMessage("Tolerance must be non-negative."));

    return std::abs(a - b) <= (std::max(std::abs(a), std::abs(b)) * tolerance);
  }

  /// Check if a floating point number <kbd>a</kbd>
  /// is zero, up to a given tolerance.
  template <class T>
  inline constexpr bool
  is_zero(const T a, const T tolerance = std::numeric_limits<T>::epsilon())
  {
    static_assert(is_floating_v<T>,
                  "is_zero: template parameter does not represent a floating "
                  "point number.");

    Assert(tolerance >= 0, ExcMessage("Tolerance must be non-negative."));

    return (std::abs(a) <= tolerance);
  }

  /// Check if a floating point number <kbd>a</kbd>
  /// is positive, up to a given tolerance.
  template <class T>
  inline constexpr bool
  is_positive(const T a, const T tolerance = std::numeric_limits<T>::epsilon())
  {
    static_assert(is_floating_v<T>,
                  "is_positive: template parameter does not represent a "
                  "floating point number.");

    Assert(tolerance >= 0, ExcMessage("Tolerance must be non-negative."));

    return (a > tolerance);
  }

  /// Check if a floating point number <kbd>a</kbd>
  /// is negative, up to a given tolerance.
  template <class T>
  inline constexpr bool
  is_negative(const T a, const T tolerance = std::numeric_limits<T>::epsilon())
  {
    static_assert(is_floating_v<T>,
                  "is_negative: template parameter does not represent a "
                  "floating point number.");

    Assert(tolerance >= 0, ExcMessage("Tolerance must be non-negative."));

    return (a < -tolerance);
  }

  /// Check if a floating point number <kbd>a</kbd> is @a definitely greater
  /// than a floating point number <kbd>b</kbd>, up to a given tolerance.
  template <class T>
  inline constexpr bool
  is_definitely_greater_than(
    const T a,
    const T b,
    const T tolerance = std::numeric_limits<T>::epsilon())
  {
    static_assert(
      is_floating_v<T>,
      "is_definitely_greather_than: template parameter does not represent a "
      "floating point number.");

    Assert(tolerance >= 0, ExcMessage("Tolerance must be non-negative."));

    return (a - b) > (std::max(std::abs(a), std::abs(b)) * tolerance);
  }

  /// Check if a floating point number <kbd>a</kbd> is @a definitely smaller
  /// than a floating point number <kbd>b</kbd>, up to a given tolerance.
  template <class T>
  inline constexpr bool
  is_definitely_less_than(const T a,
                          const T b,
                          const T tolerance = std::numeric_limits<T>::epsilon())
  {
    static_assert(
      is_floating_v<T>,
      "is_definitely_less_than: template parameter does not represent a "
      "floating point number.");

    Assert(tolerance >= 0, ExcMessage("Tolerance must be non-negative."));

    return (b - a) > (std::max(std::abs(a), std::abs(b)) * tolerance);
  }

  /**
   * Dummy type used for type traits below.
   */
  template <typename... T>
  using void_t = void;

  /**
   * Type trait to tell if a container has a <kbd>.find()</kbd> member function.
   */
  template <class ContainerType, class ValueType, class = void_t<>>
  struct has_member_find : std::false_type
  {};

  /**
   * Partial specialization: true if container has a <kbd>.find()</kbd> member
   * function.
   */
  template <class ContainerType, class ValueType>
  struct has_member_find<ContainerType,
                         ValueType,
                         void_t<decltype(std::declval<ContainerType>().find(
                           std::declval<ValueType>()))>> : std::true_type
  {};

  /**
   * Check if an arbitary container contains a given value.
   */
  template <class ContainerType, class ValueType>
  inline constexpr bool
  contains(const ContainerType &container, const ValueType &value)
  {
    if constexpr (has_member_find<ContainerType, ValueType>())
      return container.find(value) != std::end(container);
    else
      return std::find(std::begin(container), std::end(container), value) !=
             std::end(container);
  }

  /**
   * Evaluate a scalar function (of scalar variable) and its derivative at a point @p x, using
   * @ref ADHelperScalarFunction.
   */
  std::pair<double, double>
  compute_value_and_derivative(const std::function<double_AD(double_AD)> &f,
                               const double                              &x);

  /**
   * Compute the sharp Heaviside function
   * @f[
   * H(x) =
   * \begin{cases}
   * 1, & x > x0, \\
   * 0, & x \leq x0.
   * \end{cases}
   * @f]
   */
  double
  heaviside_sharp(const double &x, const double &x0 = 0);

  /**
   * Compute the smoothed Heaviside function
   * @f[
   * H(x) = \frac{1}{2}\left(1 + \tanh\left(k\left(x -
   * x_0\right)\right)\right).
   * @f]
   */
  double
  heaviside(const double &x, const double &x0 = 0, const double &k = 200);

  /**
   * Compute the smoothed trigonometric Heaviside function
   * @f[
   * H(x) = \frac{1}{2} + \frac{1}{\pi} \arctan\left(\frac{k\pi}{2}(x -
   * x_0)\right).
   * @f]
   */
  double
  heaviside_trig(const double &x, const double &x0 = 0, const double &k = 200);

  /**
   * Cosinusoidal ramp.
   *@f[
   * f(t) = \left\{
   * \begin{alignedat}{3}
   * & v_0, & \quad & \text{if } t < t_0, \\
   * & v_0 + (v_1 - v_0)\left[\frac{1}{2} - \frac{1}{2}\cos\left(\frac{t -
   *t_0}{t_1 - t_0}\pi\right)\right], & \quad & \text{if } t_0 \leq t < t_1, \\
   * & v_1, & \quad & \text{if } t \geq t_1.
   * \end{alignedat}
   * \right.
   * @f]
   */
  double
  cosine_ramp(const double &t,
              const double &t0 = 0.0,
              const double &t1 = 1.0,
              const double &v0 = 0.0,
              const double &v1 = 1.0);

  /**
   * Compute minimum value of a parallel vector over all MPI processes,
   * optionally limiting this operation to a subset of indices.
   */
  template <class VectorType>
  typename VectorType::real_type
  vec_min(const VectorType              &vec,
          const std::optional<IndexSet> &active_indices = {});

  /**
   * Compute average value of a parallel vector over all MPI processes,
   * optionally limiting this operation to a subset of indices.
   */
  template <class VectorType>
  typename VectorType::real_type
  vec_avg(const VectorType              &vec,
          const std::optional<IndexSet> &active_indices = {});

  /**
   * Compute maximum value of a parallel vector over all MPI processes,
   * optionally limiting this operation to a subset of indices.
   */
  template <class VectorType>
  typename VectorType::real_type
  vec_max(const VectorType              &vec,
          const std::optional<IndexSet> &active_indices = {});

  /**
   * @brief Compute the [Bessel functions of the first kind]
   * (https://en.wikipedia.org/wiki/Bessel_function#Bessel_functions_of_the_first_kind:_J%CE%B1).
   *
   * The function computes
   * @f[
   * J_n(z) = \sum_{m = 0}^{\infty} \frac{(-1)^m}{m! (m + n)!}
   * \left(\frac{z}{2}\right)^{2m + n}\;,
   * @f]
   * for @f$n \in \mathbb{N}@f$ and @f$z \in \mathbb{C}@f$.
   *
   * #### Implementation details
   * The function is computed by separately computing its real and imaginary
   * parts. Denoting by @f$r@f$ the modulus and by @f$\theta@f$ the argument of
   * @f$z@f$,
   * @f[
   * \begin{aligned}
   * \text{Re} J_n(z) &= \sum_{m = 0}^{\infty} \frac{(-1)^m}{m! (m +
   * n)!}\left(\frac{r}{2}\right)^{2m + n} \cos((2m + n)\theta) = \sum_{m =
   * 0}^{\infty} J_{n,m}^\text{Re} \;, \\
   * \text{Im} J_n(z) &= \sum_{m = 0}^{\infty} \frac{(-1)^m}{m! (m
   * + n)!}\left(\frac{r}{2}\right)^{2m + n} \sin((2m + n)\theta) = \sum_{m =
   * 0}^{\infty} J_{n,m}^\text{Im} \;.
   * \end{aligned}
   * @f]
   *
   * In practice, we compute the series as
   * @f[
   * \sum_{k = 0}^{K_{\max}}\left(J^*_{2k} + J^*_{2k + 1}\right)\;,
   * @f]
   * where @f$*@f$ is either @f$\text{Re}@f$ or @f$\text{Im}@f$. Since
   * subsequent terms of the series cancel out, this aids numerical stability
   * reduces the risk of getting <kbd>Inf</kbd> or <kbd>NaN</kbd> values
   * appearing in the computation.
   * @f$K_{\max}@f$ is chosen so that
   * @f[
   * \left|\sum_{k = 0}^{K_{\max}}\left(J^*_{2k} + J^*_{2k + 1}\right) - \sum_{k
   * = 0}^{K_{\max} - 1}\left(J^*_{2k} + J^*_{2k + 1}\right)\right| \leq
   * \varepsilon\;,
   * @f]
   * with @f$\varepsilon@f$ a prescribed tolerance.
   *
   * @param[in] n Order @f$n@f$ of the Bessel function to be computed.
   * @param[in] z Argument @f$z@f$ for the Bessel function.
   * @param[in] tolerance Tolerance @f$\varepsilon@f$ used in the computation of
   * the Bessel function.
   *
   * @note This extends the standard-library function cyl_bessel_j, that does
   * not support complex arguments.
   */
  std::complex<double>
  bessel_j(const unsigned int        &n,
           const std::complex<double> z,
           const double              &tolerance = 1e-6);

  /**
   * @brief Singular value decomposition of a tensor.
   *
   * This falls back onto the gesvd method from Lapack. The function is adapted
   * from from deal.II's calculate_svd_in_place (see
   * dealii/source/base/tensor.cc).
   */
  template <int dim>
  std::tuple<Tensor<2, dim>, std::array<double, dim>, Tensor<2, dim>>
  svd(const Tensor<2, dim> &A)
  {
    // This function is adapted from deal.II's calculate_svd_in_place (see
    // dealii/source/base/tensor.cc).

    Tensor<2, dim>          U, VT(A);
    std::array<double, dim> S; // Array of singular values.

    const types::blas_int     N     = dim;
    const types::blas_int     lwork = 5 * dim;
    std::array<double, lwork> work;
    types::blas_int           info;

    gesvd(&LAPACKSupport::O,
          &LAPACKSupport::A,
          &N,
          &N,
          VT.begin_raw(),
          &N,
          S.data(),
          VT.begin_raw(),
          &N,
          U.begin_raw(),
          &N,
          work.data(),
          &lwork,
          &info);

    Assert(info == 0, LAPACKSupport::ExcErrorCode("gesvd", info));

    return std::make_tuple(U, S, VT);
  }

  /**
   * @brief Compute the singular value decomposition of a tensor with an
   * axis-aligned ordering of singular values.
   *
   * Standard SVD (as computed by @ref svd) computes a decomposition of the
   * input tensor @f$A@f$ into:
   * @f[ A = \sum_{i = 1}^d \sigma_i \mathbf u_i \otimes \mathbf v_i\;, @f]
   * where the singular values @f$\sigma_i@f$ are in descending order and
   * @f$\mathbf u_i \cdot \mathbf u_j = \delta_{ij}@f$, @f$\mathbf v_i \cdot
   * \mathbf v_j = \delta_{ij}@f$.
   *
   * This functions computes a decomposition in the same form, but such that,
   * denoting by @f$\mathbf e_i@f$ the vectors of the canonical basis of
   * @f$\mathbb{R}^d@f$, there holds
   * @f[0 < \mathbf v_i \cdot \mathbf e_i = \max_{j = i, i+1, \dots, d}|\mathbf
   * v_j\cdot\mathbf e_i\;.|@f]
   * In other words, the vector @f$\mathbf v_1@f$ is the most aligned to
   * @f$\mathbf e_1@f$ among the @f$\mathbf v_i@f$, @f$\mathbf v_2@f$ is the
   * most aligned to @f$\mathbf e_2@f$, and so on.
   *
   * Furthermore, the sign of vectors @f$\mathbf u_i@f$ and @f$\mathbf v_i@f$ is
   * such that the positivity of the determinants of the matrices @f$U = [
   * \mathbf u_1, \dots, \mathbf u_d ]@f$ and @f$V = [ \mathbf v_1, \dots,
   * \mathbf v_d ]@f$ is guaranteed.
   *
   * **Reference:** @pubcite{bucelli2023preserving, Bucelli et al. (2023)}.
   */
  template <int dim>
  std::tuple<Tensor<2, dim>, std::array<double, dim>, Tensor<2, dim>>
  aligned_svd(const Tensor<2, dim> &A)
  {
    Tensor<2, dim>          U_in, VT_in;
    std::array<double, dim> S_in;

    // Compute the "basic" SVD.
    std::tie(U_in, S_in, VT_in) = utils::svd(A);

    std::array<unsigned int, dim> new_indices;
    std::array<int, dim>          sign;

    std::set<unsigned int> indices_to_place;
    for (unsigned int i = 0; i < dim; ++i)
      indices_to_place.insert(i);

    for (unsigned int i = 0; i < dim - 1; ++i)
      {
        double tmp_0 = -1.0;
        double tmp_1 = 0.0;

        for (const auto &j : indices_to_place)
          {
            tmp_1 = std::abs(VT_in[j][i]);

            if (tmp_1 > tmp_0)
              {
                tmp_0          = tmp_1;
                new_indices[i] = j;
                sign[i]        = VT_in[j][i] > 0 ? 1.0 : -1.0;
              }
          }

        indices_to_place.erase(new_indices[i]);
      }

    new_indices[dim - 1] = *indices_to_place.begin();
    sign[dim - 1]        = 1.0;

    Tensor<2, dim>          U, VT;
    std::array<double, dim> S;

    for (unsigned int i = 0; i < dim; ++i)
      {
        for (unsigned int j = 0; j < dim; ++j)
          {
            U[i][j]  = sign[j] * U_in[i][new_indices[j]];
            VT[j][i] = sign[j] * VT_in[new_indices[j]][i];
          }

        S[i] = S_in[new_indices[i]];
      }

    const double det_U = determinant(U);
    if (det_U < 0)
      {
        for (unsigned int i = 0; i < dim; ++i)
          {
            U[i][dim - 1] *= -1.0;
            VT[dim - 1][i] *= -1.0;
          }
      }

    return std::make_tuple(U, S, VT);
  }

  namespace convert
  {
    /// Conversion factor from @f$[mmHg]@f$ to @f$[Pa]@f$.
    static inline constexpr double mmHg_to_Pa = 133.322;

    /// Conversion factor from @f$[Pa]@f$ to @f$[mmHg]@f$.
    static inline constexpr double Pa_to_mmHg = 1.0 / mmHg_to_Pa;

    /// Conversion factor from @f$[ml]@f$ to @f$[m^3]@f$.
    static inline constexpr double mL_to_m3 = 1e-6;

    /// Conversion factor from @f$[m^3]@f$ to @f$[ml]@f$.
    static inline constexpr double m3_to_mL = 1e6;

    /// Conversion factor from degrees to radians.
    static inline constexpr double deg_to_rad = M_PI / 180.0;

    /// Conversion factor from radians to degrees.
    static inline constexpr double rad_to_deg = 1.0 / deg_to_rad;
  } // namespace convert

  /**
   * Namespace containing functions for kinematics problems.
   *
   * @note The functions included here are surrogates of the ones provided by
   * @dealii in <kbd>Physics::Elasticity::Kinematics</kbd>, as those functions
   * lead to issues on some compilers. More specifically, on some versions of
   * Intel compilers they result in either zero or undefined/incorrect behavior
   * depending on the level of compiler optimization.
   */
  namespace kinematics
  {
    /**
     * Compute deformation gradient tensor @f$F = I + \nabla\mathbf d@f$.
     */
    template <class NumberType, int tensor_dim>
    Tensor<2, tensor_dim, NumberType>
    F(const Tensor<2, tensor_dim, NumberType> &grad_d)
    {
      return unit_symmetric_tensor<tensor_dim, NumberType>() + grad_d;
    }

    /**
     * Compute the symmetric Green-Lagrange strain tensor @f$E =
     * \frac{1}{2}\left(F^T F - I\right)@f$.
     */
    template <class NumberType, int tensor_dim>
    SymmetricTensor<2, tensor_dim, NumberType>
    E(const Tensor<2, tensor_dim, NumberType> &F)
    {
      SymmetricTensor<2, tensor_dim, NumberType> result;

      for (unsigned int i = 0; i < tensor_dim; ++i)
        for (unsigned int j = i; j < tensor_dim; ++j)
          {
            result[i][j] = (i == j ? -0.5 : 0.0);

            for (unsigned int k = 0; k < tensor_dim; ++k)
              result[i][j] += 0.5 * (F[k][i] * F[k][j]);
          }

      return result;
    }

  } // namespace kinematics

  /**
   * @brief Invert a tensor, assuming the determinant is known.
   *
   * In situations where one needs to compute both the determinant and the
   * inverse of a tensor, calling deal.II's determinant and inverse functions
   * will lead to redundant computations (the determinant is computed twice).
   * These computations may be expensive depending on the NumberType (they
   * usually are for double_ADs). This function reuses the previously computed
   * determinant to compute the inverse.
   *
   * This function is only implemented for tensor_dim = 2 and tensor_dim = 3.
   */
  template <class NumberType, int tensor_dim>
  Tensor<2, tensor_dim, NumberType>
  invert(const Tensor<2, tensor_dim, NumberType> &A, const NumberType &det_A)
  {
    Tensor<2, tensor_dim, NumberType> Ainv;
    const NumberType                  inv_det = 1.0 / det_A;

    if constexpr (tensor_dim == 3)
      {
        Ainv[0][0] = (A[1][1] * A[2][2] - A[1][2] * A[2][1]) * inv_det;
        Ainv[0][1] = (A[0][2] * A[2][1] - A[0][1] * A[2][2]) * inv_det;
        Ainv[0][2] = (A[0][1] * A[1][2] - A[0][2] * A[1][1]) * inv_det;
        Ainv[1][0] = (A[1][2] * A[2][0] - A[1][0] * A[2][2]) * inv_det;
        Ainv[1][1] = (A[0][0] * A[2][2] - A[0][2] * A[2][0]) * inv_det;
        Ainv[1][2] = (A[0][2] * A[1][0] - A[0][0] * A[1][2]) * inv_det;
        Ainv[2][0] = (A[1][0] * A[2][1] - A[1][1] * A[2][0]) * inv_det;
        Ainv[2][1] = (A[0][1] * A[2][0] - A[0][0] * A[2][1]) * inv_det;
        Ainv[2][2] = (A[0][0] * A[1][1] - A[0][1] * A[1][0]) * inv_det;
      }
    else if constexpr (tensor_dim == 2)
      {
        Ainv[0][0] = A[1][1] * inv_det;
        Ainv[0][1] = -A[0][1] * inv_det;
        Ainv[1][0] = -A[1][0] * inv_det;
        Ainv[1][1] = A[0][0] * inv_det;
      }
    else
      {
        AssertThrow(false, ExcLifexNotImplemented());
      }

    return Ainv;
  }

  namespace MPI
  {
    /// Structure to store the data of MPI_Allreduce calls with MPI_MINLOC
    /// operation.
    struct MinLocDoubleData
    {
      double value; ///< Stored value.
      int    rank;  ///< Rank owning the minimum.
    };

    /// Return the minimum value across different processes, as well as the
    /// rank owning the minimum.
    MinLocDoubleData
    minloc(const double &in, const MPI_Comm &mpi_comm);

    /// Return the union of maps across MPI processes. If duplicate keys exist
    /// among different processes, only the value with the lowest rank is kept.
    template <class T1, class T2>
    std::map<T1, T2>
    compute_map_union(const std::map<T1, T2> &map, const MPI_Comm &mpi_comm)
    {
      const unsigned int mpi_rank = Utilities::MPI::this_mpi_process(mpi_comm);
      const unsigned int mpi_size = Utilities::MPI::n_mpi_processes(mpi_comm);

      const std::vector<std::map<T1, T2>> all_maps =
        Utilities::MPI::all_gather(mpi_comm, map);

      std::map<T1, T2> result = map;

      for (unsigned int rank = 0; rank < mpi_size; ++rank)
        if (mpi_rank != rank)
          result.insert(all_maps[rank].begin(), all_maps[rank].end());

      return result;
    }

    /**
     * @brief Compute the parallel union of vectors.
     */
    template <class T>
    void
    compute_vector_union(std::vector<T> &vector,
                         const IndexSet &owned_indices,
                         const MPI_Comm &mpi_comm)
    {
      Assert(owned_indices.size() == vector.size(), ExcLifexInternalError());

      const IndexSet all_indices = complete_index_set(owned_indices.size());
      const auto     index_owners =
        Utilities::MPI::compute_index_owner(owned_indices,
                                            all_indices,
                                            mpi_comm);

      for (unsigned int i = 0; i < vector.size(); ++i)
        vector[i] =
          Utilities::MPI::broadcast(mpi_comm, vector[i], index_owners[i]);
    }
  } // namespace MPI
} // namespace lifex::utils

#endif /* LIFEX_UTILS_NUMBERS_HPP_ */
