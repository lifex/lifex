/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_POLYNOMIAL_EXTRAPOLATION_HPP_
#define LIFEX_POLYNOMIAL_EXTRAPOLATION_HPP_

#include <deque>
#include <utility>

namespace lifex::utils
{
  /**
   * @brief Polynomial extrapolation.
   *
   * Given a series of pairs @f$(x_i, \mathbf{y}_i)@f$, for @f$i = 1, 2, \dots,
   * M@f$, computes an extrapolation @f$\mathbf{y}_{i+1}@f$ corresponding to
   * @f$x_{i+1}@f$, using the last available data pairs.
   *
   * The extrapolation is constructed by interpolating the last @f$n@f$ data
   * pairs with a polynomial of degree @f$n - 1@f$, and evaluating that
   * polynomial in @f$x_{i+1}@f$.
   *
   * The number @f$n@f$ of pairs used is computed as @f$n = \min(M, N)@f$, where
   * @f$N@f$ is the order of the desired extrapolation. In other words, if the
   * desired extrapolation is of order @f$N = 3@f$, this class will return a
   * first-order extrapolation if only one data pair is available, a second
   * order extrapolation if two data pairs are available, and a third order
   * extrapolation if three or more data pairs are available.
   *
   * The class only implements the cases @f$N \leq 4@f$.
   *
   * Usage example:
   * @code{.cpp}
   * PolynomialExtrapolation extrapolation(3);
   *
   * // Add the data pairs.
   * extrapolation.add(0, vector_0);
   * extrapolation.add(0.5, vector_1);
   * extrapolation.add(0.75, vector_2);
   *
   * // Get the extrapolation.
   * vector_3 = extrapolation.get(1.25);
   * @endcode
   */
  template <class VectorType>
  class PolynomialExtrapolation
  {
  public:
    /// Constructor.
    PolynomialExtrapolation(const unsigned int &order_);

    /// Get the extrapolation.
    VectorType
    get(const double &x) const;

    /// Add a new data point.
    void
    add(const double &x, const VectorType &y);

  protected:
    /// Extrapolation order.
    const unsigned int order;

    /// Stored data pairs.
    std::deque<std::pair<double, VectorType>> data;
  };

} // namespace lifex::utils

#endif