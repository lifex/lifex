/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/numerics/rbf_interpolation.hpp"

namespace lifex::utils
{

  RBFInterpolationDoFs::RBFInterpolationDoFs(const std::string &subsection)
    : RBFInterpolation(subsection)
  {}

  void
  RBFInterpolationDoFs::setup_system(const DoFHandler<dim> &dof_handler_src,
                                     const DoFHandler<dim> &dof_handler_dst)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Setup system");

    // The two DoF handlers must have the same number of components.
    Assert(dof_handler_src.get_fe().n_components() ==
             dof_handler_dst.get_fe().n_components(),
           ExcDimensionMismatch(dof_handler_src.get_fe().n_components(),
                                dof_handler_dst.get_fe().n_components()));

    const unsigned int n_components = dof_handler_src.get_fe().n_components();

    std::vector<PointVec> points_owned_src(n_components);
    std::vector<PointVec> points_owned_dst(n_components);

    for (unsigned int c = 0; c < n_components; ++c)
      {
        compute_points(dof_handler_src, points_owned_src[c], c);
        compute_points(dof_handler_dst, points_owned_dst[c], c);
      }

    setup_system_internal(points_owned_src, points_owned_dst);
  }

  void
  RBFInterpolationDoFs::compute_points(const DoFHandler<dim> &dof_handler,
                                       PointVec              &owned_points,
                                       const unsigned int    &component) const
  {
    ComponentMask mask(dof_handler.get_fe().n_components(), false);
    mask.set(component, true);

    const auto mapping =
      MeshHandler<dim>::get_linear_mapping(dof_handler.get_triangulation());

    const PointMap points_map =
      DoFTools::map_dofs_to_support_points(*mapping, dof_handler, mask);

    // Keep only owned points.
    const IndexSet &owned_dofs = dof_handler.locally_owned_dofs();
    for (const auto &i : points_map)
      if (owned_dofs.is_element(i.first))
        owned_points.emplace_back(i);
  }
} // namespace lifex::utils