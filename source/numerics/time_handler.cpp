/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Marco Fedele <marco.fedele@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/numerics/time_handler.hpp"

#include <utility>

namespace lifex::utils
{
  template <class VectorType>
  BDFHandler<VectorType>::BDFHandler()
    : initialized(false)
  {
    static_assert(is_any_v<VectorType,
                           double,
                           LinAlg::MPI::Vector,
                           LinAlg::MPI::BlockVector,
                           LinearAlgebra::distributed::Vector<double>,
                           std::vector<std::vector<double>>,
                           std::vector<std::vector<Tensor<1, dim, double>>>,
                           std::vector<std::vector<Tensor<2, dim, double>>>>,
                  "BDFHandler: template parameter not allowed.");

    initialized = false;
  }

  template <class VectorType>
  BDFHandler<VectorType>::BDFHandler(
    const unsigned int            &order_,
    const std::vector<VectorType> &initial_solutions)
    : initialized(false)
    , order(order_)
  {
    initialize(order, initial_solutions);
  }

  template <class VectorType>
  void
  BDFHandler<VectorType>::copy_from(const BDFHandler<VectorType> &other)
  {
    initialized = other.initialized;

    order = other.order;

    solutions = other.solutions;

    alpha = other.alpha;

    *sol_bdf           = *(other.sol_bdf);
    *sol_extrapolation = *(other.sol_extrapolation);
  }

  template <class VectorType>
  void
  BDFHandler<VectorType>::set_order(const unsigned int &order_)
  {
    order = order_;

    Assert(order > 0 && order <= 3, ExcIndexRange(order, 1, 4));

    // Initialize alpha.
    switch (order)
      {
        case 1:
          alpha = 1.0;
          break;

        case 2:
          alpha = 1.5;
          break;

        case 3:
          alpha = 11.0 / 6;
          break;

        default:
          break;
      }
  }

  template <class VectorType>
  void
  BDFHandler<VectorType>::initialize(
    const unsigned int            &order_,
    const std::vector<VectorType> &initial_solutions)
  {
    set_order(order_);

    Assert(initial_solutions.size() == order,
           ExcDimensionMismatch(initial_solutions.size(), order));

    solutions.resize(order - 1);

    for (size_t i = 0; i < initial_solutions.size() - 1; ++i)
      {
        if constexpr (is_any_v<VectorType,
                               LinAlg::MPI::Vector,
                               LinAlg::MPI::BlockVector,
                               LinearAlgebra::distributed::Vector<double>>)
          {
            Assert(!initial_solutions[i].has_ghost_elements(),
                   ExcGhostsPresent());
          }

        solutions[i] = initial_solutions[i];
      }

    sol_bdf           = std::make_shared<VectorType>(initial_solutions[0]);
    sol_extrapolation = std::make_shared<VectorType>(initial_solutions[0]);

    initialized = true;

    // Update sol_bdf and sol_extrapolation using the initial guess.
    {
      // Backup initial solutions to prevent overwriting the oldest one.
      const auto solutions_backup = solutions;
      time_advance(initial_solutions.back(), true);
      solutions = solutions_backup;
    }
  }

  template <class VectorType>
  void
  BDFHandler<VectorType>::initialize(const unsigned int &order_,
                                     const VectorType   &initial_solution)
  {
    set_order(order_);

    if constexpr (is_any_v<VectorType,
                           LinAlg::MPI::Vector,
                           LinAlg::MPI::BlockVector,
                           LinearAlgebra::distributed::Vector<double>>)
      {
        Assert(!initial_solution.has_ghost_elements(), ExcGhostsPresent());
      }

    // Initialize solutions, sol_bdf and sol_extrapolation.
    solutions.resize(order - 1);

    for (size_t i = 0; i < order - 1; ++i)
      solutions[i] = initial_solution;

    sol_bdf           = std::make_shared<VectorType>(initial_solution);
    sol_extrapolation = std::make_shared<VectorType>(initial_solution);

    initialized = true;

    // Update sol_bdf and sol_extrapolation using the initial guess.
    {
      // Backup initial solutions to prevent overwriting the oldest one.
      const auto solutions_backup = solutions;
      time_advance(initial_solution, true);
      solutions = solutions_backup;
    }
  }

  template <class VectorType>
  void
  BDFHandler<VectorType>::time_advance(const VectorType &sol_new,
                                       const bool       &update_extrapolation)
  {
    AssertThrow(initialized, ExcNotInitialized());

    Assert(order > 0 && order <= 3, ExcIndexRange(order, 1, 4));

    if constexpr (is_any_v<VectorType,
                           LinAlg::MPI::Vector,
                           LinAlg::MPI::BlockVector,
                           LinearAlgebra::distributed::Vector<double>>)
      {
        Assert(sol_new.has_ghost_elements() == false, ExcGhostsPresent());
      }

    switch (order)
      {
        case 1:
          // Order 1:
          // u_BDF = u^{n}
          // u_EXT = u^{n}

          *sol_bdf = sol_new;

          if (update_extrapolation)
            *sol_extrapolation = sol_new;

          break;

        case 2:
          // Order 2:
          // u_BDF = 2.0 * u^{n} - 0.5 * u^{n-1}
          // u_EXT = 2.0 * u^{n} - 1.0 * u^{n-1}

          // Specialization for scalars.
          if constexpr (is_any_v<VectorType, double>)
            {
              *sol_bdf = 2 * sol_new - 0.5 * solutions[0];

              if (update_extrapolation)
                *sol_extrapolation = 2 * sol_new - solutions[0];
            }

          // Specialization for MPI vectors.
          else if constexpr (is_any_v<
                               VectorType,
                               LinAlg::MPI::Vector,
                               LinAlg::MPI::BlockVector,
                               LinearAlgebra::distributed::Vector<double>>)
            {
              *sol_bdf = sol_new;
              sol_bdf->sadd(2, -0.5, solutions[0]);

              if (update_extrapolation)
                {
                  *sol_extrapolation = sol_new;
                  sol_extrapolation->sadd(2, -1, solutions[0]);
                }
            }

          // Specialization for std::vector<std::vector<*>>.
          else
            {
              for (size_t c = 0; c < solutions[0].size(); ++c)
                for (size_t q = 0; q < solutions[0][c].size(); ++q)
                  {
                    (*sol_bdf)[c][q] =
                      2 * sol_new[c][q] - 0.5 * solutions[0][c][q];
                  }

              if (update_extrapolation)
                {
                  for (size_t c = 0; c < solutions[0].size(); ++c)
                    for (size_t q = 0; q < solutions[0][c].size(); ++q)
                      {
                        (*sol_extrapolation)[c][q] =
                          2 * sol_new[c][q] - solutions[0][c][q];
                      }
                }
            }

          break;

        case 3:
          // Order 3:
          // u_BDF = 3.0 * u^{n} - 1.5 * u^{n-1} + 1/3 * u^{n-2}
          // u_EXT = 3.0 * u^{n} - 3.0 * u^{n-1} + 1.0 * u^{n-2}

          // Specialization for scalars.
          if constexpr (is_any_v<VectorType, double>)
            {
              *sol_bdf =
                3 * sol_new - 1.5 * solutions[1] + 1.0 / 3 * solutions[0];

              if (update_extrapolation)
                {
                  *sol_extrapolation =
                    3 * sol_new - 3 * solutions[1] + solutions[0];
                }
            }

          // Specialization for MPI vectors.
          else if constexpr (is_any_v<
                               VectorType,
                               LinAlg::MPI::Vector,
                               LinAlg::MPI::BlockVector,
                               LinearAlgebra::distributed::Vector<double>>)
            {
              *sol_bdf = sol_new;
              sol_bdf->sadd(3, -1.5, solutions[1]);
              sol_bdf->add(1.0 / 3, solutions[0]);

              if (update_extrapolation)
                {
                  *sol_extrapolation = sol_new;
                  sol_extrapolation->sadd(3, -3, solutions[1]);
                  sol_extrapolation->add(1, solutions[0]);
                }
            }

          // Specialization for std::vector<std::vector<*>>.
          else
            {
              for (size_t c = 0; c < solutions[0].size(); ++c)
                for (size_t q = 0; q < solutions[0][c].size(); ++q)
                  {
                    (*sol_bdf)[c][q] = 3 * sol_new[c][q] -
                                       1.5 * solutions[1][c][q] +
                                       1.0 / 3 * solutions[0][c][q];
                  }

              if (update_extrapolation)
                {
                  for (size_t c = 0; c < solutions[0].size(); ++c)
                    for (size_t q = 0; q < solutions[0][c].size(); ++q)
                      {
                        (*sol_extrapolation)[c][q] = 3 * sol_new[c][q] -
                                                     3 * solutions[1][c][q] +
                                                     solutions[0][c][q];
                      }
                }
            }
          break;

        default:
          break;
      }

    // Prepare for next time step.
    // We swap instead of popping front and pushing back because this way we
    // preserve references to solution vectors (i.e., a reference to
    // solutions[order-1] will always point to the solution from previous time
    // step, even after advancing in time).
    for (unsigned int i = 0; i + 2 < order; ++i)
      {
        // Import namespaces so that deal.II's specializations of the swap
        // function for vectors are correctly resolved.
        using namespace std;
        using namespace LinAlg::MPI;

        swap(solutions[i], solutions[i + 1]);
      }

    if (order > 1)
      solutions.back() = sol_new;
  }

  /// Explicit instantiation.
  template class BDFHandler<double>;

  /// Explicit instantiation.
  template class BDFHandler<LinAlg::MPI::Vector>;

  /// Explicit instantiation.
  template class BDFHandler<LinAlg::MPI::BlockVector>;

  /// Explicit instantiation.
  template class BDFHandler<LinearAlgebra::distributed::Vector<double>>;

  /// Explicit instantiation.
  template class BDFHandler<std::vector<std::vector<double>>>;

  /// Explicit instantiation.
  template class BDFHandler<std::vector<std::vector<Tensor<1, dim, double>>>>;

  /// Explicit instantiation.
  template class BDFHandler<std::vector<std::vector<Tensor<2, dim, double>>>>;

} // namespace lifex::utils
