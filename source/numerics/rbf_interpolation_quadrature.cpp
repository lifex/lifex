/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/numerics/rbf_interpolation.hpp"

namespace lifex::utils
{
  template <class OutputType>
  RBFInterpolationQuadrature<OutputType>::RBFInterpolationQuadrature(
    const std::string &subsection)
    : RBFInterpolation(subsection)
  {}

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::setup_system(
    const unsigned int    &n_components_,
    const DoFHandler<dim> &dof_handler_src_,
    const Quadrature<dim> &quadrature_src,
    const DoFHandler<dim> &dof_handler_dst,
    const Quadrature<dim> &quadrature_dst)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Setup system");

    n_components    = n_components_;
    dof_handler_src = &dof_handler_src_;
    n_q_src         = quadrature_src.size();

    // The interpolant operates on one component at a time: therefore, we only
    // store one point map for the source and destination meshes.
    std::vector<PointVec> points_owned_src(1);
    std::vector<PointVec> points_owned_dst(1);

    compute_points(dof_handler_src_, points_owned_src[0], quadrature_src);
    compute_points(dof_handler_dst, points_owned_dst[0], quadrature_dst);

    const unsigned int n_points_src = points_owned_src[0].size();
    const unsigned int n_points_dst = points_owned_dst[0].size();

    setup_system_internal(points_owned_src, points_owned_dst);

    // Compute the offset for current rank in the source vectors.
    const std::vector<unsigned int> n_points_src_per_rank =
      Utilities::MPI::all_gather(mpi_comm, n_points_src);
    offset_src = 0;
    for (unsigned int i = 0; i < mpi_rank; ++i)
      offset_src += n_points_src_per_rank[i];

    // Compute the offset for current rank in the destination vectors.
    const std::vector<unsigned int> n_points_dst_per_rank =
      Utilities::MPI::all_gather(mpi_comm, n_points_dst);
    offset_dst = 0;
    for (unsigned int i = 0; i < mpi_rank; ++i)
      offset_dst += n_points_dst_per_rank[i];

    // Setup facilities for quadrature evaluation.
    QuadratureEvaluationFEM<OutputType>::setup(dof_handler_dst, quadrature_dst);

    interpolated_vector.resize(n_components);
    for (auto &vec : interpolated_vector)
      vec.reinit(owned_points_dst, mpi_comm);
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::compute_points(
    const DoFHandler<dim> &dof_handler,
    PointVec              &owned_points,
    const Quadrature<dim> &quadrature) const
  {
    FEValues<dim> fe_values(dof_handler.get_fe(),
                            quadrature,
                            update_quadrature_points);

    std::vector<Point<dim>> owned_points_vector;
    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        fe_values.reinit(cell);

        for (unsigned int q = 0; q < quadrature.size(); ++q)
          owned_points_vector.emplace_back(fe_values.quadrature_point(q));
      }

    const unsigned int              n_owned_points = owned_points_vector.size();
    const std::vector<unsigned int> n_points =
      Utilities::MPI::all_gather(mpi_comm, n_owned_points);

    unsigned int offset = 0;
    for (unsigned int i = 0; i < mpi_rank; ++i)
      offset += n_points[i];

    for (unsigned int i = 0; i < owned_points_vector.size(); ++i)
      owned_points.emplace_back(
        std::make_pair(offset + i, owned_points_vector[i]));
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::interpolate(
    QuadratureEvaluationScalar &src)
  {
    // This only works if the interpolator has 1 component.
    Assert(n_components == 1, ExcDimensionMismatch(n_components, 1));

    src.init();

    LinAlg::MPI::Vector src_vector(owned_points_src, mpi_comm);

    unsigned int idx = 0;

    for (const auto &cell : dof_handler_src->active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        src.reinit(cell);

        for (unsigned int q = 0; q < n_q_src; ++q)
          {
            src_vector[offset_src + idx] = src(q);
            ++idx;
          }
      }

    interpolate_internal(interpolated_vector[0], src_vector, false);
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::interpolate(
    QuadratureEvaluation<Vector<double>> &src)
  {
    src.init();

    std::vector<LinAlg::MPI::Vector> src_vectors;
    for (unsigned int d = 0; d < n_components; ++d)
      src_vectors.emplace_back(owned_points_src, mpi_comm);

    unsigned int   idx = 0;
    Vector<double> src_loc(n_components);

    for (const auto &cell : dof_handler_src->active_cell_iterators())
      {
        if (!cell->is_locally_owned())
          continue;

        src.reinit(cell);

        for (unsigned int q = 0; q < n_q_src; ++q)
          {
            src_loc = src(q);

            Assert(src_loc.size() == n_components,
                   ExcDimensionMismatch(src_loc.size(), n_components));

            for (unsigned int d = 0; d < n_components; ++d)
              src_vectors[d][offset_src + idx] = src_loc[d];

            ++idx;
          }
      }

    for (unsigned int d = 0; d < n_components; ++d)
      {
        pcout << "\tcomponent " << d << std::flush;
        interpolate_internal(interpolated_vector[d], src_vectors[d], false);
      }
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::post_init_callback()
  {
    cell_offset   = 0;
    reinit_called = false;
  }

  template <class OutputType>
  void
  RBFInterpolationQuadrature<OutputType>::post_reinit_callback(
    const DoFHandler<dim>::active_cell_iterator & /*cell_other*/)
  {
    if (reinit_called)
      cell_offset += this->fe_values->get_quadrature().size();

    reinit_called = true;
  }

  /// Explicit instantiation.
  template class RBFInterpolationQuadrature<double>;

  /// Explicit instantiation.
  template class RBFInterpolationQuadrature<Vector<double>>;

  /// Explicit instantiation.
  template class RBFInterpolationQuadrature<Tensor<2, dim, double>>;
} // namespace lifex::utils