/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/lifex.hpp"

#include "source/numerics/polynomial_extrapolation.hpp"

namespace lifex::utils
{
  template <class VectorType>
  PolynomialExtrapolation<VectorType>::PolynomialExtrapolation(
    const unsigned int &order_)
    : order(order_)
  {}

  template <class VectorType>
  VectorType
  PolynomialExtrapolation<VectorType>::get(const double &x) const
  {
    Assert(data.size() > 0, ExcMessage("No data was inserted."));

    if (data.size() == 1)
      {
        return data.back().second;
      }
    else if (data.size() == 2)
      {
        const double c_1 =
          (x - data[0].first) / (data[1].first - data[0].first);
        const double c_0 = 1.0 - c_1;

        VectorType result(data[0].second);
        result *= c_0;
        result.add(c_1, data[1].second);

        return result;
      }
    else if (data.size() == 3)
      {
        const double &r0 = data[0].first;
        const double &r1 = data[1].first;
        const double &r2 = data[2].first;

        const double c_0 = ((r1 - x) * (r2 - x)) / ((r0 - r1) * (r0 - r2));
        const double c_1 = -((r0 - x) * (r2 - x)) / ((r0 - r1) * (r1 - r2));
        const double c_2 = ((r0 - x) * (r1 - x)) / ((r0 - r2) * (r1 - r2));

        VectorType result(data[0].second);
        result *= c_0;
        result.add(c_1, data[1].second);
        result.add(c_2, data[2].second);

        return result;
      }
    else if (data.size() == 4)
      {
        const double &r0 = data[0].first;
        const double &r1 = data[1].first;
        const double &r2 = data[2].first;
        const double &r3 = data[3].first;

        const double c_0 = -((r1 - x) * (r2 - x) * (r3 - x)) /
                           ((r0 - r1) * (r0 - r2) * (r0 - r3));
        const double c_1 = ((r0 - x) * (r2 - x) * (r3 - x)) /
                           ((r0 - r1) * (r1 - r2) * (r1 - r3));
        const double c_2 = -((r0 - x) * (r1 - x) * (r3 - x)) /
                           ((r0 - r2) * (r1 - r2) * (r2 - r3));
        const double c_3 = ((r0 - x) * (r1 - x) * (r2 - x)) /
                           ((r0 - r3) * (r1 - r3) * (r2 - r3));

        VectorType result(data[0].second);
        result *= c_0;
        result.add(c_1, data[1].second);
        result.add(c_2, data[2].second);
        result.add(c_3, data[3].second);

        return result;
      }

    Assert(false, ExcLifexNotImplemented());

    // To suppress compiler warnings.
    VectorType dummy;
    return dummy;
  }

  template <class VectorType>
  void
  PolynomialExtrapolation<VectorType>::add(const double &x, const VectorType &y)
  {
    data.push_back(std::make_pair(x, y));

    if (data.size() > order)
      data.pop_front();
  }

  /// Explicit instantiation.
  template class PolynomialExtrapolation<LinAlg::MPI::Vector>;

  /// Explicit instantiation.
  template class PolynomialExtrapolation<LinAlg::MPI::BlockVector>;
} // namespace lifex::utils