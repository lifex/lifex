/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Roberto Piersanti <roberto.piersanti@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/numerics/numbers.hpp"
#include "source/numerics/quaternions.hpp"

#include <vector>

namespace lifex::utils
{
  Quaternion
  operator+(const Quaternion &q1, const Quaternion &q2)
  {
    return Quaternion(q1.a + q2.a, q1.b + q2.b, q1.c + q2.c, q1.d + q2.d);
  }

  Quaternion
  operator-(const Quaternion &q1, const Quaternion &q2)
  {
    return Quaternion(q1.a - q2.a, q1.b - q2.b, q1.c - q2.c, q1.d - q2.d);
  }

  Quaternion
  operator*(const Quaternion &q1, const Quaternion &q2)
  {
    return Quaternion(q1.a * q2.a - q1.b * q2.b - q1.c * q2.c - q1.d * q2.d,
                      q1.a * q2.b + q1.b * q2.a + q1.c * q2.d - q1.d * q2.c,
                      q1.a * q2.c - q1.b * q2.d + q1.c * q2.a + q1.d * q2.b,
                      q1.a * q2.d + q1.b * q2.c - q1.c * q2.b + q1.d * q2.a);
  }

  double
  quaternion_dot(const Quaternion &q1, const Quaternion &q2)
  {
    return q1.a * q2.a + q1.b * q2.b + q1.c * q2.c + q1.d * q2.d;
  }

  double
  quaternion_norm(const Quaternion &q)
  {
    return std::sqrt(quaternion_dot(q, q));
  }

  Quaternion
  quaternion_normalize(const Quaternion &q)
  {
    const double norm = quaternion_norm(q);

    Quaternion result;

    if (!utils::is_zero(norm))
      {
        result.a = q.a / norm;
        result.b = q.b / norm;
        result.c = q.c / norm;
        result.d = q.d / norm;
      }
    else
      {
        result.a = 0.0;
        result.b = 0.0;
        result.c = 0.0;
        result.d = 0.0;
      }

    return result;
  }

  /// @cond DOXYGEN_SKIP

  template <>
  Tensor<2, 3, double>
  quaternion_to_rotation<3>(const Quaternion &q, const bool &normalize)
  {
    const double &a = q.a;
    const double &b = q.b;
    const double &c = q.c;
    const double &d = q.d;

    const double norm2 = normalize ? a * a + b * b + c * c + d * d : 1.0;

    Tensor<2, 3, double> Q;

    Q[0][0] = 1 - 2 * (c * c + d * d) / norm2;
    Q[0][1] = 2 * (b * c - d * a) / norm2;
    Q[0][2] = 2 * (b * d + c * a) / norm2;
    Q[1][0] = 2 * (b * c + d * a) / norm2;
    Q[1][1] = 1 - 2 * (b * b + d * d) / norm2;
    Q[1][2] = 2 * (c * d - b * a) / norm2;
    Q[2][0] = 2 * (b * d - c * a) / norm2;
    Q[2][1] = 2 * (c * d + b * a) / norm2;
    Q[2][2] = 1 - 2 * (b * b + c * c) / norm2;

    return Q;
  }

  template <>
  Tensor<2, 2, double>
  quaternion_to_rotation<2>(const Quaternion &q, const bool &normalize)
  {
    Tensor<2, 3, double> Q_3d = quaternion_to_rotation<3>(q, normalize);

    // We check that the last row and column of the result are those of the
    // identity tensor.
    Assert(utils::is_zero(Q_3d[2][0]) && utils::is_zero(Q_3d[2][1]) &&
             utils::is_zero(Q_3d[0][2]) && utils::is_zero(Q_3d[1][2]) &&
             utils::is_equal(Q_3d[2][2], 1.0),
           ExcMessage(
             "The provided quaternion does not represent a 2D rotation."));

    Tensor<2, 2, double> result;
    for (unsigned int i = 0; i < 2; ++i)
      for (unsigned int j = 0; j < 2; ++j)
        result[i][j] = Q_3d[i][j];

    return result;
  }

  /// @endcond

  Quaternion
  rotation_to_quaternion(const Tensor<2, 3, double> &Q)
  {
    Quaternion   q;
    const double t = trace(Q);

    if (t > 0)
      {
        const double r = std::sqrt(1.0 + t);
        const double s = 0.5 / r;

        q.a = 0.5 * r;
        q.b = (Q[2][1] - Q[1][2]) * s;
        q.c = (Q[0][2] - Q[2][0]) * s;
        q.d = (Q[1][0] - Q[0][1]) * s;
      }
    else if (Q[0][0] > Q[1][1] && Q[0][0] > Q[2][2])
      {
        const double s = 2 * std::sqrt(1.0 + Q[0][0] - Q[1][1] - Q[2][2]);

        q.a = (Q[2][1] - Q[1][2]) / s;
        q.b = 0.25 * s;
        q.c = (Q[0][1] + Q[1][0]) / s;
        q.d = (Q[0][2] + Q[2][0]) / s;
      }
    else if (Q[1][1] > Q[0][0] && Q[1][1] > Q[2][2])
      {
        const double s = 2 * std::sqrt(1.0 + Q[1][1] - Q[0][0] - Q[2][2]);

        q.a = (Q[0][2] - Q[2][0]) / s;
        q.b = (Q[0][1] + Q[1][0]) / s;
        q.c = 0.25 * s;
        q.d = (Q[1][2] + Q[2][1]) / s;
      }
    else
      {
        const double s = 2 * std::sqrt(1.0 + Q[2][2] - Q[0][0] - Q[1][1]);

        q.a = (Q[1][0] - Q[0][1]) / s;
        q.b = (Q[0][2] + Q[2][0]) / s;
        q.c = (Q[1][2] + Q[2][1]) / s;
        q.d = 0.25 * s;
      }

    return quaternion_normalize(q);
  }

  Quaternion
  rotation_to_quaternion(const Tensor<2, 2, double> &Q)
  {
    Tensor<2, 3, double> Q_3d;

    for (unsigned int i = 0; i < 2; ++i)
      for (unsigned int j = 0; j < 2; ++j)
        Q_3d[i][j] = Q[i][j];

    Q_3d[2][2] = 1.0;

    return rotation_to_quaternion(Q_3d);
  }

  Quaternion
  slerp(const Quaternion &q0_, const Quaternion &q1_, const double &t)
  {
    const Quaternion q0 = quaternion_normalize(q0_);
    Quaternion       q1 = quaternion_normalize(q1_);

    double dot = quaternion_dot(q0, q1);

    if (utils::is_negative(dot))
      {
        q1 = q1 * (-1);
        dot *= (-1);
      }

    const double tol = 1e-8;

    if (dot > 1 - tol)
      {
        return quaternion_normalize(q0 + t * (q1 - q0));
      }

    const double theta_0 = std::acos(dot); // angle between input vectors.

    const double theta = theta_0 * t; // angle between v0 and result.

    const double sin_theta   = std::sin(theta);
    const double sin_theta_0 = std::sin(theta_0);

    const double s1 = sin_theta / sin_theta_0;
    const double s0 =
      std::cos(theta) - dot * s1; // = sin(theta_0 - theta) / sin(theta_0)

    return quaternion_normalize(s0 * q0 + s1 * q1);
  }

  Tensor<2, 3, double>
  bislerp(const Tensor<2, 3, double> &Qa,
          const Tensor<2, 3, double> &Qb,
          const double                t)
  {
    const Quaternion i = Quaternion(0, 1, 0, 0);
    const Quaternion j = Quaternion(0, 0, 1, 0);
    const Quaternion k = Quaternion(0, 0, 0, 1);

    const Quaternion qa = rotation_to_quaternion(Qa);
    const Quaternion qb = rotation_to_quaternion(Qb);

    // We check qa * i, qa * j, qa * k instead of i * qa, j * qa, k * qa as in
    // the original paper. That is because we want to express rotations of the
    // axis system represented by qa around its principal axes, not around the
    // Cartesian axes x, y, z.
    const std::vector<Quaternion> candidates = {qa, qa * i, qa * j, qa * k};

    size_t max_idx = 0;

    double max_norm = std::abs(quaternion_dot(qa, qb));

    for (size_t n = 0; n < candidates.size(); ++n)
      {
        double tmp = std::abs(quaternion_dot(candidates[n], qb));
        if (tmp > max_norm)
          {
            max_norm = tmp;
            max_idx  = n;
          }
      }

    return quaternion_to_rotation<3>(slerp(candidates[max_idx], qb, t));
  }

  Tensor<2, 3, double>
  orient(const Tensor<2, 3, double> &Q, const double &alpha, const double &beta)
  {
    const double sina = std::sin(alpha);
    const double cosa = std::cos(alpha);

    const double sinb = std::sin(beta);
    const double cosb = std::cos(beta);

    Tensor<2, 3, double> Q1;

    Q1[0][0] = Q[0][0] * cosa + Q[0][1] * sina;
    Q1[0][1] = -Q[0][0] * sina * cosb + Q[0][1] * cosa * cosb - Q[0][2] * sinb;
    Q1[0][2] = -Q[0][0] * sina * sinb + Q[0][1] * cosa * sinb + Q[0][2] * cosb;
    Q1[1][0] = Q[1][0] * cosa + Q[1][1] * sina;
    Q1[1][1] = -Q[1][0] * sina * cosb + Q[1][1] * cosa * cosb - Q[1][2] * sinb;
    Q1[1][2] = -Q[1][0] * sina * sinb + Q[1][1] * cosa * sinb + Q[1][2] * cosb;
    Q1[2][0] = Q[2][0] * cosa + Q[2][1] * sina;
    Q1[2][1] = -Q[2][0] * sina * cosb + Q[2][1] * cosa * cosb - Q[2][2] * sinb;
    Q1[2][2] = -Q[2][0] * sina * sinb + Q[2][1] * cosa * sinb + Q[2][2] * cosb;

    return Q1;
  }

} // namespace lifex::utils
