/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_UTILS_RBF_INTERPOLATION_HPP_
#define LIFEX_UTILS_RBF_INTERPOLATION_HPP_

#include "source/core_model.hpp"
#include "source/quadrature_evaluation.hpp"

#include "source/geometry/geodesic_distance.hpp"
#include "source/geometry/mesh_handler.hpp"

#include "source/numerics/linear_solver_handler.hpp"
#include "source/numerics/numbers.hpp"
#include "source/numerics/preconditioner_handler.hpp"

#include <algorithm>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace lifex::utils
{
  /**
   * @brief Perform interpolation between different meshes using radial basis
   * functions.
   *
   * Given a function defined on a source mesh @f$\mathcal{M}^0@f$, this
   * class allows computing its radial basis function interpolant on a different
   * mesh @f$\mathcal{M}^1@f$.
   *
   * This class should not be used directly. Instead, one of its derived classes
   * should be used, depending on the interpolation method of interest. See the
   * documentation below for more details.
   *
   * ## Mathematical background
   *
   * **References**: @refcite{deparis2014rescaled, Deparis et al. (2014)},
   * @refcite{salvador2020intergrid, Salvador et al. (2020)},
   * @pubcite{bucelli2023preserving, Bucelli et al. (2023)}.
   *
   * Let @f$f(\mathbf{x})@f$ be a function defined on the mesh
   * @f$\mathcal{M}^0@f$. Let @f$\mathbf{f}^0@f$ be the vector of its
   * evaluations at a set of points @f$\mathbf{x}^0_i@f$, with @f$i = 0, 1,
   * \dots, n^0@f$, i.e. @f$\mathbf{f}^0_i = f(\mathbf{x}^0_i)@f$.
   *
   * Let @f$\mathcal{M}^1@f$ be a different mesh (not necessarily nested within
   * @f$\mathcal{M}^0@f$, and possibly with a different type of elements or
   * finite element space. The radial-basis-function (RBF) interpolant of
   * @f$f(\mathbf x)@f$ is defined as:
   * @f[
   * \Pi_f(\mathbf x) = \sum_{i = 0}^{n^0} \gamma_i^f \phi(\|\mathbf{x} -
   * \mathbf{x}^0_i\|, r)\;,
   * @f]
   * where @f$\gamma_i^f@f$ are the (unknown) interpolation coefficients, and
   * @f$\phi(t, r)@f$ are Wendland's @f$C^2@f$ radial basis function with
   * support @f$r@f$, defined as
   * @f[
   * \phi(t, r) = \max\left\{1-\frac{t}{r}, 0\right\}^4
   * \left(1+4\frac{t}{r}\right)\;.
   * @f]
   *
   * The coefficients in the interpolant are computed by imposing
   * @f$\Pi_f(\mathbf x^0_i) = \mathbf f^0_i@f$ for all @f$i = 0, 1, \dots,
   * n^0@f$, leading to the linear system
   * @f[
   * \begin{gathered}
   * \Phi^\text{int}\boldsymbol{\gamma}^f = \mathbf f^0\;, \\
   * \Phi^\text{int}_{ij} = \phi(\|\mathbf x^0_i - \mathbf x^0_j\|, r)\;.
   * \end{gathered}
   * @f]
   *
   * Once the interpolant is constructed, it can be evaluated on any point of
   * @f$\mathbb{R}^3@f$, and in particular it can be evaluated on the mesh
   * @f$\mathcal{M}^1@f$. Let @f$\mathbf{x}^1_i@f$ be the set of points on which
   * the interpolant must be evaluated, with @f$i = 0, 1, \dots, n^1@f$. Then,
   * @f[
   * \mathbf{f}^1_i = \Pi_f(\mathbf{x}^1_i) = \sum_{j = 0}^{n^0} \gamma_i^f
   * \phi(\|\mathbf{x}^1_i - \mathbf{x}^0_j\|, r) \qquad i = 0, 1, \dots, n^1\;,
   * @f]
   * which can be compactly expressed as
   * @f[
   * \begin{gathered}
   * \mathbf{f}^1 = \Phi^\text{eval}\boldsymbol{\gamma}^f\;, \\
   * \Phi^\text{eval}_{ij} = \phi(\|\mathbf x^1_i - \mathbf x^0_j\|, r)\;.
   * \end{gathered}
   * @f]
   *
   * The points @f$\mathbf{x}^0_i@f$ and @f$\mathbf{x}^1_i@f$ can be either the
   * support points of the degrees of freedom of the mesh
   * (@ref RBFInterpolationDoFs) or quadrature points within mesh elements
   * (@ref RBFInterpolationQuadrature).
   *
   * ### Rescaling
   * Depending on the choice of the support radius @f$r@f$, the interpolant may
   * show significant oscillations. To mitigate this effect, it can be rescaled
   * by the interpolant of the constant function @f$g(x) = 1@f$. This leads to
   * the definition of a new interpolant:
   * @f[
   * \Pi_f^\text{rescaled}(\mathbf x) = \frac{\Pi_f(\mathbf x)}{\Pi_g(\mathbf
   * x)}\;.
   * @f]
   * This behavior can be turned on in the parameter file by setting
   * <kbd>Rescale = true</kbd>.
   *
   * ### Adaptive selection of the RBF support radius
   * The RBF support @f$r@f$ may be different for each point @f$\mathbf x^0_i@f$
   * in the source mesh @f$\mathcal{M}^0@f$, to account for their possibly
   * inhomogeneous spatial distribution.
   *
   * This behavior can be activated by setting <kbd>Adaptive RBF support =
   * true</kbd>. If so, on each point @f$x^0_i@f$, the support radius @f$r_i@f$
   * is such that the sphere of radius @f$r_i@f$ centered at @f$x^0_i@f$
   * contains at least @f$N@f$ other points of mesh @f$\mathcal{M}^0@f$. The
   * number @f$N@f$ is controlled by the parameter <kbd>Adaptive RBF
   * neighbors</kbd>. If @f$x_0^j@f$ is the @f$N@f$-th nearest neighbor to
   * @f$x_0^i@f$, then @f$r_i@f$ is defined as
   * @f[
   *   r_i = \alpha \|x_0^i - x_0^j\|\;,
   * @f]
   * where @f$\alpha@f$ is a user-defined support increase factor, controlled by
   * the parameter <kbd>Support increase factor</kbd>.
   *
   * If instead <kbd>Adaptive RBF support = false</kbd>, the support radius is
   * constant and controlled by the parameter <kbd>RBF support radius</kbd>.
   *
   * ## Geodesic distance cutoff
   * For non-convex domains, it may be possible that two points are close in
   * Euclidean distance, but they belong to disconnected parts of the mesh
   * (consider for example a c-shaped domain). In that case, it might be
   * desirable to prevent those points from interacting in the interpolant (i.e.
   * setting the corresponding entries in @f$\Phi^\text{int}@f$ or
   * @f$\Phi^\text{eval}@f$ to zero).
   *
   * To this end, this class allows to redefine the computation of the distance
   * as follows. Let @f$g(\mathbf{x}, \mathbf{y})@f$ be the geodesic distance as
   * computed by @ref utils::GeodesicDistance. Then, we define the distance
   * between two points @f$\mathbf{x}@f$ and @f$\mathbf{y}@f$ as
   * @f[
   * d(\mathbf{x}, \mathbf{y}) = \begin{cases}
   *   \infty & \text{if } g(\mathbf{x}, \mathbf{y}) \geq r\;, \\
   *   g(\mathbf{x}, \mathbf{y}) & \text{if } g(\mathbf{x}, \mathbf{y}) > \beta
   *     h_\text{max}\;, \\
   *   \|\mathbf{x} - \mathbf{y}\| & \text{otherwise}\;,
   * \end{cases}
   * @f]
   * where @f$r@f$ is the RBF support radius associated to either @f$\mathbf
   * x@f$ or @f$\mathbf y@f$ (depending on the need), @f$\beta > 0@f$ is a
   * user-defined threshold to detect high-curvature regions (see
   * @ref prm_high_curvature_threshold) and @f$h_\text{max}@f$ is the maximum
   * diameter of the elements of the reference mesh used for geodesic distance
   * evaluation.
   *
   * ## Implementation notes
   * The matrices @f$\Phi^\text{int}@f$ and @f$\Phi^\text{eval}@f$ depend only
   * on the locations of support points in the two meshes, and not on the
   * function being interpolated. Therefore, they are computed only once when
   * calling the @ref setup_system_internal method. This is a very costly
   * operation, and it involves a large amount of communication between
   * processors. The method also computes local RBF support, if adaptive support
   * is enabled, and the interpolant of @f$g(x) = 1@f$ if rescaling is enabled.
   *
   * After that, multiple functions can be interpolated by calling the
   * @ref interpolate_internal method. This step involves the solution of a
   * linear system for the interpolation coefficients.
   *
   * ## Advanced parameters
   *
   * The following parameters are only visible if generating the parameter file
   * with the <kbd>-g full</kbd> flag.
   *
   * The assembly of the interpolant requires to communicate points between
   * parallel processes. To determine if a point is relevant to a process
   * @f$r@f$ (i.e. if it might fall within the support of one of the RBFs owned
   * by @f$r@f$), we construct a coarse representation of the source points
   * stored by @f$r@f$. The coarse representation is the union of axis-aligned
   * bounding boxes such that it contains all points. The number of bounding
   * boxes (and hence the accuracy of the representation) is controlled by the
   * parameter <kbd>Number of bounding box subdivisions</kbd>.
   *
   * The interpolation and evaluation matrices may have a large number of
   * elements. For algorithmic reasons, the matrices are assembled by column
   * (i.e. each process assembles the columns of the points it owns, rather than
   * the rows), although they are partitioned by rows. This means that, at the
   * end of assembly, there may be a lot of inter-process communication. If the
   * matrices are very large, this may lead to out-of-memory issues or
   * segfaults. To avoid this, the communication can be split over a few calls,
   * instead of only one. This is controlled by the parameter <kbd>Number of
   * compress calls during assembly</kbd>.
   */
  class RBFInterpolation : public CoreModel
  {
  public:
    /// Alias for a map from indices to points.
    using PointMap = std::map<types::global_dof_index, Point<dim>>;

    /// Class that extends a Point by adding its index and the index of the
    /// closest point in the geodesic reference mesh.
    class IndexPoint : public Point<dim>
    {
    public:
      /// Default constructor.
      IndexPoint() = default;

      /// Constructor.
      IndexPoint(const PointMap::value_type &p)
        : Point<dim>(p.second)
        , id(p.first)
        , geodesic_closest_point(0)
      {}

      /// Serialization to a Boost Archive.
      template <class Archive>
      void
      serialize(Archive &ar, const unsigned int version)
      {
        Point<dim>::serialize(ar, version);
        ar &id;
        ar &geodesic_closest_point;
      }

      /// Index.
      types::global_dof_index id;

      /// Nearest point in the geodesic distance mesh.
      types::global_dof_index geodesic_closest_point;
    };

    /// Alias for a vector of indexed points.
    using PointVec = std::vector<IndexPoint>;

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /// Set the mesh to use for geodesic distance evaluation.
    void
    set_geodesic_reference_mesh(
      const std::shared_ptr<const utils::MeshHandler<dim>>
        &geodesic_reference_mesh_)
    {
      geodesic_reference_mesh = geodesic_reference_mesh_;
    }

  protected:
    /// Constructor. It is made protected so that this class can be instantiated
    /// only by derived classes.
    RBFInterpolation(const std::string &subsection);

    /// Serialize the interpolation and evaluation matrices to a file.
    void
    serialize_matrices(const std::string &filename) const;

    /// Deserialize the interpolation and evaluation matrices from a file.
    void
    deserialize_matrices(const std::string &filename);

    /**
     * @brief Setup the interpolator.
     *
     * This assembles the matrices @f$\Phi^\text{int}@f$ and
     * @f$\Phi^\text{eval}@f$. If required, it also computes the adaptive
     * support radius and the interpolant of the constant function @f$g(x) =
     * 1@f$ for rescaling.
     *
     * This method has a significant computational cost, and it involves a lot
     * of communication between processes. Therefore, it should be used
     * sparingly.
     *
     * @note The arguments points_src and points_dst are deliberately passed by
     * non-const reference, since this method may set the geodesic_closest_point
     * member of their elements.
     */
    void
    setup_system_internal(std::vector<PointVec> &points_src,
                          std::vector<PointVec> &points_dst);

    /**
     * @brief Compute the interpolant.
     *
     * This solves the linear system to compute the interpolation coefficients,
     * and then evaluates the interpolant onto the nodes of the mesh
     * @f$\mathcal{M}^1@f$.
     *
     * @param[out] dst Vector where the result of the interpolation should be
     * stored. The vector must already be properly initialized, and must not
     * contain ghost elements.
     * @param[in] src Vector storing the finite element field to be interpolated.
     * The vector must already be properly initialized, and must not contain
     * ghost elements.
     * @param[in] disable_rescaling If set to true, rescaling is disabled
     * (overriding the setting @ref prm_rescale defined through parameters).
     */
    void
    interpolate_internal(LinAlg::MPI::Vector       &dst,
                         const LinAlg::MPI::Vector &src,
                         const bool                &disable_rescaling = false);

    /// Evaluate the radial basis function with a given support.
    double
    rbf(const double &r, const double &support) const;

    /// Indices of owned points on the source mesh.
    IndexSet owned_points_src;

    /// Indices of owned points on the destination mesh.
    IndexSet owned_points_dst;

    /// Interpolation matrix @f$\Phi^\text{int}@f$.
    LinAlg::MPI::SparseMatrix interpolation_matrix;

    /// Linear solver used to invert the interpolation matrix
    /// @f$\Phi^\text{int}@f$.
    LinearSolverHandler<LinAlg::MPI::Vector> linear_solver;

    /// Evaluation matrix @f$\Phi^\text{eval}@f$.
    LinAlg::MPI::SparseMatrix evaluation_matrix;

    /// Interpolation coefficients @f$\boldsymbol{\gamma}^f@f$.
    LinAlg::MPI::Vector interpolation_coefficients_owned;

    /// Vector interpolating the constant function @f$g(x) = 1@f$ (used if
    /// rescaling is enabled).
    LinAlg::MPI::Vector interpolant_of_one_owned;

    /// Preconditioner matrix.
    LinAlg::MPI::SparseMatrix preconditioner_matrix;

    /// Preconditioner handler.
    PreconditionerHandler preconditioner_handler;

    /// Geodesic distance evaluator.
    GeodesicDistance<dim, dim> geodesic_distance;

    /// Mesh used for the evaluation of the geodesic distance.
    std::shared_ptr<const utils::MeshHandler<dim>> geodesic_reference_mesh;

    /// @name Parameters read from file.
    /// @{

    /// Toggle geodesic distance cutoff.
    bool prm_geodesic_cutoff_enable;

    /// Toggle adaptive RBF support.
    bool prm_adaptive_rbf_support;

    /// Number @f$N@f$ of neighbors included in the adaptive support.
    unsigned int prm_adaptive_neighbors;

    /// Support increase factor @f$c@f$.
    double prm_support_increase_factor;

    /// Maximum of adaptive RBF support.
    double prm_max_adaptive_support;

    /// Support @f$r@f$ of the radial basis functions.
    double prm_rbf_support;

    /// Toggle rescaling the interpolant by the interpolant of the constant
    /// function @f$g(x) = 1@f$.
    bool prm_rescale;

    /// Relative tolerance used in assembling the preconditioner.
    double prm_prec_reduction;

    /// Toggle serialization of the matrices to file.
    bool prm_serialize_matrices;

    /// Filename used for the serialization of matrices.
    std::string prm_serialization_filename;

    /// Toggle deserialization of the matrices from file.
    bool prm_deserialize_matrices;

    /// Filename used for the deserialization of matrices.
    std::string prm_deserialization_filename;

    /// Type of preconditioner being used.
    std::string prm_preconditioner_type;

    /**
     * @brief Relative threshold to detect high curvature regions.
     *
     * This parameter is only meaningful if geodesic distance cutoff is enabled.
     * When evaluating the distance between two points, we check whether the
     * shortest path that connects them is at high curvature by comparing their
     * Euclidean distance @f$d@f$ with their geodesic distance @f$g@f$. If
     */
    double prm_high_curvature_threshold;

    /// Number of subdivisions used when building the coarse bounding box
    /// representation of the processes on current subdomain.
    unsigned int prm_n_bbox_subdivisions;

    /**
     * @brief Number of calls to compress when assembling matrices.
     *
     * Each process is filling the matrices by column, rather than by row. This
     * is convenient because it reduces the computations and the need for
     * communication. However, this also means that a lot of data needs to be
     * transferred when calling matrix.compress. We observed that this leads
     * to segfaults/out of memory issues for large problems. As a workaround,
     * we compress the matrix a few times, rather than only once at the end of
     * assembly.
     *
     * @todo Is there a smarter solution?
     */
    unsigned int prm_n_matrix_compress_calls;

    /// @}
  };

  /**
   * @brief Radial basis function interpolation at DoF support points.
   *
   * This specializes the class @ref RBFInterpolation to the case where the
   * interpolation and evaluation points are the support points of the degrees
   * of freedom on the source and destination meshes.
   */
  class RBFInterpolationDoFs : public RBFInterpolation
  {
  public:
    /// Constructor.
    RBFInterpolationDoFs(const std::string &subsection);

    /// Setup.
    void
    setup_system(const DoFHandler<dim> &dof_handler_src,
                 const DoFHandler<dim> &dof_handler_dst);

    /// Compute the interpolation.
    void
    interpolate(LinAlg::MPI::Vector &dst, const LinAlg::MPI::Vector &src)
    {
      interpolate_internal(dst, src, false);
    }

  protected:
    /// Compute points for a given DoF handler (interpolation or evaluation).
    void
    compute_points(const DoFHandler<dim> &dof_handler,
                   PointVec              &owned_points,
                   const unsigned int    &component) const;
  };

  /**
   * @brief Radial basis function interpolation at quadrature nodes.
   *
   * This specializes the class @ref RBFInterpolation to the case where the
   * interpolation and evaluation points are quadrature points within the mesh
   * elements.
   *
   * The class inherits from @ref QuadratureEvaluation, so that the interpolated
   * values can be accessed using the same interface.
   *
   * This class is purely virtual, since it does not define its call operator
   * (see @ref QuadratureEvaluation). The user must define a derived class that
   * evaluates the result as needed. See the source code for
   * @ref examples::ExampleRBFInterpolation for an example of use.
   */
  template <class OutputType>
  class RBFInterpolationQuadrature : public RBFInterpolation,
                                     public QuadratureEvaluationFEM<OutputType>
  {
  public:
    /// Constructor.
    RBFInterpolationQuadrature(const std::string &subsection);

    /// Setup system.
    void
    setup_system(const unsigned int    &n_components_,
                 const DoFHandler<dim> &dof_handler_src_,
                 const Quadrature<dim> &quadrature_src,
                 const DoFHandler<dim> &dof_handler_dst,
                 const Quadrature<dim> &quadrature_dst);

    /// Interpolate from a quadrature evaluation object. The interpolation
    /// result is stored internally, and can be accessed through the interface
    /// of the base class QuadratureEvaluation.
    void
    interpolate(QuadratureEvaluationScalar &src);

    /// Same as above but for vectorial data. The QuadratureEvaluationVector
    /// object must return vectors with as many entries as the n_components
    /// argument to setup_system.
    void
    interpolate(QuadratureEvaluation<Vector<double>> &src);

  protected:
    /// Compute points for a given DoF handler (interpolation or evaluation).
    void
    compute_points(const DoFHandler<dim> &dof_handler,
                   PointVec              &owned_points,
                   const Quadrature<dim> &quadrature) const;

    /// Callback after init.
    virtual void
    post_init_callback() override;

    /// Callback after reinit.
    virtual void
    post_reinit_callback(
      const DoFHandler<dim>::active_cell_iterator &cell_other) override;

    /// Number of components to be interpolated.
    unsigned int n_components;

    /// Pointer to the source DoF handler.
    const DoFHandler<dim> *dof_handler_src;

    /// Number of quadrature points on the source mesh.
    unsigned int n_q_src = 0;

    /// Offset of the indices of the points owned by current processor on the
    /// source mesh.
    unsigned int offset_src = 0;

    /// Offset of the indices of the points owned by current processor on the
    /// destination mesh.
    unsigned int offset_dst = 0;

    /// Offset of current cell.
    unsigned int cell_offset;

    /// Flag that marks whether reinit has already been called since last init.
    bool reinit_called;

    /// Interpolated vector, one for each interpolated component.
    std::vector<LinAlg::MPI::Vector> interpolated_vector;
  };
} // namespace lifex::utils


/// @cond DOXYGEN_SKIP

// These define the traits needed to use the IndexPoint class with
// Boost::geometry::index utilities.
namespace boost::geometry::traits
{
  template <>
  struct tag<lifex::utils::RBFInterpolation::IndexPoint>
  {
    using type = point_tag;
  };

  template <>
  struct coordinate_type<lifex::utils::RBFInterpolation::IndexPoint>
  {
    using type = double;
  };

  template <>
  struct coordinate_system<lifex::utils::RBFInterpolation::IndexPoint>
  {
    using type = boost::geometry::cs::cartesian;
  };

  template <>
  struct dimension<lifex::utils::RBFInterpolation::IndexPoint>
    : boost::mpl::int_<lifex::dim>
  {};

  template <std::size_t d>
  struct access<lifex::utils::RBFInterpolation::IndexPoint, d>
  {
    static inline double
    get(const lifex::utils::RBFInterpolation::IndexPoint &p)
    {
      return p[d];
    }

    static inline void
    set(lifex::utils::RBFInterpolation::IndexPoint &p, const double &value)
    {
      p[d] = value;
    }
  };
} // namespace boost::geometry::traits

/// @endcond

#endif
