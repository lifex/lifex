/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/io/hdf5_io.hpp"

#include "source/numerics/rbf_interpolation.hpp"
#include "source/numerics/tools.hpp"

#include <deal.II/base/bounding_box.h>

#include <deal.II/numerics/rtree.h>

#include <boost/filesystem.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/box.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/serialization/set.hpp>

#include <fstream>
#include <limits>

namespace lifex::utils
{
  namespace
  {
    /**
     * @brief Constructs a BoundingBox from a PointVec.
     */
    BoundingBox<dim>
    bbox_from_index_points(const RBFInterpolation::PointVec &points)
    {
      BoundingBox<dim> box;
      Point<dim>      &lower = box.get_boundary_points().first;
      Point<dim>      &upper = box.get_boundary_points().second;

      if (points.size() > 0)
        {
          lower = points.front();
          upper = points.front();
        }

      for (const auto &p : points)
        for (unsigned int d = 0; d < dim; ++d)
          {
            lower[d] = std::min(lower[d], p[d]);
            upper[d] = std::max(upper[d], p[d]);
          }

      return box;
    }

    /**
     * @brief Tightens the a bounding box around the points in a PointVec.
     *
     * This takes in input a bounding box and a map of points, finds the points
     * within the map that are inside the box and returns the bounding box of
     * those points.
     */
    BoundingBox<dim>
    tighten_bbox(const BoundingBox<dim>           &box,
                 const RBFInterpolation::PointVec &points)
    {
      Point<dim> new_lower, new_upper;

      for (unsigned int d = 0; d < dim; ++d)
        {
          new_lower[d] = std::numeric_limits<double>::max();
          new_upper[d] = -std::numeric_limits<double>::max();
        }

      for (const auto &p : points)
        if (box.point_inside(p))
          {
            for (unsigned int d = 0; d < dim; ++d)
              {
                new_lower[d] = std::min(new_lower[d], p[d]);
                new_upper[d] = std::max(new_upper[d], p[d]);
              }
          }

      return BoundingBox<dim>(std::make_pair(new_lower, new_upper));
    }

    /**
     * @brief Splits a BoundingBox in two.
     *
     * It determines the direction along which the bounding box is the longest,
     * then splits in half along that direction. Finally, the two resulting
     * boxes are made tighter using the function tigthen_bbox.
     */
    std::pair<BoundingBox<dim>, BoundingBox<dim>>
    split_bbox(const BoundingBox<dim>           &box,
               const RBFInterpolation::PointVec &points)
    {
      const auto &lower = box.get_boundary_points().first;
      const auto &upper = box.get_boundary_points().second;

      // Find the direction with the longest side length.
      unsigned int split_dir        = 0;
      double       split_dir_length = upper[split_dir] - lower[split_dir];

      for (unsigned int d = 0; d < dim; ++d)
        if (upper[d] - lower[d] > split_dir_length)
          {
            split_dir        = d;
            split_dir_length = upper[d] - lower[d];
          }

      const double split_point = 0.5 * (lower[split_dir] + upper[split_dir]);

      // Create the new boxes.
      std::pair<BoundingBox<dim>, BoundingBox<dim>> result =
        std::make_pair(box, box);
      result.first.get_boundary_points().second[split_dir] = split_point;
      result.second.get_boundary_points().first[split_dir] = split_point;

      // Tighten the boxes on the data points.
      result.first  = tighten_bbox(result.first, points);
      result.second = tighten_bbox(result.second, points);

      return result;
    }

    /**
     * @brief Construct a set of bounding boxes around a list of points.
     *
     * Given a PointVec, this function:
     * - creates the axis-aligned bounding box of those points;
     * - iteratively calls BoundingBox::split on the boxes, until the original
     * box has been split in half n_subdivisions times;
     * - extends all boxes by the prescribed amount.
     *
     * The result is a vector of 2^n_subdivision BoundingBoxes.
     */
    std::vector<BoundingBox<dim>>
    build_refined_boxes(const RBFInterpolation::PointVec &points,
                        const unsigned int               &n_subdivisions,
                        const double                     &extend_amount)
    {
      std::vector<BoundingBox<dim>> boxes;
      boxes.push_back(bbox_from_index_points(points));

      // The loop starts from 1, since we already have one level of the tree
      // (the initial box).
      for (unsigned int i = 0; i < n_subdivisions; ++i)
        {
          std::vector<BoundingBox<dim>> new_boxes;

          for (const auto &box : boxes)
            {
              auto split_result = split_bbox(box, points);
              new_boxes.push_back(split_result.first);
              new_boxes.push_back(split_result.second);
            }

          boxes = new_boxes;
        }

      for (auto &box : boxes)
        box.extend(extend_amount);

      return boxes;
    }

    /**
     * @brief Intermediate data structure for matrix assembly.
     */
    class MatrixData
    {
    public:
      /// Class to store a matrix row.
      class Row
      {
      public:
        /// Constructor.
        Row() = default;

        /// Add an entry.
        void
        add(const unsigned int &col, const double &val)
        {
          col_indices.push_back(col);
          col_values.push_back(val);
        }

        /// Get the column indices.
        const std::vector<unsigned int> &
        column_indices() const
        {
          return col_indices;
        }

        /// Get the values vector.
        const std::vector<double> &
        values() const
        {
          return col_values;
        }

        /// Get the number of entries.
        unsigned int
        n_entries() const
        {
          return col_indices.size();
        }

      protected:
        /// Column indices.
        std::vector<unsigned int> col_indices;

        /// Values.
        std::vector<double> col_values;
      };

      /// Constructor.
      MatrixData(const unsigned int &n_rows_)
        : n_rows(n_rows_)
      {}

      /// Get a row.
      Row &
      row(const unsigned int &i)
      {
        return rows[i];
      }

      /// Get a row, const overload.
      Row
      row(const unsigned int &i) const
      {
        Assert(i < n_rows, ExcDimensionMismatch(i, n_rows));

        auto row_it = rows.find(i);

        if (row_it != rows.end())
          return row_it->second;
        else
          return Row();
      }

      /// Clear data.
      void
      clear()
      {
        rows.clear();
      }

      /// Fill a SparseMatrix object with the data contained in this object.
      /// The sparsity pattern is reinitialized.
      void
      assemble(const IndexSet            &owned_rows,
               const IndexSet            &relevant_rows,
               const IndexSet            &owned_cols,
               const unsigned int        &n_compress_calls,
               LinAlg::MPI::SparseMatrix &matrix) const
      {
        const unsigned int n_rows = owned_rows.size();
        const unsigned int n_cols = owned_cols.size();

        // Build the sparsity pattern.
        {
          DynamicSparsityPattern dsp(n_rows, n_cols, relevant_rows);

          for (const auto &[i, row] : rows)
            {
              if (row.n_entries() > 0)
                dsp.add_entries(i,
                                row.column_indices().begin(),
                                row.column_indices().end());
            }

          SparsityTools::distribute_sparsity_pattern(dsp,
                                                     owned_rows,
                                                     Core::mpi_comm,
                                                     relevant_rows);

          matrix.reinit(owned_rows, owned_cols, dsp, Core::mpi_comm);
        }

        // Fill the matrix.
        {
          // The (implicitly rounded-down) integer division here is
          // intentional.
          const unsigned int compress_every = n_rows / n_compress_calls;

          for (unsigned int i = 0; i < n_rows; ++i)
            {
              const auto &row = this->row(i);

              if (row.n_entries() > 0)
                matrix.set(i, row.column_indices(), row.values());

              if ((i > 0 && i % compress_every == 0) || i == n_rows - 1)
                matrix.compress(VectorOperation::insert);
            }
        }
      }

    protected:
      /// Number of rows.
      unsigned int n_rows;

      /// Rows.
      std::map<types::global_dof_index, Row> rows;
    };

    /**
     * @brief Write a vector to a binary file.
     */
    template <class T>
    void
    write_vector(std::ofstream &file, const std::vector<T> &vector)
    {
      const auto size = vector.size() * sizeof(T);
      file.write(reinterpret_cast<const char *>(vector.data()), size);
    }

    /**
     * @brief Read a vector from a binary file.
     */
    template <class T>
    std::vector<T>
    read_vector(std::ifstream &file, const unsigned int &size)
    {
      std::vector<T> result(size);

      file.read(reinterpret_cast<char *>(result.data()),
                result.size() * sizeof(T));

      return result;
    }
  } // namespace

  RBFInterpolation::RBFInterpolation(const std::string &subsection)
    : CoreModel(subsection)
    , linear_solver(subsection + " / Linear solver", {"CG", "GMRES"}, "GMRES")
    , preconditioner_handler(subsection + " / Preconditioner")
  {}

  void
  RBFInterpolation::declare_parameters(ParamHandler &params) const
  {
    params.enter_subsection_path(prm_subsection_path);

    params.declare_entry(
      "Enable geodesic cutoff",
      "false",
      Patterns::Bool(),
      "If true, points whose geodesic distance is greater than a given "
      "threshold are considered to be infinitely far.");

    params.enter_subsection("RBF support radius");
    {
      params.declare_entry_selection(
        "Support computation method",
        "Adaptive",
        "Adaptive | Constant",
        "If set to adaptive, the RBF support radius is adaptively selected for "
        "every point in the source mesh, based on the distance from the "
        "nearest neighbors. Otherwise, the support is constant.");

      params.declare_entry(
        "Adaptive RBF neighbors",
        "10",
        Patterns::Integer(1),
        "Number of nearest neighbors included in the support of each RBF, if "
        "adaptive support is enabled.");

      params.declare_entry("Maximum adaptive support",
                           "0.1",
                           Patterns::Double(0),
                           "Maximum value for the adaptive support.");

      params.declare_entry(
        "Support increase factor",
        "2.0",
        Patterns::Double(1.0),
        "If adaptive RBF support is enabled, the support for a given source "
        "point is the distance from its N-th nearest neighbor upscaled by this "
        "factor.");

      params.declare_entry(
        "Constant support",
        "0.1",
        Patterns::Double(0),
        "Support radius of the RBFs used in the interpolation if "
        "Support computation method = Constant.");
    }
    params.leave_subsection();

    params.declare_entry("Rescale",
                         "true",
                         Patterns::Bool(),
                         "If true, the interpolant is rescaled by the "
                         "interpolant of the constant function equal to 1.");

    params.enter_subsection("Serialization");
    {
      params.declare_entry("Serialize matrices to file",
                           "true",
                           Patterns::Bool(),
                           "If true, the interpolation and evaluation matrices "
                           "are serialized to file.");

      params.declare_entry(
        "Serialization filename",
        "rbf-interpolation.h5",
        Patterns::FileName(Patterns::FileName::FileType::output),
        "Name of the file where the matrices will be serialized. The path is "
        "relative to the output directory.");

      params.declare_entry(
        "Deserialize matrices from file",
        "false",
        Patterns::Bool(),
        "If true, the interpolation and evaluation matrices "
        "are deserialized from a file created by a previous run.");

      params.declare_entry(
        "Deserialization filename",
        "",
        Patterns::FileName(Patterns::FileName::FileType::input),
        "Name of the file from which the matrices will be deserialized. The "
        "path is either absolute or relative to the executable path.");
    }
    params.leave_subsection();

    params.set_verbosity(VerbosityParam::Full);
    params.declare_entry("High-curvature threshold",
                         "0.5",
                         Patterns::Double(0),
                         "Relative threshold used to detect high-curvature "
                         "regions in the geodesic reference mesh. Only "
                         "meaningful if enabling geodesic distance cutoff.");

    params.declare_entry(
      "Number of bounding box subdivisions",
      "0",
      Patterns::Integer(0),
      "A coarse representation of the source points is built to facilitate "
      "parallel communication. The higher this parameter, the more refined "
      "this representation.");

    params.declare_entry(
      "Number of compress calls during assembly",
      "1",
      Patterns::Integer(1),
      "Matrix assembly may require a lot of inter-process communication. To "
      "keep it contained, it is possible to subdivide the communicated data in "
      "multiple blocks, indicated by this number.");
    params.reset_verbosity();

    params.leave_subsection_path();

    linear_solver.declare_parameters(params);

    params.enter_subsection_path(
      prm_subsection_path + " / Preconditioner / Approximate cardinal basis");
    {
      params.declare_entry("Local solver reduction",
                           "1e-2",
                           Patterns::Double(0),
                           "Relative tolerance of the local solver used to "
                           "assemble the preconditioner.");
    }
    params.leave_subsection_path();

    preconditioner_handler.declare_parameters(params);

    // Override preconditioner types to add the approximate cardinal basis.
    params.enter_subsection_path(prm_subsection_path + " / Preconditioner");
    params.declare_entry_selection("Preconditioner type",
                                   "AMG",
                                   "Approximate cardinal basis | " +
                                     PreconditionerHandler::types,
                                   "Determine which preconditioner to use");
    params.leave_subsection_path();
  }

  void
  RBFInterpolation::parse_parameters(ParamHandler &params)
  {
    params.parse();

    params.enter_subsection_path(prm_subsection_path);

    prm_geodesic_cutoff_enable = params.get_bool("Enable geodesic cutoff");

    params.enter_subsection("RBF support radius");
    {
      prm_adaptive_rbf_support =
        params.get("Support computation method") == "Adaptive";

      if (prm_adaptive_rbf_support)
        {
          prm_adaptive_neighbors = params.get_integer("Adaptive RBF neighbors");
          prm_support_increase_factor =
            params.get_double("Support increase factor");
          prm_max_adaptive_support =
            params.get_double("Maximum adaptive support");
        }
      else
        {
          prm_rbf_support = params.get_double("Constant support");
        }
    }
    params.leave_subsection();

    prm_rescale = params.get_bool("Rescale");

    params.enter_subsection("Serialization");
    {
      prm_serialize_matrices = params.get_bool("Serialize matrices to file");
      prm_serialization_filename = params.get("Serialization filename");
      prm_deserialize_matrices =
        params.get_bool("Deserialize matrices from file");
      prm_deserialization_filename = params.get("Deserialization filename");
    }
    params.leave_subsection();

    prm_high_curvature_threshold =
      params.get_double("High-curvature threshold");
    prm_n_bbox_subdivisions =
      params.get_integer("Number of bounding box subdivisions");
    prm_n_matrix_compress_calls =
      params.get_integer("Number of compress calls during assembly");

    params.leave_subsection_path();

    linear_solver.parse_parameters(params);

    params.enter_subsection_path(prm_subsection_path + " / Preconditioner");
    {
      prm_preconditioner_type = params.get("Preconditioner type");

      if (prm_preconditioner_type == "Approximate cardinal basis")
        {
          params.enter_subsection("Approximate cardinal basis");
          prm_prec_reduction = params.get_double("Local solver reduction");
          params.leave_subsection();
        }
    }
    params.leave_subsection_path();

    if (prm_preconditioner_type != "Approximate cardinal basis")
      preconditioner_handler.parse_parameters(params);
  }

  void
  RBFInterpolation::serialize_matrices(const std::string &filename) const
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path +
                                       " / Matrix serialization");

    HDF5::File file(prm_output_directory + filename,
                    HDF5::File::FileAccessMode::create,
                    mpi_comm);

    file.set_attribute("mpi_size", mpi_size);

    HDF5::Group interp_group = file.create_group("interpolation_matrix");
    hdf5::write_matrix(interp_group, interpolation_matrix);

    HDF5::Group eval_group = file.create_group("evaluation_matrix");
    hdf5::write_matrix(eval_group, evaluation_matrix);

    if (prm_preconditioner_type == "Approximate cardinal basis")
      {
        HDF5::Group prec_group = file.create_group("preconditioner");
        hdf5::write_matrix(prec_group, preconditioner_matrix);
      }
  }

  void
  RBFInterpolation::deserialize_matrices(const std::string &filename)
  {
    AssertThrow(boost::filesystem::exists(filename),
                ExcMessage("File " + filename + " does not exist."));

    HDF5::File file(filename, HDF5::File::FileAccessMode::open, mpi_comm);

    const unsigned int serialization_mpi_size =
      file.get_attribute<unsigned int>("mpi_size");

    AssertThrow(mpi_size == serialization_mpi_size,
                ExcMessage(
                  "Deserialization of interpolation matrices must be done with "
                  " the same number of processes as serialization "
                  "(serialization processes: " +
                  std::to_string(serialization_mpi_size) +
                  ", deserialization processes: " + std::to_string(mpi_size) +
                  "."));

    HDF5::Group interp_group = file.open_group("interpolation_matrix");
    hdf5::read_matrix(interp_group,
                      interpolation_matrix,
                      owned_points_src,
                      owned_points_src,
                      owned_points_src,
                      mpi_comm);

    HDF5::Group eval_group = file.open_group("evaluation_matrix");
    hdf5::read_matrix(eval_group,
                      evaluation_matrix,
                      owned_points_dst,
                      owned_points_dst,
                      owned_points_src,
                      mpi_comm);

    if (prm_preconditioner_type == "Approximate cardinal basis")
      {
        HDF5::Group prec_group = file.open_group("preconditioner");
        hdf5::read_matrix(prec_group,
                          preconditioner_matrix,
                          owned_points_src,
                          owned_points_src,
                          owned_points_src,
                          mpi_comm);
      }
  }

  void
  RBFInterpolation::setup_system_internal(std::vector<PointVec> &points_src,
                                          std::vector<PointVec> &points_dst)
  {
    namespace bgi = boost::geometry::index;

    const unsigned int n_components = points_src.size();

    Assert(n_components == points_dst.size(),
           ExcDimensionMismatch(n_components, points_dst.size()));

    unsigned int n_points_src = 0;
    unsigned int n_points_dst = 0;

    for (unsigned int c = 0; c < n_components; ++c)
      {
        n_points_src += Utilities::MPI::sum(points_src[c].size(), mpi_comm);
        n_points_dst += Utilities::MPI::sum(points_dst[c].size(), mpi_comm);
      }

    // Compute sets of locally owned points.
    owned_points_src.set_size(n_points_src);
    owned_points_dst.set_size(n_points_dst);
    for (unsigned int c = 0; c < n_components; ++c)
      {
        {
          std::set<types::global_dof_index> indices_src;
          for (const auto &i : points_src[c])
            indices_src.insert(i.id);
          owned_points_src.add_indices(indices_src.begin(), indices_src.end());
        }

        {
          std::set<types::global_dof_index> indices_dst;
          for (const auto &i : points_dst[c])
            indices_dst.insert(i.id);
          owned_points_dst.add_indices(indices_dst.begin(), indices_dst.end());
        }
      }

    pcout << utils::log::separator_section << std::endl;
    pcout << prm_subsection_path << std::endl;
    pcout << "    Source points:      " << n_points_src << std::endl
          << "    Destination points: " << n_points_dst << std::endl;

    if (!prm_deserialize_matrices)
      {
        if (prm_geodesic_cutoff_enable)
          {
            AssertThrow(geodesic_reference_mesh != nullptr,
                        ExcMessage(
                          "You must provide a reference mesh to compute "
                          "the geodesic distance."));

            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path +
                " / Setup system / Setup geodesic distance");

            std::vector<Point<dim>> geodesic_srcs;
            for (const auto &points : points_src)
              for (const auto &p : points)
                geodesic_srcs.push_back(p);

            geodesic_distance.setup_system(geodesic_reference_mesh,
                                           geodesic_srcs,
                                           prm_adaptive_rbf_support ?
                                             prm_max_adaptive_support :
                                             prm_rbf_support);
          }

        double support_min = std::numeric_limits<double>::max();
        double support_max = 0.0;
        double support_avg = 0.0;

        IndexSet relevant_points_src(owned_points_src.size());
        IndexSet relevant_points_dst(owned_points_dst.size());

        MatrixData data_int(n_points_src);
        MatrixData data_eva(n_points_dst);

        for (unsigned int c = 0; c < n_components; ++c)
          {
            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path + " / Setup system / Collect point data");

            PointVec all_points_src;
            PointVec all_points_dst;

            // We determine the processes we need to request source and
            // destination points from, by computing and intersecting the
            // respective bounding boxes.
            std::vector<std::vector<BoundingBox<dim>>> all_bboxes_src;

            // Precompute the closest geodesic distance DoFs for the source
            // points that we own.
            if (prm_geodesic_cutoff_enable)
              {
                for (auto &p : points_src[c])
                  p.geodesic_closest_point =
                    geodesic_distance.find_closest_dof(p);
              }

            {
              TimerOutput::Scope timer_section(
                timer_output,
                prm_subsection_path + " / Setup system / Collect point data "
                                      "/ Bounding boxes");

              const double extend_size =
                (prm_adaptive_rbf_support ? prm_max_adaptive_support :
                                            prm_rbf_support) +
                (prm_geodesic_cutoff_enable ?
                   2.0 * geodesic_distance.get_diameter_max() :
                   0.0);

              const std::vector<BoundingBox<dim>> boxes_src =
                build_refined_boxes(points_src[c],
                                    prm_n_bbox_subdivisions,
                                    extend_size);

              all_bboxes_src = Utilities::MPI::all_gather(mpi_comm, boxes_src);
            }

            // We communicate points as needed. Each process sends its owned
            // points only to those other processes whose (enlarged) source
            // bounding boxes contain them.
            {
              TimerOutput::Scope timer_section(
                timer_output,
                prm_subsection_path + " / Setup system / Collect point data "
                                      "/ Communicate points");

              std::map<unsigned int, PointVec> data_to_send_src;
              std::map<unsigned int, PointVec> data_to_send_dst;

              for (unsigned int rank = 0; rank < mpi_size; ++rank)
                if (rank != mpi_rank)
                  {
                    const auto             &other_boxes = all_bboxes_src[rank];
                    RTree<BoundingBox<dim>> other_tree =
                      pack_rtree(other_boxes);

                    for (const auto &p : points_src[c])
                      {
                        std::vector<BoundingBox<dim>> containing_boxes;
                        bgi::query(other_tree,
                                   bgi::contains(p),
                                   std::back_inserter(containing_boxes));

                        if (containing_boxes.size() > 0)
                          data_to_send_src[rank].push_back(p);
                      }

                    for (const auto &p : points_dst[c])
                      {
                        std::vector<BoundingBox<dim>> containing_boxes;
                        bgi::query(other_tree,
                                   bgi::contains(p),
                                   std::back_inserter(containing_boxes));

                        if (containing_boxes.size() > 0)
                          data_to_send_dst[rank].push_back(p);
                      }
                  }

              all_bboxes_src.clear();

              {
                const auto data_received_src =
                  Utilities::MPI::some_to_some(mpi_comm, data_to_send_src);
                data_to_send_src.clear();

                all_points_src = points_src[c];
                for (const auto &p : data_received_src)
                  all_points_src.insert(all_points_src.end(),
                                        p.second.begin(),
                                        p.second.end());

                std::set<types::global_dof_index> new_indices;
                for (const auto &p : all_points_src)
                  new_indices.insert(p.id);
                relevant_points_src.add_indices(new_indices.begin(),
                                                new_indices.end());
              }

              {
                const auto data_received_dst =
                  Utilities::MPI::some_to_some(mpi_comm, data_to_send_dst);
                data_to_send_dst.clear();

                all_points_dst = points_dst[c];
                for (const auto &p : data_received_dst)
                  all_points_dst.insert(all_points_dst.end(),
                                        p.second.begin(),
                                        p.second.end());

                std::set<types::global_dof_index> new_indices;
                for (const auto &p : all_points_dst)
                  new_indices.insert(p.id);
                relevant_points_dst.add_indices(new_indices.begin(),
                                                new_indices.end());
              }
            }

            // Precompute the closest geodesic distance DoFs for all the
            // received source and destination points.
            if (prm_geodesic_cutoff_enable)
              {
                for (auto &p : all_points_src)
                  p.geodesic_closest_point =
                    geodesic_distance.find_closest_dof(p);

                for (auto &p : all_points_dst)
                  p.geodesic_closest_point =
                    geodesic_distance.find_closest_dof(p);
              }

            const auto distance = [&](const IndexPoint &p_j,
                                      const IndexPoint &p_i,
                                      const double     &threshold) {
              const double dist_euclidean = p_j.distance(p_i);

              double d_max = threshold;

              if (prm_geodesic_cutoff_enable)
                d_max += 2.0 * geodesic_distance.get_diameter_max();

              if (dist_euclidean > d_max)
                return std::numeric_limits<double>::max();

              if (prm_geodesic_cutoff_enable)
                {
                  geodesic_distance.set_source(p_j.geodesic_closest_point);

                  const double dist_geodesic =
                    geodesic_distance(p_i.geodesic_closest_point, d_max);

                  if (dist_geodesic >= d_max)
                    return std::numeric_limits<double>::max();

                  const bool high_curvature =
                    dist_geodesic - dist_euclidean >
                    prm_high_curvature_threshold *
                      geodesic_distance.get_diameter_max();

                  if (high_curvature)
                    return dist_geodesic;
                }

              return dist_euclidean;
            };

            const auto nth_closest_point_distance =
              [&](const IndexPoint &origin, const PointVec &points) {
                // We build a max heap that stores squared distances of the N
                // nearest neighbors.
                std::vector<double> nearest;
                nearest.reserve(prm_adaptive_neighbors);

                for (const auto &p_j : points)
                  {
                    if (p_j.id == origin.id)
                      continue;

                    // If the point is further than the N-th farthest point, we
                    // don't even compute the distance (this may avoid computing
                    // the geodesic distance, which is expensive).
                    if (nearest.size() == prm_adaptive_neighbors &&
                        origin.distance(p_j) > nearest.front())
                      continue;

                    const double dist =
                      distance(origin, p_j, prm_max_adaptive_support);

                    if (nearest.size() < prm_adaptive_neighbors)
                      {
                        nearest.push_back(dist);

                        if (nearest.size() == prm_adaptive_neighbors)
                          std::make_heap(nearest.begin(), nearest.end());
                      }
                    else if (nearest.front() > dist)
                      {
                        std::pop_heap(nearest.begin(), nearest.end());
                        nearest.pop_back();

                        nearest.push_back(dist);
                        std::push_heap(nearest.begin(), nearest.end());
                      }
                  }

                return nearest.front();
              };

            {
              TimerOutput::Scope timer_section(
                timer_output,
                prm_subsection_path + " / Setup system / Collect point data "
                                      "/ Compute matrix entries");

              // Build RTrees with source and destination points.
              auto tree_src = pack_rtree(all_points_src);
              auto tree_dst = pack_rtree(all_points_dst);

              // Given an RTree, a point and a range, filter out points of the
              // RTree that are surely outside of that range. This is done by
              // building a bounding box around the point, with the size given
              // by twice the range.
              const auto points_in_range = [&](const IndexPoint        &center,
                                               const RTree<IndexPoint> &tree,
                                               const double            &range) {
                const std::pair<Point<dim>, Point<dim>> box_boundary =
                  std::make_pair(center, center);
                BoundingBox<dim> box(box_boundary);
                box.extend(range);
                if (prm_geodesic_cutoff_enable)
                  box.extend(2.0 * geodesic_distance.get_diameter_max());

                PointVec result;
                tree.query(bgi::within(box), std::back_inserter(result));

                return result;
              };

              for (const auto &p_j : points_src[c])
                {
                  double support = 0.0;

                  if (prm_adaptive_rbf_support)
                    {
                      support =
                        std::min(prm_support_increase_factor *
                                   nth_closest_point_distance(p_j,
                                                              all_points_src),
                                 prm_max_adaptive_support);

                      support_min = std::min(support_min, support);
                      support_max = std::max(support_max, support);
                      support_avg += support;
                    }
                  else
                    {
                      support = prm_rbf_support;
                    }

                  {
                    const PointVec near_points_src =
                      points_in_range(p_j, tree_src, support);

                    for (const auto &p_i : near_points_src)
                      {
                        const double dist = distance(p_j, p_i, support);

                        if (dist < support)
                          data_int.row(p_i.id).add(p_j.id, rbf(dist, support));
                      }
                  }

                  {
                    const PointVec near_points_dst =
                      points_in_range(p_j, tree_dst, support);

                    for (const auto &p_i : near_points_dst)
                      {
                        const double dist = distance(p_j, p_i, support);

                        if (dist < support)
                          data_eva.row(p_i.id).add(p_j.id, rbf(dist, support));
                      }
                  }
                }
            }
          }

        if (prm_geodesic_cutoff_enable)
          geodesic_distance.clear();

        if (prm_adaptive_rbf_support)
          {
            support_min = Utilities::MPI::min(support_min, mpi_comm);
            support_max = Utilities::MPI::max(support_max, mpi_comm);
            support_avg =
              Utilities::MPI::sum(support_avg, mpi_comm) / n_points_src;

            pcout << utils::log::separator_subsection << std::endl;
            pcout << "    Minimum support: " << support_min << std::endl
                  << "    Average support: " << support_avg << std::endl
                  << "    Maximum support: " << support_max << std::endl;
          }

        {
          TimerOutput::Scope timer_section(
            timer_output,
            prm_subsection_path + " / Setup system / Assemble matrices");

          pcout << utils::log::separator_subsection << std::endl;
          pcout << "    Assemble interpolation matrix" << std::endl;
          data_int.assemble(owned_points_src,
                            relevant_points_src,
                            owned_points_src,
                            prm_n_matrix_compress_calls,
                            interpolation_matrix);
          data_int.clear();

          pcout << "    Assemble evaluation matrix" << std::endl;
          data_eva.assemble(owned_points_dst,
                            relevant_points_dst,
                            owned_points_src,
                            prm_n_matrix_compress_calls,
                            evaluation_matrix);
          data_eva.clear();
        }

        // Explicit conversion to double to avoid error due to overflow (when
        // multiplying the number of points) and integer division.
        const double n_points_src_double = static_cast<double>(n_points_src);

        pcout << utils::log::separator_subsection << std::endl;
        pcout << "    Interpolation fill-in: "
              << interpolation_matrix.n_nonzero_elements() /
                   (n_points_src_double * n_points_src_double)
              << std::endl;
        pcout << "    Evaluation fill-in:    "
              << evaluation_matrix.n_nonzero_elements() /
                   (n_points_src_double * n_points_dst)
              << std::endl;

        {
          unsigned int n_nonzero_rows_eval = 0;
          for (const auto &row :
               evaluation_matrix.locally_owned_range_indices())
            if (evaluation_matrix.row_length(row) > 0)
              ++n_nonzero_rows_eval;

          n_nonzero_rows_eval =
            Utilities::MPI::sum(n_nonzero_rows_eval, mpi_comm);
          AssertThrow(n_nonzero_rows_eval == n_points_dst,
                      ExcMessage(
                        std::to_string(n_points_dst - n_nonzero_rows_eval) +
                        " points in the destination mesh are not "
                        "reached by any RBF in the source mesh. "
                        "Try increasing the RBF support radius."));
        }

        if (prm_preconditioner_type == "Approximate cardinal basis")
          {
            TimerOutput::Scope timer_section(
              timer_output,
              prm_subsection_path +
                " / Setup system / Assemble preconditioner");

            // Preconditioner for the local interpolation problems, applying
            // the Gauss-Seidel method.
            class LocalGaussSeidel
            {
            public:
              void
              vmult(Vector<double> &dst, const Vector<double> &src) const
              {
                for (unsigned int i = 0; i < matrix->n(); ++i)
                  {
                    dst(i) = src(i);

                    for (unsigned int j = 0; j < i; ++j)
                      dst(i) -= (*matrix)(i, j) * dst(j);
                  }
              }

              void
              initialize(const FullMatrix<double> &matrix)
              {
                this->matrix = &matrix;
              }

            protected:
              // Local matrix.
              const FullMatrix<double> *matrix;
            };

            LocalGaussSeidel local_prec;

            preconditioner_matrix.reinit(interpolation_matrix);
            SolverGMRES<Vector<double>>::AdditionalData additional_data(1000);

            pcout << utils::log::separator_subsection << std::endl;
            pcout << "    Assembling preconditioner" << std::endl;

            for (const auto &i : owned_points_src)
              {
                // This includes both owned and not owned points.
                const auto &neighbors_all = data_int.row(i).column_indices();

                // We only retrieve owned points.
                std::vector<unsigned int> neighbors;
                for (const auto &j : neighbors_all)
                  if (owned_points_src.is_element(j))
                    neighbors.push_back(j);

                const unsigned int n = neighbors.size();

                FullMatrix<double> matrix(n, n);
                Vector<double>     rhs(n);
                Vector<double>     sol(n);

                for (unsigned int j = 0; j < n; ++j)
                  {
                    for (unsigned int k = 0; k < n; ++k)
                      {
                        matrix(j, k) =
                          interpolation_matrix(neighbors[j], neighbors[k]);
                      }

                    if (neighbors[j] == i)
                      rhs[j] = 1.0;
                    else
                      rhs[j] = 0.0;
                  }

                // Solve the local interpolation problem.
                ReductionControl            reduction_control(1000,
                                                   1e-12,
                                                   prm_prec_reduction);
                SolverGMRES<Vector<double>> solver(reduction_control,
                                                   additional_data);

                local_prec.initialize(matrix);
                solver.solve(matrix, sol, rhs, local_prec);

                // Copy the results into the preconditioner.
                for (unsigned int j = 0; j < n; ++j)
                  preconditioner_matrix.set(i, neighbors[j], sol[j]);
              }

            preconditioner_matrix.compress(VectorOperation::insert);
          }

        data_int.clear();

        if (prm_serialize_matrices)
          {
            pcout << utils::log::separator_subsection << std::endl;
            pcout << "    Serializing matrices to "
                  << prm_serialization_filename << std::endl;
            serialize_matrices(prm_serialization_filename);
          }
      }
    else
      {
        pcout << utils::log::separator_subsection << std::endl;
        pcout << "    Deserializing matrices from "
              << prm_deserialization_filename << std::endl;
        deserialize_matrices(prm_deserialization_filename);
      }

    if (prm_preconditioner_type != "Approximate cardinal basis")
      {
        TimerOutput::Scope timer_section(
          timer_output,
          prm_subsection_path + " / Setup system / Assemble preconditioner");

        preconditioner_handler.initialize(interpolation_matrix);
      }

    interpolation_coefficients_owned.reinit(owned_points_src, mpi_comm);

    if (prm_rescale)
      {
        TimerOutput::Scope timer_section(
          timer_output,
          prm_subsection_path + " / Setup system / Interpolate constant");

        pcout << utils::log::separator_subsection << std::endl;
        pcout << "    Interpolating constant function" << std::flush;

        interpolant_of_one_owned.reinit(owned_points_dst, mpi_comm);

        LinAlg::MPI::Vector vector_of_one(owned_points_src, mpi_comm);
        vector_of_one = 1.0;

        interpolate_internal(interpolant_of_one_owned, vector_of_one, true);
      }
  }

  void
  RBFInterpolation::interpolate_internal(LinAlg::MPI::Vector       &dst,
                                         const LinAlg::MPI::Vector &src,
                                         const bool &disable_rescaling)
  {
    TimerOutput::Scope timer_section(timer_output,
                                     prm_subsection_path + " / Interpolate");

    // Compute the interpolation coefficients by inverting the interpolation
    // matrix.
    if (prm_preconditioner_type == "Approximate cardinal basis")
      linear_solver.solve(interpolation_matrix,
                          interpolation_coefficients_owned,
                          src,
                          preconditioner_matrix);
    else
      linear_solver.solve(interpolation_matrix,
                          interpolation_coefficients_owned,
                          src,
                          preconditioner_handler);

    // Compute the interpolant.
    evaluation_matrix.vmult(dst, interpolation_coefficients_owned);

    // Apply rescaling if needed.
    if (prm_rescale && !disable_rescaling)
      for (const auto &i : dst.locally_owned_elements())
        dst[i] /= interpolant_of_one_owned[i];

    dst.compress(VectorOperation::insert);
  }

  double
  RBFInterpolation::rbf(const double &r, const double &support) const
  {
    const double r_rel = r / support;
    const double a     = 1.0 - r_rel;

    if (a > 0)
      return a * a * a * a * (1.0 + 4.0 * r_rel);
    else
      return 0.0;
  }
} // namespace lifex::utils