/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 */

#include "source/geometry/mesh_handler.hpp"
#include "source/geometry/mesh_info.hpp"

#include <deal.II/distributed/tria_base.h>

#include <algorithm>
#include <limits>
#include <memory>
#include <sstream>

namespace lifex::utils
{
  template <int mesh_dim, int mesh_spacedim>
  unsigned int
  n_quad_points_to_integrate_polynomial(
    const unsigned int                           &degree,
    const Triangulation<mesh_dim, mesh_spacedim> &triangulation,
    const Mapping<mesh_dim, mesh_spacedim>       &mapping)
  {
    const unsigned int mapping_degree = get_mapping_degree(mapping);

    // We are integrating p(x)*detJ(x) on the reference element, where J(x) is
    // the jacobian of the mapping. If the mesh is tetrahedral, then the entries
    // of the jacobian have one less degree than the mapping itself. If instead
    // the mesh is hexahedral, the jacobian entries have the same degree as the
    // mapping.
    const unsigned int jacobian_degree =
      dim *
      (mapping_degree -
       (MeshHandler<mesh_dim, mesh_spacedim>::is_hex(triangulation) ? 0 : 1));

    // A quadrature formula with nq nodes is exact up to the degree 2*nq - 1. We
    // want to integrate exactly polynomials of degree (degree +
    // jacobian_degree), which would yield
    //   nq >= (degree + jacobian_degree + 1) / 2,
    // which can be obtained by taking
    //   nq = ceil((degree + jacobian_degree + 1) / 2),
    // or
    //   nq = floor((degree + jacobian_degree + 2) / 2) .
    // We choose the latter, since rounding down can be obtained by means of
    // integer division, without any need for casting.
    const unsigned int n_q_points = (degree + jacobian_degree + 2) / 2;

    return n_q_points;
  }

  template <int mesh_dim, int mesh_spacedim>
  MeshInfo<mesh_dim, mesh_spacedim>::MeshInfo(
    const Triangulation<mesh_dim, mesh_spacedim> &triangulation_)
    : _initialized(false)
    , triangulation(triangulation_)
  {}

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::initialize(
    const Mapping<mesh_dim, mesh_spacedim> &mapping)
  {
    _initialized = true;

    diameter_tot = 0;
    diameter_min = std::numeric_limits<double>::max();
    diameter_max = std::numeric_limits<double>::min();

    // Compute number of locally owned active cells, depending on whether the
    // triangulation is parallel or not. This works for all types of parallel
    // triangulation (shared, distributed, fullydistributed).
    const auto *parallel_triangulation = dynamic_cast<
      const parallel::TriangulationBase<mesh_dim, mesh_spacedim> *>(
      &triangulation);

    if (parallel_triangulation)
      n_cells = parallel_triangulation->n_locally_owned_active_cells();
    else
      n_cells = triangulation.n_active_cells();

    // Compute diameter vector, as well as
    // its total, minimum, maximum and average value.
    diameters.resize(n_cells);

    unsigned int c = 0;

    for (auto cell = triangulation.begin_active(); cell != triangulation.end();
         ++cell)
      {
        if (cell->is_locally_owned())
          {
            diameters[c] = cell->diameter(mapping);

            diameter_tot += diameters[c];
            diameter_min = std::min(diameter_min, diameters[c]);
            diameter_max = std::max(diameter_max, diameters[c]);

            // Compute volume IDs and face/line boundary IDs.
            ids_volume.insert(cell->material_id());

            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              {
                if (cell->face(face)->at_boundary())
                  {
                    ids_face.insert(cell->face(face)->boundary_id());

                    // Lines are objects of dimension (dim - 2), which only
                    // makes sense if dim >= 3.
                    if constexpr (dim > 2)
                      {
                        for (unsigned int line = 0;
                             line < cell->face(face)->n_lines();
                             ++line)
                          {
                            ids_line.insert(
                              cell->face(face)->line(line)->boundary_id());
                          }
                      }
                  }
              }

            ++c;
          }
      }

    // Communicate mesh info across different parallel processes.
    n_cells      = Utilities::MPI::sum(n_cells, mpi_comm);
    diameter_tot = Utilities::MPI::sum(diameter_tot, mpi_comm);
    diameter_min = Utilities::MPI::min(diameter_min, mpi_comm);
    diameter_max = Utilities::MPI::max(diameter_max, mpi_comm);

    diameter_avg = diameter_tot / n_cells;

    // Communicate IDs across different parallel processes.
    auto allgather_ids = [](const auto &ids) -> std::vector<int> {
      int              n_ids = ids.size();
      std::vector<int> n_ids_vec(mpi_size);
      MPI_Allgather(&n_ids, 1, MPI_INT, n_ids_vec.data(), 1, MPI_INT, mpi_comm);

      int offsets = 0;
      MPI_Exscan(&n_ids, &offsets, 1, MPI_INT, MPI_SUM, mpi_comm);
      std::vector<int> offsets_vec(mpi_size);
      MPI_Allgather(
        &offsets, 1, MPI_INT, offsets_vec.data(), 1, MPI_INT, mpi_comm);

      std::vector<int> ids_vec(ids.begin(), ids.end());

      n_ids = Utilities::MPI::sum(n_ids, mpi_comm);
      std::vector<int> ids_vec_global(n_ids);
      MPI_Allgatherv(ids_vec.data(),
                     ids_vec.size(),
                     MPI_INT,
                     ids_vec_global.data(),
                     n_ids_vec.data(),
                     offsets_vec.data(),
                     MPI_INT,
                     mpi_comm);
      return ids_vec_global;
    };

    std::vector<int> ids_volume_global = allgather_ids(ids_volume);
    ids_volume = std::set<types::material_id>(ids_volume_global.begin(),
                                              ids_volume_global.end());

    std::vector<int> ids_face_global = allgather_ids(ids_face);
    ids_face = std::set<types::material_id>(ids_face_global.begin(),
                                            ids_face_global.end());

    std::vector<int> ids_line_global = allgather_ids(ids_line);
    ids_line = std::set<types::material_id>(ids_line_global.begin(),
                                            ids_line_global.end());
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::initialize()
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);

    MeshInfo::initialize(*mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::clear()
  {
    _initialized = false;

    diameters.clear();

    n_cells = 0;

    diameter_tot = 0;
    diameter_min = 0;
    diameter_max = 0;
    diameter_avg = 0;

    ids_volume.clear();
    ids_face.clear();
    ids_line.clear();
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::print(const std::string &label,
                                           const std::string &n_dofs_info,
                                           const bool        &print_ids) const
  {
    AssertThrow(_initialized, ExcNotInitialized());

    pcout << utils::log::separator_section << std::endl;

    if (!label.empty())
      pcout << label << std::endl << std::endl;


    pcout << "    Element type: ";
    if (MeshHandler<mesh_dim, mesh_spacedim>::is_hex(triangulation))
      pcout << "Hexahedra";
    else // if (MeshHandler<mesh_dim, mesh_spacedim>::is_tet(triangulation))
      pcout << "Tetrahedra";

    pcout << std::endl << std::endl;

    pcout << "    Maximum cell diameter: " << diameter_max << std::endl
          << "    Average cell diameter: " << diameter_avg << std::endl
          << "    Minimum cell diameter: " << diameter_min << std::endl
          << std::endl;


    pcout << "    Number of active cells: "
          << triangulation.n_global_active_cells() << std::endl;

    if (!n_dofs_info.empty())
      pcout << "    Number of degrees of freedom: " << n_dofs_info << std::endl;


    if (print_ids)
      {
        if (!ids_volume.empty() || !ids_face.empty() || !ids_line.empty())
          pcout << std::endl;

        if (!ids_volume.empty())
          {
            pcout << "    Volume IDs: ";
            for (auto it = ids_volume.begin(); it != ids_volume.end(); ++it)
              {
                pcout << *it;

                if (it != std::prev(ids_volume.end()))
                  pcout << ", ";
                else
                  pcout << std::endl;
              }
          }

        if (!ids_face.empty())
          {
            pcout << "    Face boundary IDs: ";
            for (auto it = ids_face.begin(); it != ids_face.end(); ++it)
              {
                pcout << *it;

                if (it != std::prev(ids_face.end()))
                  pcout << ", ";
                else
                  pcout << std::endl;
              }
          }

        if (!ids_line.empty())
          {
            pcout << "    Line boundary IDs: ";
            for (auto it = ids_line.begin(); it != ids_line.end(); ++it)
              {
                pcout << *it;

                if (it != std::prev(ids_line.end()))
                  pcout << ", ";
                else
                  pcout << std::endl;
              }
          }
      }

    pcout << utils::log::separator_section << std::endl;
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::print(const std::string  &label,
                                           const unsigned int &n_dofs,
                                           const bool         &print_ids) const
  {
    return print(label, std::to_string(n_dofs), print_ids);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::Quality::add_edge_ratio(
    const DoFHandler<dim>::active_cell_iterator &cell,
    const double                                &edge_ratio)
  {
    edge_ratio_min = std::min(edge_ratio, edge_ratio_min);
    edge_ratio_max = std::max(edge_ratio, edge_ratio_max);

    if (edge_ratio > 10)
      {
        std::stringstream message;

        message << "    [rank " << std::setw(4) << mpi_rank
                << "] | Material ID = " << cell->material_id();

        if (cell->at_boundary())
          {
            message << " | Boundary IDs = ";
            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              if (cell->face(face)->at_boundary())
                message << std::setw(4) << cell->face(face)->boundary_id()
                        << " ";
          }

        message << " | Edge ratio = " << std::setw(13) << edge_ratio;

        bad_elements.push_back(message.str());
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::Quality::add_jacobian(
    const DoFHandler<dim>::active_cell_iterator &cell,
    const double                                &jacobian)
  {
    jacobian_min = std::min(jacobian, jacobian_min);
    jacobian_max = std::max(jacobian, jacobian_max);

    if (!utils::is_positive(jacobian))
      {
        std::stringstream message;

        message << "    [rank " << std::setw(4) << mpi_rank
                << "] | Material ID = " << cell->material_id();

        if (cell->at_boundary())
          {
            message << " | Boundary IDs = ";
            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              if (cell->face(face)->at_boundary())
                message << std::setw(4) << cell->face(face)->boundary_id()
                        << " ";
          }

        message << " | Jacobian = " << std::setw(13) << jacobian;

        bad_elements.push_back(message.str());
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::Quality::add_radius_ratio(
    const DoFHandler<dim>::active_cell_iterator &cell,
    const double                                &radius_ratio)
  {
    radius_ratio_min = std::min(radius_ratio, radius_ratio_min);
    radius_ratio_max = std::max(radius_ratio, radius_ratio_max);

    if (radius_ratio > 10)
      {
        std::stringstream message;

        message << "    [rank " << std::setw(4) << mpi_rank
                << "] | Material ID = " << cell->material_id();

        if (cell->at_boundary())
          {
            message << " | Boundary IDs = ";
            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              if (cell->face(face)->at_boundary())
                message << std::setw(4) << cell->face(face)->boundary_id()
                        << " ";
          }

        message << " | Radius ratio = " << std::setw(13) << radius_ratio;

        bad_elements.push_back(message.str());
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::Quality::compress()
  {
    edge_ratio_min = Utilities::MPI::min(edge_ratio_min, mpi_comm);
    edge_ratio_max = Utilities::MPI::max(edge_ratio_max, mpi_comm);

    jacobian_min = Utilities::MPI::min(jacobian_min, mpi_comm);
    jacobian_max = Utilities::MPI::max(jacobian_max, mpi_comm);

    radius_ratio_min = Utilities::MPI::min(radius_ratio_min, mpi_comm);
    radius_ratio_max = Utilities::MPI::max(radius_ratio_max, mpi_comm);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::Quality::print(const bool &verbose)
  {
    if (compute_edge_ratio)
      {
        pcout << "\tMinimum edge ratio   = " << std::setw(13) << edge_ratio_min
              << std::endl;
        pcout << "\tMaximum edge ratio   = " << std::setw(13) << edge_ratio_max
              << std::endl;
      }

    if (compute_jacobian)
      {
        pcout << "\tMinimum jacobian     = " << std::setw(13) << jacobian_min
              << std::endl;
        pcout << "\tMaximum jacobian     = " << std::setw(13) << jacobian_max
              << std::endl;
      }

    if (compute_radius_ratio)
      {
        pcout << "\tMinimum radius ratio = " << std::setw(13)
              << radius_ratio_min << std::endl;
        pcout << "\tMaximum radius ratio = " << std::setw(13)
              << radius_ratio_max << std::endl;
      }

    if (verbose)
      {
        pcout << std::endl;

        for (unsigned int rank = 0; rank < mpi_size; ++rank)
          {
            if (rank == Core::mpi_rank)
              {
                for (const auto &error_message : bad_elements)
                  std::cout << error_message << std::endl;
              }

            MPI_Barrier(mpi_comm);
          }
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  typename MeshInfo<mesh_dim, mesh_spacedim>::Quality
  MeshInfo<mesh_dim, mesh_spacedim>::compute_quality(
    const Mapping<mesh_dim, mesh_spacedim> &mapping,
    const bool                             &compute_edge_ratio,
    const bool                             &compute_jacobian,
    const bool                             &compute_radius_ratio) const
  {
    // Create a dummy FE and DoF handler.
    auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    // We use a nodal quadrature formula to evaluate jacobians at vertices.
    // Notice that the weights of this quadrature are uninitialized, so it
    // cannot be used for integral computation.
    auto quadrature_formula_jacobians =
      MeshHandler<mesh_dim, mesh_spacedim>::get_quadrature_nodal(triangulation);

    FEValues<mesh_dim, mesh_spacedim> fe_values(mapping,
                                                *fe,
                                                *quadrature_formula_jacobians,
                                                update_jacobians);

    Quality quality(compute_edge_ratio, compute_jacobian, compute_radius_ratio);

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            fe_values.reinit(cell);

            // Edge ratio. For non-default mappings the ratio computed is the
            // one on the physical, not on the mapped mesh. For such mappings
            // the distance between vertices is not equal to the length of the
            // edges.
            /// @todo Extend to higher order mappings, if possible.
            if (compute_edge_ratio)
              {
                double len_min = std::numeric_limits<double>::max();
                double len_max = std::numeric_limits<double>::min();

                for (unsigned int line_idx = 0; line_idx < cell->n_lines();
                     ++line_idx)
                  {
                    const double len = cell->line(line_idx)->diameter();

                    len_min = std::min(len_min, len);
                    len_max = std::max(len_max, len);
                  }

                quality.add_edge_ratio(cell, len_max / len_min);
              }

            // Jacobian at quadrature points.
            if (compute_jacobian)
              {
                for (unsigned int q = 0;
                     q < quadrature_formula_jacobians->size();
                     ++q)
                  {
                    quality.add_jacobian(cell,
                                         fe_values.jacobian(q).determinant());
                  }
              }

            // Radius ratio. For non-default mappings the ratio computed is the
            // one on the physical, not on the mapped mesh.
            // Radius ratio is only computed for tetrahedral meshes.
            if (compute_radius_ratio &&
                MeshHandler<mesh_dim, mesh_spacedim>::is_tet(triangulation))
              {
                // Radius ratio is implemented according to the Verdict user
                // manual
                // (https://vtk.org/Wiki/images/6/6b/VerdictManual-revA.pdf),
                // which provides a definition of the quality metrics used
                // by Paraview.
                if constexpr (mesh_dim == 3)
                  {
                    const double volume       = cell->measure();
                    double       surface_area = 0.0;

                    for (unsigned int f = 0; f < cell->n_faces(); ++f)
                      {
                        const auto &face = cell->face(f);

                        // We compute the areas "manually" because, for some
                        // reason, face->measure() just returns 0. This code is
                        // taken from the deal.ii implementation of measure
                        // (with less checks on cell reference type).
                        auto v01 = face->vertex(1) - face->vertex(0);
                        auto v02 = face->vertex(2) - face->vertex(0);

                        surface_area += 0.5 * cross_product_3d(v01, v02).norm();
                      }

                    const auto L0 = cell->vertex(1) - cell->vertex(0);
                    const auto L2 = cell->vertex(0) - cell->vertex(2);
                    const auto L3 = cell->vertex(3) - cell->vertex(0);

                    const double radius_ratio =
                      surface_area / (108.0 * volume * volume) *
                      (L3.norm_square() * cross_product_3d(L2, L0) +
                       L2.norm_square() * cross_product_3d(L3, L0) +
                       L0.norm_square() * cross_product_3d(L3, L2))
                        .norm();

                    quality.add_radius_ratio(cell, radius_ratio);
                  }
                else if constexpr (mesh_dim == 2)
                  {
                    const auto L0 = cell->vertex(2) - cell->vertex(1);
                    const auto L1 = cell->vertex(0) - cell->vertex(2);
                    const auto L2 = cell->vertex(1) - cell->vertex(0);

                    const double area = cell->measure();

                    const double r =
                      2.0 * area / (L0.norm() + L1.norm() + L2.norm());
                    const double R =
                      L0.norm() * L1.norm() * L2.norm() / (4.0 * area);

                    quality.add_radius_ratio(cell, R / (2.0 * r));
                  }
              }
          }
      }

    quality.compress();

    return quality;
  }

  template <int mesh_dim, int mesh_spacedim>
  typename MeshInfo<mesh_dim, mesh_spacedim>::Quality
  MeshInfo<mesh_dim, mesh_spacedim>::compute_quality(
    const bool &compute_edge_ratio,
    const bool &compute_jacobian,
    const bool &compute_radius_ratio) const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);
    return compute_quality(*mapping,
                           compute_edge_ratio,
                           compute_jacobian,
                           compute_radius_ratio);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::save_diameters(
    const std::string &filename) const
  {
    AssertThrow(_initialized, ExcNotInitialized());

    std::ofstream output;

    for (unsigned int rank = 0; rank < mpi_size; ++rank)
      {
        if (rank == Core::mpi_rank)
          {
            // Rank 0 opens and writes the file, other ranks append to it.
            output.open(filename,
                        (mpi_rank == 0) ? std::ofstream::out :
                                          std::ofstream::app);

            for (const auto &d : diameters)
              output << d << std::endl;
          }

        MPI_Barrier(mpi_comm);
      }

    output.close();
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_mesh_volume(
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    // Create a dummy FE and DoF handler.
    const auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    // The volume is the integral of the constant 1.
    const unsigned int n_quadrature_nodes =
      n_quad_points_to_integrate_polynomial(0, triangulation, mapping);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    const auto quadrature_formula =
      MeshHandler<mesh_dim, mesh_spacedim>::get_quadrature_gauss(
        triangulation, n_quadrature_nodes);

    const unsigned int n_q_points = quadrature_formula->size();

    FEValues<mesh_dim, mesh_spacedim> fe_values(mapping,
                                                *fe,
                                                *quadrature_formula,
                                                update_JxW_values);

    double volume = 0;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            fe_values.reinit(cell);

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                volume += fe_values.JxW(q);
              }
          }
      }

    // Sum over the different parallel processes.
    volume = Utilities::MPI::sum(volume, mpi_comm);

    return volume;
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_mesh_volume() const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);
    return MeshInfo::compute_mesh_volume(*mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_submesh_volume(
    const std::set<types::material_id>     &material_ids,
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    // Create a dummy FE and DoF handler.
    const auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    // The volume is the integral of the constant 1.
    const unsigned int n_quadrature_nodes =
      n_quad_points_to_integrate_polynomial(0, triangulation, mapping);

    const auto quadrature_formula =
      MeshHandler<mesh_dim, mesh_spacedim>::get_quadrature_gauss(
        triangulation, n_quadrature_nodes);

    const unsigned int n_q_points = quadrature_formula->size();

    FEValues<mesh_dim, mesh_spacedim> fe_values(mapping,
                                                *fe,
                                                *quadrature_formula,
                                                update_JxW_values);

    double       volume      = 0;
    unsigned int found_count = 0;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned() &&
            material_ids.find(cell->material_id()) != material_ids.end())
          {
            fe_values.reinit(cell);

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                volume += fe_values.JxW(q);
              }

            ++found_count;
          }
      }

    found_count = Utilities::MPI::sum(found_count, mpi_comm);

    std::string exc_message =
      "None of the material IDs was found. Check that at least one of the IDs ";
    for (const auto &material_id : material_ids)
      exc_message += std::to_string(material_id) + ", ";
    exc_message += "really exists in the input triangulation.";

    AssertThrow(found_count != 0, ExcMessage(exc_message));

    // Sum over the different parallel processes.
    volume = Utilities::MPI::sum(volume, mpi_comm);

    return volume;
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_submesh_volume(
    const std::set<types::material_id> &material_ids) const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);
    return MeshInfo::compute_submesh_volume(material_ids, *mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_submesh_volume(
    const types::material_id               &material_id,
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    return MeshInfo::compute_submesh_volume({{material_id}}, mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_submesh_volume(
    const types::material_id &material_id) const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);
    return MeshInfo::compute_submesh_volume(material_id, *mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_surface_area(
    const types::boundary_id               &boundary_id,
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    // Create a dummy FE and DoF handler.
    const auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    // The area is the integral of the constant 1.
    const unsigned int n_quadrature_nodes =
      n_quad_points_to_integrate_polynomial(0, triangulation, mapping);

    const auto face_quadrature_formula =
      MeshHandler<mesh_dim, mesh_spacedim>::template get_quadrature_gauss<
        mesh_dim - 1>(triangulation, n_quadrature_nodes);

    const unsigned int n_q_points = face_quadrature_formula->size();

    FEFaceValues<mesh_dim, mesh_spacedim> fe_face_values(
      mapping, *fe, *face_quadrature_formula, update_JxW_values);

    double       area        = 0;
    unsigned int found_count = 0;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              {
                if (cell->face(face)->at_boundary() &&
                    cell->face(face)->boundary_id() == boundary_id)
                  {
                    fe_face_values.reinit(cell, face);

                    for (unsigned int q = 0; q < n_q_points; ++q)
                      {
                        area += fe_face_values.JxW(q);
                      }

                    ++found_count;
                  }
              }
          }
      }

    found_count = Utilities::MPI::sum(found_count, mpi_comm);

    AssertThrow(found_count != 0,
                ExcMessage("Boundary ID " + std::to_string(boundary_id) +
                           " not found."));

    // Sum over the different parallel processes.
    area = Utilities::MPI::sum(area, mpi_comm);

    return area;
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  MeshInfo<mesh_dim, mesh_spacedim>::compute_surface_area(
    const types::boundary_id &boundary_id) const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);
    return MeshInfo::compute_surface_area(boundary_id, *mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  Tensor<1, mesh_spacedim, double>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_flat_boundary_normal(
    const types::boundary_id &boundary_id) const
  {
    // Create a dummy FE and DoF handler.
    const auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    // We just compute the normal at the barycenter of
    // the first face found on the considered boundary.
    const auto face_quadrature_formula =
      MeshHandler<mesh_dim, mesh_spacedim>::template get_quadrature_gauss<
        mesh_dim - 1>(triangulation, 1);

    const unsigned int n_face_q_points = face_quadrature_formula->size();

    FEFaceValues<mesh_dim, mesh_spacedim> fe_face_values(
      *fe, *face_quadrature_formula, update_normal_vectors);

    Tensor<1, mesh_spacedim, double> normal_vector;
    unsigned int                     found_count = 0;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              {
                if (cell->face(face)->at_boundary() &&
                    cell->face(face)->boundary_id() == boundary_id)
                  {
                    fe_face_values.reinit(cell, face);

                    for (unsigned int q = 0; q < n_face_q_points; ++q)
                      {
                        normal_vector = fe_face_values.normal_vector(q);
                      }

                    ++found_count;

                    break;
                  }
              }

            if (found_count > 0)
              break;
          }
      }

    // Here found_count is either 0 or 1.
    // Summing found_count returns the number of processes
    // owning a face on the considered boundary.
    found_count = Utilities::MPI::sum(found_count, mpi_comm);

    AssertThrow(found_count != 0,
                ExcMessage("Boundary ID " + std::to_string(boundary_id) +
                           " not found."));

    // Compute average over the different parallel processes.
    // Since we are assuming that the boundary is flat,
    // every process owning a face on that boundary computes the same
    // normal vector as the other processes (up to the numerical precision).
    // Therefore, returning the average is equivalent
    // to return any one of the normal vectors found.
    for (unsigned int d = 0; d < dim; ++d)
      {
        normal_vector[d] =
          Utilities::MPI::sum(normal_vector[d], mpi_comm) / found_count;
      }

    return normal_vector;
  }

  template <int mesh_dim, int mesh_spacedim>
  Point<mesh_spacedim>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_mesh_barycenter(
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    // Create a dummy FE and DoF handler.
    const auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    // The barycenter is the integral of the function x.
    const unsigned int n_quadrature_nodes =
      n_quad_points_to_integrate_polynomial(1, triangulation, mapping);

    const auto quadrature_formula =
      MeshHandler<mesh_dim, mesh_spacedim>::get_quadrature_gauss(
        triangulation, n_quadrature_nodes);

    const unsigned int n_q_points = quadrature_formula->size();

    FEValues<mesh_dim, mesh_spacedim> fe_values(mapping,
                                                *fe,
                                                *quadrature_formula,
                                                update_JxW_values |
                                                  update_quadrature_points);


    Point<mesh_spacedim> barycenter;
    double               volume = 0;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            fe_values.reinit(cell);

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                volume += fe_values.JxW(q);
                barycenter += fe_values.quadrature_point(q) * fe_values.JxW(q);
              }
          }
      }

    // Sum over the different parallel processes.
    volume = Utilities::MPI::sum(volume, mpi_comm);
    for (unsigned int d = 0; d < dim; ++d)
      barycenter[d] = Utilities::MPI::sum(barycenter[d], mpi_comm);

    barycenter /= volume;

    return barycenter;
  }

  template <int mesh_dim, int mesh_spacedim>
  Point<mesh_spacedim>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_mesh_barycenter() const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);
    return MeshInfo::compute_mesh_barycenter(*mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  Point<mesh_spacedim>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_surface_barycenter(
    const std::set<types::boundary_id>     &boundary_ids,
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    // Create a dummy FE and DoF handler.
    const auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    // The barycenter is the integral of the function x.
    const unsigned int n_quadrature_nodes =
      n_quad_points_to_integrate_polynomial(1, triangulation, mapping);

    const auto face_quadrature_formula =
      MeshHandler<mesh_dim, mesh_spacedim>::template get_quadrature_gauss<
        mesh_dim - 1>(triangulation, n_quadrature_nodes);

    const unsigned int n_q_points = face_quadrature_formula->size();

    FEFaceValues<mesh_dim, mesh_spacedim> fe_face_values(
      mapping,
      *fe,
      *face_quadrature_formula,
      update_JxW_values | update_quadrature_points);

    Point<mesh_spacedim> barycenter;
    double               area        = 0;
    unsigned int         found_count = 0;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            for (unsigned int face = 0; face < cell->n_faces(); ++face)
              {
                if (cell->face(face)->at_boundary() &&
                    boundary_ids.find(cell->face(face)->boundary_id()) !=
                      boundary_ids.end())
                  {
                    fe_face_values.reinit(cell, face);

                    for (unsigned int q = 0; q < n_q_points; ++q)
                      {
                        barycenter += fe_face_values.quadrature_point(q) *
                                      fe_face_values.JxW(q);

                        area += fe_face_values.JxW(q);
                      }

                    ++found_count;
                  }
              }
          }
      }

    found_count = Utilities::MPI::sum(found_count, mpi_comm);

    std::string exc_message =
      "None of the boundary IDs was found. Check that at least one of the IDs ";
    for (const auto &boundary_id : boundary_ids)
      exc_message += std::to_string(boundary_id) + ", ";
    exc_message += "really exists in the input triangulation.";

    AssertThrow(found_count != 0, ExcMessage(exc_message));

    // Sum over the different parallel processes.
    area = Utilities::MPI::sum(area, mpi_comm);
    for (unsigned int component = 0; component < dim; ++component)
      {
        barycenter[component] =
          Utilities::MPI::sum(barycenter[component], mpi_comm);
      }

    return barycenter / area;
  }

  template <int mesh_dim, int mesh_spacedim>
  Point<mesh_spacedim>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_surface_barycenter(
    const std::set<types::boundary_id> &boundary_ids) const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);

    return MeshInfo::compute_surface_barycenter(boundary_ids, *mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  Point<mesh_spacedim>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_surface_barycenter(
    const types::boundary_id               &boundary_id,
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    return MeshInfo::compute_surface_barycenter({{boundary_id}}, mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  Point<mesh_spacedim>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_surface_barycenter(
    const types::boundary_id &boundary_id) const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);
    return MeshInfo::compute_surface_barycenter(boundary_id, *mapping);
  }

  template <int mesh_dim, int mesh_spacedim>
  Tensor<1, mesh_spacedim, double>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_mesh_moment_inertia(
    const Mapping<mesh_dim, mesh_spacedim> &mapping) const
  {
    // Create a dummy FE and DoF handler.
    const auto fe =
      MeshHandler<mesh_dim, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    // The d-th component of the moment of inertia is the integral of the
    // function (x_d)^2, where x_d is the d-th component of the position vector.
    const unsigned int n_quadrature_nodes =
      n_quad_points_to_integrate_polynomial(2, triangulation, mapping);

    const auto quadrature_formula =
      MeshHandler<mesh_dim, mesh_spacedim>::get_quadrature_gauss(
        triangulation, n_quadrature_nodes);

    const unsigned int n_q_points = quadrature_formula->size();

    FEValues<mesh_dim, mesh_spacedim> fe_values(mapping,
                                                *fe,
                                                *quadrature_formula,
                                                update_JxW_values |
                                                  update_quadrature_points);

    Tensor<1, mesh_spacedim, double> moment;

    for (const auto &cell : dof_handler.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            fe_values.reinit(cell);

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                for (unsigned int d = 0; d < dim; ++d)
                  // d-th component = integral of x_d^2
                  moment[d] += fe_values.quadrature_point(q)[d] *
                               fe_values.quadrature_point(q)[d] *
                               fe_values.JxW(q);
              }
          }
      }

    // Sum over the different parallel processes.
    for (unsigned int d = 0; d < dim; ++d)
      moment[d] = Utilities::MPI::sum(moment[d], mpi_comm);

    return moment;
  }

  template <int mesh_dim, int mesh_spacedim>
  Tensor<1, mesh_spacedim, double>
  MeshInfo<mesh_dim, mesh_spacedim>::compute_mesh_moment_inertia() const
  {
    const auto mapping =
      MeshHandler<mesh_dim, mesh_spacedim>::get_linear_mapping(triangulation);

    return MeshInfo::compute_mesh_moment_inertia(*mapping);
  }

  /// @cond DOXYGEN_SKIP

  template class MeshInfo<dim, dim>;

  template unsigned int
  n_quad_points_to_integrate_polynomial<dim, dim>(
    const unsigned int            &degree,
    const Triangulation<dim, dim> &triangulation,
    const Mapping<dim, dim>       &mapping);

#if LIFEX_DIM > 2
  template class MeshInfo<2, dim>;

  template unsigned int
  n_quad_points_to_integrate_polynomial<2, dim>(
    const unsigned int          &degree,
    const Triangulation<2, dim> &triangulation,
    const Mapping<2, dim>       &mapping);
#endif

#if LIFEX_DIM > 1
  template class MeshInfo<1, dim>;

  template unsigned int
  n_quad_points_to_integrate_polynomial<1, dim>(
    const unsigned int          &degree,
    const Triangulation<1, dim> &triangulation,
    const Mapping<1, dim>       &mapping);
#endif

  /// @endcond

} // namespace lifex::utils
