/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 */

#ifndef LIFEX_UTILS_MESH_INFO_HPP_
#define LIFEX_UTILS_MESH_INFO_HPP_

#include "source/core.hpp"

#include "source/numerics/numbers.hpp"

#include <deal.II/distributed/tria_base.h>

#include <deal.II/fe/mapping_fe.h>
#include <deal.II/fe/mapping_fe_field.h>
#include <deal.II/fe/mapping_q.h>

#include <fstream>
#include <limits>
#include <optional>
#include <set>
#include <string>
#include <vector>

namespace lifex::utils
{
  /**
   * @brief Get the degree of the given mapping.
   *
   * This assumes that the concrete Mapping type is MappingFEField<mesh_dim,
   * mesh_spacedim, LinAlg::MPI::Vector>, MappingFE<mesh_dim, mesh_spacedim> or
   * MappingQ<mesh_dim, mesh_spacedim>. An exception is thrown otherwise.
   *
   * @param[in] mapping Geometric mapping applied to the mesh.
   */
  template <int mesh_dim, int mesh_spacedim = mesh_dim>
  unsigned int
  get_mapping_degree(const Mapping<mesh_dim, mesh_spacedim> &mapping)
  {
    auto mapping_fe_field = dynamic_cast<
      const MappingFEField<mesh_dim, mesh_spacedim, LinAlg::MPI::Vector> *>(
      &mapping);
    if (mapping_fe_field)
      return mapping_fe_field->get_degree();

    auto mapping_fe =
      dynamic_cast<const MappingFE<mesh_dim, mesh_spacedim> *>(&mapping);
    if (mapping_fe)
      return mapping_fe->get_degree();

    auto mapping_q =
      dynamic_cast<const MappingQ<mesh_dim, mesh_spacedim> *>(&mapping);
    if (mapping_q)
      return mapping_q->get_degree();

    Assert(false, ExcLifexNotImplemented());
    return 0;
  }

  /**
   * @brief Return the number of quadrature points needed to integrate exactly
   * polynomials of a given degree.
   */
  template <int mesh_dim, int mesh_spacedim = mesh_dim>
  unsigned int
  n_quad_points_to_integrate_polynomial(
    const unsigned int                           &degree,
    const Triangulation<mesh_dim, mesh_spacedim> &triangulation,
    const Mapping<mesh_dim, mesh_spacedim>       &mapping);

  /**
   * @brief Class to store geometrical information of a
   * parallel distributed triangulation.
   *
   * This class also contains some mesh utilities, such as
   * @ref compute_mesh_volume and @ref compute_surface_area.
   */
  template <int mesh_dim, int mesh_spacedim = mesh_dim>
  class MeshInfo : public Core
  {
  public:
    /// Constructor.
    MeshInfo(const Triangulation<mesh_dim, mesh_spacedim> &triangulation_);

    /// Compute diameters, volume IDs and face/line boundary IDs of the mapped
    /// input triangulation.
    void
    initialize(const Mapping<mesh_dim, mesh_spacedim> &mapping);

    /// Same as the function above but for a default linear mapping.
    void
    initialize();

    /// Clear all data members and reset to a non-initialized state.
    void
    clear();

    /// Print the following mesh information to standard output:
    /// - a user-specified label;
    /// - maximum cell diameter;
    /// - average cell diameter;
    /// - minimum cell diameter;
    /// - a user-specified string info about the number of degrees of freedom
    /// stored by a DoFHandler attached to the current mesh;
    /// - if the last parameter is <kbd>true</kbd>, the volume IDs and face/line
    ///   boundary IDs.
    void
    print(const std::string &label,
          const std::string &n_dofs_info,
          const bool        &print_ids) const;

    /// Same as the function above, where the second input is the number of dofs
    /// to print.
    void
    print(const std::string  &label,
          const unsigned int &n_dofs,
          const bool         &print_ids) const;

    /**
     * @brief Class to store the mesh quality information.
     *
     * Allows to keep track of the following metrics:
     * - **edge ratio**: for every element @f$E@f$, whose edges have lengths
     * @f$l_i@f$, @f$i = 1, \dots, n_\text{edges}@f$, computes @f$\frac{\max_i
     * l_i}{\min_i l_i}@f$, and reports maximum and minimum ratio across all
     * elements;
     * - **jacobians**: computes the jacobian of the mapping from the reference
     * to the physical element and prints minimum and maximum value;
     * - **radius ratio**: computes the maximum and minimum ratio between the
     * circumscribed and inscribed spheres to an element. This only works for
     * tetrahedral meshes.
     */
    class Quality
    {
    public:
      /// Constructor.
      Quality(const bool &compute_edge_ratio_,
              const bool &compute_jacobian_,
              const bool &compute_radius_ratio_)
        : compute_edge_ratio(compute_edge_ratio_)
        , compute_jacobian(compute_jacobian_)
        , compute_radius_ratio(compute_radius_ratio_)
      {
        edge_ratio_min = std::numeric_limits<double>::max();
        edge_ratio_max = std::numeric_limits<double>::min();

        jacobian_min = std::numeric_limits<double>::max();
        jacobian_max = std::numeric_limits<double>::min();

        radius_ratio_min = std::numeric_limits<double>::max();
        radius_ratio_max = std::numeric_limits<double>::min();
      }

      /// Add new edge ratio.
      void
      add_edge_ratio(const DoFHandler<dim>::active_cell_iterator &cell,
                     const double                                &edge_ratio);

      /// Add new jacobian.
      void
      add_jacobian(const DoFHandler<dim>::active_cell_iterator &cell,
                   const double                                &jacobian);

      /// Add new radius ratio.
      void
      add_radius_ratio(const DoFHandler<dim>::active_cell_iterator &cell,
                       const double &radius_ratio);

      /// Communicate between processes.
      void
      compress();

      /// Print the data stored in this object.
      void
      print(const bool &verbose = false);

      /// Get minimum edge ratio.
      double
      get_edge_ratio_min() const
      {
        return edge_ratio_min;
      }

      /// Get maximum edge ratio.
      double
      get_edge_ratio_max() const
      {
        return edge_ratio_max;
      }

      /// Get minimum jacobian.
      double
      get_jacobian_min() const
      {
        return jacobian_min;
      }

      /// Get maximum jacobian.
      double
      get_jacobian_max() const
      {
        return jacobian_max;
      }

      /// Get minimum radius ratio.
      double
      get_radius_ratio_min() const
      {
        return radius_ratio_min;
      }

      /// Get maximum radius ratio.
      double
      get_radius_ratio_max() const
      {
        return radius_ratio_max;
      }

    protected:
      bool   compute_edge_ratio; ///< Toggle computation of edge ratio.
      double edge_ratio_min;     ///< Minimum edge ratio.
      double edge_ratio_max;     ///< Maximum edge ratio.

      bool   compute_jacobian; ///< Toggle computation of jacobian.
      double jacobian_min;     ///< Minimum jacobian.
      double jacobian_max;     ///< Maximum jacobian.

      bool   compute_radius_ratio; ///< Toggle computation of radius ratio.
      double radius_ratio_min;     ///< Minimum radius ratio.
      double radius_ratio_max;     ///< Maximum radius ratio.

      /// String descriptions of elements with bad quality, for error reporting.
      std::vector<std::string> bad_elements;
    };

    /**
     * @brief Compute quality metrics of the mesh, as implemented by
     * the class Quality.
     *
     * @note For simplex meshes, the minimum and maximum jacobians are
     * estimated, not computed exactly.
     *
     * @todo Check proper metrics on tetrahedra.
     */
    Quality
    compute_quality(const Mapping<mesh_dim, mesh_spacedim> &mapping,
                    const bool                             &compute_edge_ratio,
                    const bool                             &compute_jacobian,
                    const bool &compute_radius_ratio) const;

    /// Same as above, but for default linear mappings.
    Quality
    compute_quality(const bool &compute_edge_ratio   = true,
                    const bool &compute_jacobian     = true,
                    const bool &compute_radius_ratio = true) const;

    /// Compute and print the mesh quality info.
    Quality
    print_quality(const Mapping<mesh_dim, mesh_spacedim> &mapping,
                  const bool                             &compute_edge_ratio,
                  const bool                             &compute_jacobian,
                  const bool                             &compute_radius_ratio,
                  const bool                             &verbose = false) const
    {
      Quality info = compute_quality(mapping,
                                     compute_edge_ratio,
                                     compute_jacobian,
                                     compute_radius_ratio);

      info.print(verbose);

      return info;
    }

    /// Same as above, but for default linear mappings.
    Quality
    print_quality(const bool &compute_edge_ratio   = true,
                  const bool &compute_jacobian     = true,
                  const bool &compute_radius_ratio = true,
                  const bool &verbose              = false) const
    {
      Quality info = compute_quality(compute_edge_ratio,
                                     compute_jacobian,
                                     compute_radius_ratio);

      info.print(verbose);

      return info;
    }

    /// Save diameter vector @ref diameters to output file,
    /// sorted by parallel rank and cell index.
    void
    save_diameters(const std::string &filename) const;

    /// Getter for @ref _initialized.
    bool
    initialized() const
    {
      return _initialized;
    }

    /// Get total mesh diameter.
    const double &
    get_diameter_tot() const
    {
      AssertThrow(_initialized, ExcNotInitialized());

      return diameter_tot;
    }

    /// Get minimum cell diameter.
    const double &
    get_diameter_min() const
    {
      AssertThrow(_initialized, ExcNotInitialized());

      return diameter_min;
    }

    /// Get maximum cell diameter.
    const double &
    get_diameter_max() const
    {
      AssertThrow(_initialized, ExcNotInitialized());

      return diameter_max;
    }

    /// Get average cell diameter.
    const double &
    get_diameter_avg() const
    {
      AssertThrow(_initialized, ExcNotInitialized());

      return diameter_avg;
    }

    /// Get volume IDs.
    const std::set<types::material_id> &
    get_ids_volume() const
    {
      AssertThrow(_initialized, ExcNotInitialized());

      return ids_volume;
    }

    /// Get face boundary IDs.
    const std::set<types::boundary_id> &
    get_ids_face() const
    {
      AssertThrow(_initialized, ExcNotInitialized());

      return ids_face;
    }

    /// Get line boundary IDs.
    const std::set<types::boundary_id> &
    get_ids_line() const
    {
      AssertThrow(_initialized, ExcNotInitialized());

      return ids_line;
    }

    /// Compute the volume of a mesh.
    ///
    /// @param[in] mapping Geometric mapping applied to the mesh.
    double
    compute_mesh_volume(const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    double
    compute_mesh_volume() const;

    /// Compute the volume of a subportion of a mesh, given a set of material
    /// IDs that define it.
    ///
    /// @param[in] material_ids Set of IDs defining the submesh.
    /// @param[in] mapping Geometric mapping applied to the mesh.
    double
    compute_submesh_volume(
      const std::set<types::material_id>     &material_ids,
      const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    double
    compute_submesh_volume(
      const std::set<types::material_id> &material_ids) const;

    /// Compute the volume of a subportion of a mesh, given its material ID.
    ///
    /// @param[in] material_id Material ID of the submesh.
    /// @param[in] mapping Geometric mapping applied to the mesh.
    double
    compute_submesh_volume(
      const types::material_id               &material_id,
      const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    double
    compute_submesh_volume(const types::material_id &material_id) const;

    /// Compute the area of a surface, given its boundary ID.
    ///
    /// @param[in] boundary_id Boundary ID of the surface.
    /// @param[in] mapping Geometric mapping applied to the mesh.
    double
    compute_surface_area(const types::boundary_id               &boundary_id,
                         const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    double
    compute_surface_area(const types::boundary_id &boundary_id) const;

    /// Compute the normal vector to a @b flat boundary surface, given its
    /// boundary ID.
    Tensor<1, mesh_spacedim, double>
    compute_flat_boundary_normal(const types::boundary_id &boundary_id) const;

    /// Compute the barycenter of the mesh.
    ///
    /// @param[in] mapping Geometric mapping applied to the mesh.
    Point<mesh_spacedim>
    compute_mesh_barycenter(
      const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    Point<mesh_spacedim>
    compute_mesh_barycenter() const;

    /// Compute the barycenter of a set of @b flat boundary surfaces, given
    /// their boundary IDs.
    ///
    /// @param[in] boundary_ids Set of IDs describing the surfaces.
    /// @param[in] mapping Geometric mapping applied to the mesh.
    Point<mesh_spacedim>
    compute_surface_barycenter(
      const std::set<types::boundary_id>     &boundary_ids,
      const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    Point<mesh_spacedim>
    compute_surface_barycenter(
      const std::set<types::boundary_id> &boundary_ids) const;

    /// Compute the barycenter of a @b flat boundary surface, given its boundary
    /// ID.
    ///
    /// @param[in] boundary_id Boundary ID of the surface.
    /// @param[in] mapping Geometric mapping applied to the mesh.
    Point<mesh_spacedim>
    compute_surface_barycenter(
      const types::boundary_id               &boundary_id,
      const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    Point<mesh_spacedim>
    compute_surface_barycenter(const types::boundary_id &boundary_id) const;

    /// Compute the moment of inertia of the mesh w.r.t. Cartesian directions.
    ///
    /// @param[in] mapping Geometric mapping applied to the mesh.
    Tensor<1, mesh_spacedim, double>
    compute_mesh_moment_inertia(
      const Mapping<mesh_dim, mesh_spacedim> &mapping) const;

    /// Same as the function above, but with a default linear mapping.
    Tensor<1, mesh_spacedim, double>
    compute_mesh_moment_inertia() const;

    /**
     * Check that a given set of bounday IDs is consistent with the IDs
     * actually defined on the triangulation. If the check fails, throws an
     * exception whose message lists the incorrect tags. Can be used to check
     * the consistency between user-provided boundary IDs and mesh boundary IDs,
     * e.g. to make sure that the user has defined boundary conditions on all
     * boundaries.
     *
     * @param[in] boundary_ids The set of user-provided boundary IDs.
     * @param[in] allow_inexistent If true, boundary_ids is allowed to contain
     * IDs that are not defined on the triangulation.
     * @param[in] allow_missing If true, boundary_ids does not need to contain
     * all IDs defined on the triangulation (i.e. it is allowed to be a proper
     * subset of ids_face).
     * @param[in] error_message_prefix, error_message_inexistent,
     * error_message_missing Strings used to compose the error message as
     * follows: "<error_message_prefix>. <error_message_inexistent> <list of
     * inexistent tags>. <error_message_missing> <list of missing tags>.".
     */
    template <class Container>
    void
    check_face_ids(const Container   &boundary_ids,
                   const bool        &allow_inexistent,
                   const bool        &allow_missing,
                   const std::string &error_message_prefix,
                   const std::string &error_message_inexistent,
                   const std::string &error_message_missing) const;

    /**
     * Check that a given set of volume IDs is consistent with the IDs
     * actually defined on the triangulation. If the check fails, throws an
     * exception whose message lists the incorrect tags. Can be used to check
     * the consistency between user-provided volume IDs and mesh volume IDs,
     * e.g. to make sure that the user has defined boundary conditions on all
     * boundaries.
     *
     * @param[in] volume_ids The set of user-provided volume IDs.
     * @param[in] allow_inexistent If true, boundary_ids is allowed to contain
     * IDs that are not defined on the triangulation.
     * @param[in] allow_missing If true, boundary_ids does not need to contain
     * all IDs defined on the triangulation (i.e. it is allowed to be a proper
     * subset of ids_face).
     * @param[in] error_message_prefix, error_message_inexistent,
     * error_message_missing Strings used to compose the error message as
     * follows: "<error_message_prefix>. <error_message_inexistent> <list of
     * inexistent tags>. <error_message_missing> <list of missing tags>.".
     */
    template <class Container>
    void
    check_volume_ids(const Container   &volume_ids,
                     const bool        &allow_inexistent,
                     const bool        &allow_missing,
                     const std::string &error_message_prefix,
                     const std::string &error_message_inexistent,
                     const std::string &error_message_missing) const;

  private:
    /**
     * Check the consistency between two sets of (volume, face or line) IDs. If
     * If the check fails, throws an exception whose message lists the incorrect
     * tags. Can be used to check the consistency between user-provided boundary
     * IDs and mesh boundary IDs, e.g. to make sure that the user has defined
     * boundary conditions on all boundaries. Provides a common logic to
     * @ref check_face_ids and @ref check_volume_ids.
     *
     * @param[in] ids_to_check The set of IDs whose correctness needs to be
     * checked.
     * @param[in] valid_ids The set of valid IDs (e.g. IDs that are defined on
     * the mesh).
     * @param[in] allow_inexistent If true, boundary_ids is allowed to contain
     * IDs that are not defined on the triangulation.
     * @param[in] allow_missing If true, boundary_ids does not need to contain
     * all IDs defined on the triangulation (i.e. it is allowed to be a proper
     * subset of ids_face).
     * @param[in] error_message_prefix, error_message_inexistent,
     * error_message_missing Strings used to compose the error message as
     * follows: "<error_message_prefix>. <error_message_inexistent> <list of
     * inexistent tags>. <error_message_missing> <list of missing tags>.".
     */
    template <class ContainerA, class ContainerB>
    void
    check_ids(const ContainerA  &ids_to_check,
              const ContainerB  &valid_ids,
              const bool        &allow_inexistent,
              const bool        &allow_missing,
              const std::string &error_message_prefix,
              const std::string &error_message_inexistent,
              const std::string &error_message_missing) const;

    /// <kbd>true</kbd> if @ref initialize was called, <kbd>false</kbd> otherwise.
    bool _initialized;

    /// Reference to a triangulation object.
    const Triangulation<mesh_dim, mesh_spacedim> &triangulation;

    /// Vector containing the diameter of each locally owned active cell.
    std::vector<double> diameters;

    /// Global number of active cells.
    unsigned int n_cells;

    double diameter_tot; ///< Total mesh diameter.
    double diameter_min; ///< Minimum cell diameter.
    double diameter_max; ///< Maximum cell diameter.
    double diameter_avg; ///< Average cell diameter.

    std::set<types::material_id> ids_volume; ///< Volume IDs.
    std::set<types::boundary_id> ids_face;   ///< Face boundary IDs.
    std::set<types::boundary_id> ids_line;   ///< Line boundary IDs.
  };

  template <int mesh_dim, int mesh_spacedim>
  template <class ContainerA, class ContainerB>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::check_ids(
    const ContainerA  &ids_to_check,
    const ContainerB  &valid_ids,
    const bool        &allow_inexistent,
    const bool        &allow_missing,
    const std::string &error_message_prefix,
    const std::string &error_message_inexistent,
    const std::string &error_message_missing) const
  {
    std::string inexistent_ids = "";
    std::string missing_ids    = "";

    if (!allow_inexistent)
      {
        for (const auto &tag : ids_to_check)
          if (!utils::contains(valid_ids, tag))
            inexistent_ids += " " + std::to_string(tag);
      }

    if (!allow_missing)
      {
        for (const auto &tag : valid_ids)
          if (!utils::contains(ids_to_check, tag))
            missing_ids += " " + std::to_string(tag);
      }

    if ((!allow_missing && !missing_ids.empty()) ||
        (!allow_inexistent && !inexistent_ids.empty()))
      {
        std::string error_message = error_message_prefix + ".";

        if (!allow_inexistent && !inexistent_ids.empty())
          error_message +=
            " " + error_message_inexistent + ":" + inexistent_ids + ".";

        if (!allow_missing && !missing_ids.empty())
          error_message +=
            " " + error_message_missing + ":" + missing_ids + ".";

        AssertThrow(false, ExcMessage(error_message));
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  template <class Container>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::check_face_ids(
    const Container   &boundary_ids,
    const bool        &allow_inexistent,
    const bool        &allow_missing,
    const std::string &error_message_prefix,
    const std::string &error_message_inexistent,
    const std::string &error_message_missing) const
  {
    check_ids(boundary_ids,
              ids_face,
              allow_inexistent,
              allow_missing,
              error_message_prefix,
              error_message_inexistent,
              error_message_missing);
  }

  template <int mesh_dim, int mesh_spacedim>
  template <class Container>
  void
  MeshInfo<mesh_dim, mesh_spacedim>::check_volume_ids(
    const Container   &volume_ids,
    const bool        &allow_inexistent,
    const bool        &allow_missing,
    const std::string &error_message_prefix,
    const std::string &error_message_inexistent,
    const std::string &error_message_missing) const
  {
    check_ids(volume_ids,
              ids_volume,
              allow_inexistent,
              allow_missing,
              error_message_prefix,
              error_message_inexistent,
              error_message_missing);
  }

} // namespace lifex::utils

#endif /* LIFEX_UTILS_MESH_INFO_HPP_ */
