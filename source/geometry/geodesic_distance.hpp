/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_GEODESIC_DISTANCE_HPP
#define LIFEX_GEODESIC_DISTANCE_HPP

#include "source/core_model.hpp"

#include "source/geometry/mesh_handler.hpp"

#include <deal.II/numerics/rtree.h>

#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace lifex::utils
{
  /**
   * @brief Geodesic distance evaluator.
   *
   * Given a mesh and two points @f$\mathbf{p}_0@f$, @f$\mathbf{p}_1@f$,
   * evaluates the distance between those points measured traveling between
   * adjacend mesh vertices.
   *
   * The two input points @f$\mathbf{p}_0@f$ and @f$\mathbf{p}_1@f$ are not
   * assumed to be vertices of the mesh. Instead, the two nearest mesh vertices
   * @f$\hat{\mathbf{p}}_0@f$ and @f$\hat{\mathbf{p}}_1@f$ are retrieved, and
   * then the shortest path between them is computed.
   *
   * Two mesh vertices are considered to be adjacent if they are vertices of the
   * same element. Therefore, in tetrahedral meshes, adjacent vertices are
   * vertices sharing an edge. In hexahedral meshes, instead, adjacent vertices
   * are connected either by an edge or an element diagonal. The distance
   * between adjacent vertices is the euclidean distance. The shortest path is
   * computed using [Dijkstra's
   * algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) (see also
   * @refcite{dijkstra1959note, Dijkstra (1959)}).
   *
   * To compute the distance between two points @f$\mathbf{a}@f$ and
   * @f$\mathbf{b}@f$:
   * 1. call the method @ref set_source passing the coordinates of
   * @f$\mathbf{a}@f$ as argument. This will locate @f$\hat{\mathbf{a}}@f$, the
   * closest mesh vertex to @f$\mathbf{a}@f$.
   * 2. call GeodesicDistance::operator() passing the coordinates of
   * @f$\mathbf{b}@f$ as argument. This will locate @f$\hat{\mathbf{b}}@f$, the
   * closest mesh vertex to @f$\mathbf{a}@f$, and return the length of the
   * shortest path between @f$\hat{\mathbf{a}}@f$ and @f$\hat{\mathbf{b}}@f$.
   *
   * Optionally, the class also allows to compute the shortest path themselves
   * (instead of only the distance). To do so, set <kbd>compute_shortest_path_ =
   * true</kbd> when calling the @ref setup_system method. Shortest path can be * computed by calling @ref get_shortest_path, and can be saved to a VTU file
   * by calling @ref save_path_to_file.
   *
   * ### Thresholding
   *
   * When calling operator(), you may optionally pass in a threshold value. If
   * the shortest path between source and destination points is longer than the
   * threshold, then the operator simply returns the threshold. This allows to
   * significantly reduce the cost of evaluation in situations when points
   * further than a threshold can be disregarded (see
   * e.g. @ref RBFInterpolation).
   *
   * ### Usage in parallel
   *
   * The class can be used in parallel. The initialization method
   * @ref setup_system is a collective operation, but the calls to operator()
   * are not. Furthermore, the method @ref set_source and operator() can be
   * called on points that are not owned by the current parallel process.
   *
   * To allow for this, the @ref setup_system method performs communication
   * between processes making sure that everyone has the coordinates and
   * adjacency information of all mesh vertices. This is an expensive operation,
   * in terms of computations and memory usage, and it clearly does not scale
   * effectively.
   *
   * As an alternative, if the source points that current process requires are
   * known at the initialization stage, and the maximum threshold for current
   * process is also known, these can be passed to an overloaded version of the
   * @ref setup_system method. Doing so, each process will only receive the
   * points (and the associated adjacency information) that are within the
   * thresold from source points it requires.
   *
   * ### Notes on computational efficiency
   *
   * **Repeated distance evaluations.** The class builds a shortest path tree
   * from the source point (@f$\hat{\mathbf{a}}@f$) to other points in the mesh.
   * Calling repeatedly GeodesicDistance::operator() with the same source point
   * will reuse already computed distances, if possible.
   *
   * **Retrieving mesh vertices.** The operation of finding the closest mesh
   * vertex @f$\hat{\mathbf{a}}@f$ to some point @f$\mathbf{a}@f$ is costly, if
   * compared to the cost of evaluating the shortest path itself. If multiple
   * evaluations of the distance are required, involving the same points
   * multiple times, the index of the closest mesh vertex can be retrieved once
   * and for all using the @ref find_closest_dof method of this class. That
   * index can be passed to the overloads of both @ref set_source and operator().
   *
   * ### Example of use
   * @code{.cpp}
   * std::shared_ptr<utils::MeshHandler<dim>> mesh;
   *
   * // ... initialize the mesh ...
   *
   * utils::GeodesicDistance<dim> geodesic_distance;
   *
   * // This computes (among other things) the adjacency graph of mesh vertices.
   * geodesic_distance.setup_system(mesh);
   *
   * // Set the origin point.
   * const Point<dim> origin = {0, 0, 0};
   * geodesic_distance.set_source(origin);
   *
   * // Evaluate the distance.
   * const Point<dim> destination = {1, 0, 0};
   * const double distance = geodesic_distance(destination);
   * @endcode
   *
   * See also @ref examples::ExampleGeodesicDistance.
   */
  template <int mesh_dim, int mesh_spacedim>
  class GeodesicDistance
  {
  public:
    /**
     * @brief Setup the geodesic distance evaluator.
     *
     * This method initializes the internal data structures needed to evaluate
     * the geodesic distance.
     *
     * This allows to compute the distance between arbitrary source and
     * destination points, regardless of whether they belong to the mesh and
     * whether their nearest mesh vertices is owned by the current process. As a
     * consequence, the method requires a lot of parallel communication (to
     * transfer points and the adjacency graph to all processes). If you know in
     * advance that the current process will only require a specific set of
     * source points and will always call operator() with a known (maximum)
     * threshold, it may be significantly better to use the other overloaded
     * version of this method.
     *
     * @param[in] triangulation_ The reference mesh used to evaluate the
     * geodesic distance.
     * @param[in] compute_shortest_path_ If enabled, enables the computation of
     * shortest path (on top of shortest distances), which can be retrieved
     * through @ref get_shortest_path.
     */
    void
    setup_system(
      const std::shared_ptr<const utils::MeshHandler<mesh_dim, mesh_spacedim>>
                 &triangulation_,
      const bool &compute_shortest_path_ = false);

    /**
     * @brief Setup the geodesic distance evaluator.
     *
     * This method initializes the internal data structures needed to evaluate
     * the geodesic distance.
     *
     * This method takes as input a set of points and a threshold. The set of
     * points should contain all possible source points that are used by the
     * current process. The threshold should be the maximum threshold used when
     * calling operator(). The method takes advantage of this information to
     * minimize the parallel communication: a point, and the associated
     * adjacency information, is communicated to a process only if it is closer
     * than the threshold to one of the source points of that process.
     *
     * @param[in] triangulation_ The reference mesh used to evaluate the
     * geodesic distance.
     * @param[in] source_points The vector of source points that can be
     * requested by the current process.
     * @param[in] threshold The maximum threshold used when calling operator().
     * @param[in] compute_shortest_path_ If enabled, enables the computation of
     * shortest path (on top of shortest distances), which can be retrieved
     * through @ref get_shortest_path.
     */
    void
    setup_system(
      const std::shared_ptr<const utils::MeshHandler<mesh_dim, mesh_spacedim>>
                                              &triangulation_,
      const std::vector<Point<mesh_spacedim>> &source_points,
      const double                            &threshold,
      const bool                              &compute_shortest_path_ = false);

    /// Set the source point (i.e. the point from which the distance must be
    /// computed).
    void
    set_source(const types::global_dof_index &p, const double &offset = 0.0);

    /// Add a source point.
    void
    add_source(const types::global_dof_index &p, const double &offset = 0.0);

    /// Set the source point to the mesh vertex closest to the provided
    /// coordinates.
    void
    set_source(const Point<mesh_spacedim> &p, const double &offset = 0.0);

    /// Add a source point at the mesh vertex closest to the provided
    /// coordinates.
    void
    add_source(const Point<mesh_spacedim> &p, const double &offset = 0.0);

    /// Set a complete block point (i.e. a point from which distance becomes
    /// infinity).
    void
    set_block(const types::global_dof_index &p);

    /// Set a complete block point at mesh vertex closest to the provided
    /// coordinates.
    void
    set_block(const Point<mesh_spacedim> &p);

    /// Evaluate the geodesic distance from the source of a point with given
    /// index.
    double operator()(const types::global_dof_index &destination,
                      const std::optional<double>   &threshold = {});

    /// Evaluate the geodesic distance from the source of the vertex closest to
    /// the provided coordinates.
    double operator()(const Point<mesh_spacedim>  &destination,
                      const std::optional<double> &threshold = {});

    /// Find the closest DoF to a point.
    unsigned int
    find_closest_dof(const Point<mesh_spacedim> &p) const;

    /// Get the maximum diameter of the reference mesh.
    double
    get_diameter_max() const
    {
      return triangulation->get_info().get_diameter_max();
    }

    /// Clear all data.
    void
    clear();

    /// @name Shortest path computation.
    /// @{

    /// Alias for a path.
    using Path = std::vector<std::pair<Point<mesh_spacedim>, double>>;

    /// Get the shortest path from the source point to a point. Returns a vector
    /// of pairs where each entry contains the coordinates of a point on the
    /// shortest path and the associated geoedsic distance.
    Path
    get_shortest_path(const types::global_dof_index &destination);

    /// Get the shortest path from the source point to a point. Returns a vector
    /// of pairs where each entry contains the coordinates of a point on the
    /// shortest path and the associated geoedsic distance.
    Path
    get_shortest_path(const Point<mesh_spacedim> &destination);

    /// Save a path to a file.
    void
    save_path_to_file(const Path &path, const std::string &filename);

    /// @}

  protected:
    /// Heap class to store close points.
    class PointHeap
    {
    public:
      /// Constructor.
      PointHeap(const unsigned int &n_points)
        : global_to_heap_index(n_points, -1)
      {}

      /// Insert new element into the heap.
      void
      insert(const types::global_dof_index &p, const double &distance);

      /// Reduce the value of a given entry.
      void
      decrease_distance(const types::global_dof_index &p,
                        const double                  &distance);

      /// Remove the point with minimum distance from the heap, and return it.
      std::pair<types::global_dof_index, double>
      pop_min();

      /// Retrieve the size of the heap.
      inline unsigned int
      size() const
      {
        return data.size();
      }

      /// Check whether the container is empty.
      inline bool
      empty() const
      {
        return size() == 0;
      }

      /// Clear the content of this container.
      inline void
      clear()
      {
        data.clear();
      }

    protected:
      /// Restore the heap property.
      void
      min_heapify(const unsigned int &i);

      /// Swap an element with its parent.
      void
      swap_up(const unsigned int &i);

      /// Retrieve parent of a given node.
      unsigned int
      parent(const unsigned int &i) const
      {
        return (i - 1) / 2;
      }

      /// Retrieve left child of a given node.
      unsigned int
      left(const unsigned int &i) const
      {
        return 2 * i + 1;
      }

      /// Retrieve right child of a given node.
      unsigned int
      right(const unsigned int &i) const
      {
        return 2 * i + 2;
      }

      /// Internal data.
      std::vector<std::pair<types::global_dof_index, double>> data;

      /// Vector that, to each global index, associates the index inside the
      /// heap.
      std::vector<int> global_to_heap_index;
    };

    /// Class to represent a row of the adjacency graph.
    class AdjacencyRow
    {
    public:
      /// Adjacency row iterator.
      class Iterator
      {
      public:
        /// Constructor.
        Iterator(const AdjacencyRow &row_, const unsigned int &index_ = 0)
          : row(row_)
          , index(index_)
        {}

        /// Increment operator.
        Iterator &
        operator++()
        {
          ++index;
          return *this;
        }

        /// Equality operator.
        bool
        operator==(const Iterator &other) const
        {
          return index == other.index;
        }

        /// Inequality operator.
        bool
        operator!=(const Iterator &other) const
        {
          return index != other.index;
        }

        /// Get the column pointed by the iterator.
        const types::global_dof_index &
        col() const
        {
          return row.cols[index];
        }

        /// Get the value pointed by the iterator.
        const double &
        val() const
        {
          return row.vals[index];
        }

      protected:
        /// Row pointed by the iterator.
        const AdjacencyRow &row;

        /// Current index within the row.
        unsigned int index;
      };

      /// The iterator class needs to access the column indices and the
      /// associated values.
      friend class Iterator;

      /// Insert a new entry into the row.
      void
      add(const types::global_dof_index &col, const double &val)
      {
        cols.push_back(col);
        vals.push_back(val);
        ++size;
      }

      /// Get an iterator to the first element.
      Iterator
      begin() const
      {
        return Iterator(*this);
      }

      /// Get a one-past-end iterator.
      Iterator
      end() const
      {
        return Iterator(*this, size);
      }

      /// Boost serialization, needed for MPI communication.
      template <class Archive>
      void
      serialize(Archive &ar, const unsigned int /*version*/)
      {
        ar &cols;
        ar &vals;
        ar &size;
      }

    protected:
      std::vector<types::global_dof_index> cols;     ///< Entry column indices.
      std::vector<double>                  vals;     ///< Entry values.
      unsigned int                         size = 0; ///< Row size.
    };

    /// Helper to remove an index from an IndexSet.
    void
    remove_index(IndexSet &set, const types::global_dof_index &i)
    {
      IndexSet set_to_remove(set.size());
      set_to_remove.add_index(i);

      set.subtract_set(set_to_remove);
    }

    /**
     * @brief Internal initialization method.
     *
     * Set up the points vector and the adjacency graph for locally relevant
     * mesh vertices. The caller of this method is then responsible for parallel
     * communication.
     */
    void
    setup_system_internal(
      const std::shared_ptr<const utils::MeshHandler<mesh_dim, mesh_spacedim>>
                 &triangulation_,
      const bool &compute_shortest_path_);

    /**
     * @brief Build the RTree structure used to retrieve points.
     */
    void
    build_points_rtree(const IndexSet &relevant_points);

    /**
     * @brief Check whether a given index is that of a source point.
     */
    bool
    is_source(const types::global_dof_index &i) const
    {
      for (const auto &[source, offset] : sources)
        if (source == i)
          return true;

      return false;
    }

    /// Mesh.
    std::shared_ptr<const utils::MeshHandler<mesh_dim, mesh_spacedim>>
      triangulation;

    /// DoF handler.
    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;

    /// Vector storing, for each mesh vertex, its coordinates.
    std::vector<Point<mesh_spacedim>> points_vector;

    /// Adjacency graph of the mesh nodes.
    std::vector<AdjacencyRow> adjacency;

    /// Source points. For each, we store an index and a distance offset.
    std::vector<std::pair<types::global_dof_index, double>> sources;

    /// Flag indicating whether the source point was initialized.
    bool source_initialized = false;

    /// Vector of distances from the source point.
    std::vector<double> distances;

    /// Point status: 0 = unvisited; 1 = close (i.e. on the propagation front);
    /// 2 = alive (i.e. behind the propagation front).
    std::vector<unsigned int> status;

    /// Heap of close points.
    std::unique_ptr<PointHeap> close_heap;

    /// Alias of an indexed point, i.e. a pair of a point and the associated
    /// global index.
    using IndexedPoint = std::pair<Point<dim>, types::global_dof_index>;

    /// RTree of mesh nodes. We don't use the DoFLocator class here, because
    /// that class assumes that all processes always participate in every query
    /// (i.e. the search for a point is a collective operation), whereas this
    /// class needs to be callable by every process independently.
    RTree<IndexedPoint> nodes_rtree;

    /// Toggle the computation of shortest paths on top of their lengths.
    bool compute_shortest_path;

    /// Map that to each visited node associates its predecessor along the
    /// shortest path that leads to the source point. Travelling backwards along
    /// this map allows to retrieve the shortest path.
    std::map<types::global_dof_index, types::global_dof_index> predecessor;
  };
} // namespace lifex::utils

#endif
