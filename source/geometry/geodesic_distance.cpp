/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/geometry/geodesic_distance.hpp"

#include "source/numerics/numbers.hpp"

#include <deal.II/base/bounding_box.h>

#include <algorithm>
#include <limits>
#include <set>

namespace lifex::utils
{
  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::PointHeap::insert(
    const types::global_dof_index &p,
    const double                  &distance)
  {
    data.push_back(std::make_pair(p, distance));
    global_to_heap_index[p] = data.size() - 1;

    unsigned int i = data.size() - 1;
    while (i != 0 && data[parent(i)].second > data[i].second)
      {
        swap_up(i);
        i = parent(i);
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::PointHeap::decrease_distance(
    const types::global_dof_index &p,
    const double                  &distance)
  {
    int i = global_to_heap_index[p];

    data[i].second = distance;

    while (i != 0 && data[parent(i)].second > data[i].second)
      {
        swap_up(i);
        i = parent(i);
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  std::pair<types::global_dof_index, double>
  GeodesicDistance<mesh_dim, mesh_spacedim>::PointHeap::pop_min()
  {
    const std::pair<types::global_dof_index, double> result = data[0];

    data[0] = data.back();
    min_heapify(0);
    data.pop_back();

    global_to_heap_index[result.first] = -1;

    return result;
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::PointHeap::min_heapify(
    const unsigned int &i)
  {
    const unsigned int l = left(i);
    const unsigned int r = right(i);

    unsigned int smallest = i;
    if (l < data.size() && data[l].second < data[smallest].second)
      smallest = l;
    if (r < data.size() && data[r].second < data[smallest].second)
      smallest = r;

    if (smallest != i)
      {
        swap_up(smallest);
        min_heapify(smallest);
      }
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::PointHeap::swap_up(
    const unsigned int &i)
  {
    const unsigned int p = parent(i);

    const auto &global_child  = data[i].first;
    const auto &global_parent = data[p].first;

    global_to_heap_index[global_child]  = p;
    global_to_heap_index[global_parent] = i;

    std::swap(data[i], data[p]);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::setup_system_internal(
    const std::shared_ptr<const utils::MeshHandler<mesh_dim, mesh_spacedim>>
               &triangulation_,
    const bool &compute_shortest_path_)
  {
    triangulation         = triangulation_;
    compute_shortest_path = compute_shortest_path_;

    // We setup a dummy FE space and DoF handler.
    auto fe_linear = triangulation->get_fe_lagrange(1);

    dof_handler.reinit(triangulation->get());
    dof_handler.distribute_dofs(*fe_linear);

    const unsigned int n_dofs     = dof_handler.n_dofs();
    const IndexSet     owned_dofs = dof_handler.locally_owned_dofs();

    // Setup the vector of point coordinates.
    {
      auto mapping_linear = triangulation->get_linear_mapping();
      const std::map<types::global_dof_index, Point<mesh_spacedim>> points =
        DoFTools::map_dofs_to_support_points(*mapping_linear, dof_handler);

      points_vector.resize(n_dofs);
      for (const auto &p : points)
        points_vector[p.first] = p.second;
    }

    // Build the adjacency graph.
    {
      adjacency.resize(n_dofs);

      const unsigned int dofs_per_cell = fe_linear->dofs_per_cell;
      std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

      for (const auto &cell : dof_handler.active_cell_iterators())
        {
          if (!cell->is_locally_owned() && !cell->is_ghost())
            continue;

          cell->get_dof_indices(dof_indices);

          for (unsigned int i = 0; i < dofs_per_cell; ++i)
            {
              const Point<mesh_spacedim> &p_i = points_vector[dof_indices[i]];

              for (unsigned int j = i + 1; j < dofs_per_cell; ++j)
                {
                  const Point<mesh_spacedim> &p_j =
                    points_vector[dof_indices[j]];
                  const double distance = p_i.distance(p_j);

                  adjacency[dof_indices[i]].add(dof_indices[j], distance);
                  adjacency[dof_indices[j]].add(dof_indices[i], distance);
                }
            }
        }
    }

    distances.resize(n_dofs);
    status.resize(n_dofs, 0);
    close_heap = std::make_unique<PointHeap>(n_dofs);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::build_points_rtree(
    const IndexSet &relevant_points)
  {
    std::vector<IndexedPoint> indexed_points;
    for (const auto &i : relevant_points)
      indexed_points.push_back(std::make_pair(points_vector[i], i));

    nodes_rtree = pack_rtree(indexed_points);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::setup_system(
    const std::shared_ptr<const utils::MeshHandler<mesh_dim, mesh_spacedim>>
               &triangulation_,
    const bool &compute_shortest_path_)
  {
    setup_system_internal(triangulation_, compute_shortest_path_);

    // Parallel communication: we send everything to everyone.
    {
      utils::MPI::compute_vector_union(points_vector,
                                       dof_handler.locally_owned_dofs(),
                                       Core::mpi_comm);

      // Merge the adjacency graphs across processes.
      const IndexSet owned_dofs = dof_handler.locally_owned_dofs();
      const std::vector<unsigned int> index_owners =
        Utilities::MPI::compute_index_owner(
          owned_dofs, complete_index_set(owned_dofs.size()), Core::mpi_comm);

      for (unsigned int i = 0; i < adjacency.size(); ++i)
        adjacency[i] = Utilities::MPI::broadcast(Core::mpi_comm,
                                                 adjacency[i],
                                                 index_owners[i]);
    }

    build_points_rtree(complete_index_set(dof_handler.n_dofs()));
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::setup_system(
    const std::shared_ptr<const utils::MeshHandler<mesh_dim, mesh_spacedim>>
                                            &triangulation_,
    const std::vector<Point<mesh_spacedim>> &source_points,
    const double                            &threshold,
    const bool                              &compute_shortest_path_)
  {
    setup_system_internal(triangulation_, compute_shortest_path_);

    // For parallel communication, we build a bounding box that contains all
    // possible source points on the current process, enlarged by the threshold.
    BoundingBox<mesh_spacedim> bbox(source_points);
    bbox.extend(threshold + get_diameter_max());

    // Boxes are communicated to every process.
    const std::vector<BoundingBox<mesh_spacedim>> all_bboxes =
      Utilities::MPI::all_gather(Core::mpi_comm, bbox);

    // We send a point (and the associated row in the adjacency matrix) to a
    // rank if that point falls within the bounding box of that rank.
    std::map<unsigned int, std::vector<IndexedPoint>> points_to_send;
    std::map<unsigned int,
             std::vector<std::pair<AdjacencyRow, types::global_dof_index>>>
      adjacency_to_send;

    for (unsigned int rank = 0; rank < Core::mpi_size; ++rank)
      {
        if (rank == Core::mpi_rank)
          continue;

        const BoundingBox<mesh_spacedim> &box_rank = all_bboxes[rank];

        for (const auto &i : dof_handler.locally_owned_dofs())
          {
            if (box_rank.point_inside(points_vector[i]))
              {
                points_to_send[rank].push_back(
                  std::make_pair(points_vector[i], i));
                adjacency_to_send[rank].push_back(
                  std::make_pair(adjacency[i], i));
              }
          }
      }

    const std::map<unsigned int, std::vector<IndexedPoint>> points_received =
      Utilities::MPI::some_to_some(Core::mpi_comm, points_to_send);
    const std::map<
      unsigned int,
      std::vector<std::pair<AdjacencyRow, types::global_dof_index>>>
      adjacency_received =
        Utilities::MPI::some_to_some(Core::mpi_comm, adjacency_to_send);

    // We take the received points and adjacency rows and fill the respective
    // data structures.
    std::set<types::global_dof_index> relevant_points_set;
    for (const auto &points_rank : points_received)
      for (const auto &p : points_rank.second)
        {
          points_vector[p.second] = p.first;
          relevant_points_set.insert(p.second);
        }

    for (const auto &adjacency_rank : adjacency_received)
      for (const auto &a : adjacency_rank.second)
        adjacency[a.second] = a.first;

    IndexSet relevant_points = dof_handler.locally_owned_dofs();
    relevant_points.add_indices(relevant_points_set.begin(),
                                relevant_points_set.end());
    build_points_rtree(relevant_points);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::set_source(
    const types::global_dof_index &p,
    const double                  &offset)
  {
    // If the source was already initialized, there is only one source point and
    // it coincides with the provided one (with the same offset), then we do
    // nothing. This way, we reuse already computed distances.
    if (source_initialized && sources.size() == 1 && sources[0].first == p &&
        utils::is_equal(sources[0].second, offset))
      return;

    // Reset previous data.
    sources.clear();
    close_heap->clear();
    std::fill(distances.begin(),
              distances.end(),
              std::numeric_limits<double>::max());
    std::fill(status.begin(), status.end(), 0);

    add_source(p, offset);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::add_source(
    const types::global_dof_index &p,
    const double                  &offset)
  {
    // If the source was already initialized, and the provided point is already
    // a source point, with the same offset, we do nothing. This way, we reuse
    // already computed distances.
    if (source_initialized && is_source(p))
      {
        for (const auto &[s, o] : sources)
          if (s == p && utils::is_equal(o, offset))
            return;
      }

    sources.push_back(std::make_pair(p, offset));
    source_initialized = true;

    distances[p] = offset;
    status[p]    = 1;
    close_heap->insert(p, offset);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::set_source(
    const Point<mesh_spacedim> &p,
    const double               &offset)
  {
    set_source(find_closest_dof(p), offset);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::add_source(
    const Point<mesh_spacedim> &p,
    const double               &offset)
  {
    add_source(find_closest_dof(p), offset);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::set_block(
    const types::global_dof_index &p)
  {
    distances[p] = std::numeric_limits<double>::max();
    status[p]    = 2;
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::set_block(
    const Point<mesh_spacedim> &p)
  {
    set_block(find_closest_dof(p));
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  GeodesicDistance<mesh_dim, mesh_spacedim>::operator()(
    const types::global_dof_index &destination,
    const std::optional<double>   &threshold)
  {
    AssertThrow(source_initialized,
                ExcMessage("The source point must be set with set_source() "
                           "before calling operator()."));

    if (status[destination] == 2)
      return distances[destination];

    // If the destination is further than the threshold from all source points,
    // we return the threshold.
    if (threshold.has_value())
      {
        const Point<dim> &dest_p = points_vector[destination];
        bool              found  = false;

        for (const auto &[source, offset] : sources)
          if (points_vector[source].distance(dest_p) + offset <
              threshold.value())
            {
              found = true;
              break;
            }

        if (!found)
          return threshold.value();
      }

    const double thr_value =
      threshold.value_or(std::numeric_limits<double>::max());

    while (!close_heap->empty())
      {
        // Retrieve the point in the close set with the smallest distance.
        const auto          trial_point = close_heap->pop_min();
        const unsigned int &i           = trial_point.first;
        const double       &d_i         = distances[i];

        // Remove it from the close set and add it to the alive set.
        status[i] = 2;

        const auto &adjacency_row = adjacency[i];
        auto        end_it        = adjacency_row.end();

        // Update the distance of each of its neighbors, and mark them as close.
        for (auto it = adjacency_row.begin(); it != end_it; ++it)
          {
            const types::global_dof_index &j        = it.col();
            unsigned int                  &status_j = status[j];

            if (status_j != 2)
              {
                double      &d_j     = distances[j];
                const double new_d_j = d_i + it.val();

                if (status_j == 0)
                  {
                    status_j = 1;
                    d_j      = new_d_j;

                    if (thr_value < d_j)
                      d_j = threshold.value();

                    close_heap->insert(j, d_j);

                    if (compute_shortest_path)
                      predecessor[j] = i;
                  }
                else if (/* status_j == 1 && */ new_d_j < d_j)
                  {
                    d_j = new_d_j;

                    if (thr_value < d_j)
                      d_j = threshold.value();

                    close_heap->decrease_distance(j, d_j);

                    if (compute_shortest_path)
                      predecessor[j] = i;
                  }
              }
          }

        // If the current point is farther than the threshold, we return the
        // threshold.
        if (threshold.has_value() && trial_point.second >= threshold.value())
          return threshold.value();

        // If the current point is the destination one, we return its distance.
        if (i == destination)
          return trial_point.second;
      }

    return std::numeric_limits<double>::max();
  }

  template <int mesh_dim, int mesh_spacedim>
  double
  GeodesicDistance<mesh_dim, mesh_spacedim>::operator()(
    const Point<mesh_spacedim>  &destination,
    const std::optional<double> &threshold)
  {
    return (*this)(find_closest_dof(destination), threshold);
  }

  template <int mesh_dim, int mesh_spacedim>
  unsigned int
  GeodesicDistance<mesh_dim, mesh_spacedim>::find_closest_dof(
    const Point<mesh_spacedim> &p) const
  {
    std::vector<IndexedPoint> nearest;
    nodes_rtree.query(boost::geometry::index::nearest(p, 1),
                      std::back_inserter(nearest));

    return nearest[0].second;
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::clear()
  {
    triangulation = nullptr;
    close_heap    = nullptr;

    source_initialized = false;

    dof_handler.clear();
    points_vector.clear();
    adjacency.clear();
    distances.clear();
    status.clear();

    compute_shortest_path = false;
    predecessor.clear();
  }

  template <int mesh_dim, int mesh_spacedim>
  typename GeodesicDistance<mesh_dim, mesh_spacedim>::Path
  GeodesicDistance<mesh_dim, mesh_spacedim>::get_shortest_path(
    const types::global_dof_index &destination)
  {
    Assert(compute_shortest_path,
           ExcMessage("Shortest path computation must be enabled when "
                      "initializing this class."));

    // Compute distance and shortest path tree.
    (*this)(destination);

    // Reconstruct the shortest path.
    std::vector<std::pair<Point<mesh_spacedim>, double>> shortest_path;
    types::global_dof_index                              cur = destination;

    while (!is_source(cur))
      {
        shortest_path.push_back(
          std::make_pair(points_vector[cur], distances[cur]));

        cur = predecessor.at(cur);
      }

    // Insert the source point.
    shortest_path.push_back(std::make_pair(points_vector[cur], distances[cur]));

    // Flip the shortest path vector (so that it begins at the source node and
    // ends at the destination node).
    return std::vector<std::pair<Point<mesh_spacedim>, double>>(
      shortest_path.rbegin(), shortest_path.rend());
  }

  template <int mesh_dim, int mesh_spacedim>
  typename GeodesicDistance<mesh_dim, mesh_spacedim>::Path
  GeodesicDistance<mesh_dim, mesh_spacedim>::get_shortest_path(
    const Point<mesh_spacedim> &destination)
  {
    const types::global_dof_index destination_idx =
      find_closest_dof(destination);

    return get_shortest_path(destination_idx);
  }

  template <int mesh_dim, int mesh_spacedim>
  void
  GeodesicDistance<mesh_dim, mesh_spacedim>::save_path_to_file(
    const Path        &path,
    const std::string &filename)
  {
    // We need to build a 1D triangulation to save the shortest path. To do so,
    // we manually build the vertices and cell data.
    Triangulation<1, mesh_spacedim> triangulation;
    Vector<double>                  data(path.size());

    {
      std::vector<Point<mesh_spacedim>> vertices;
      std::vector<CellData<1>>          cells;

      for (unsigned int i = 0; i < path.size(); ++i)
        {
          vertices.push_back(path[i].first);

          if (i < path.size() - 1)
            {
              CellData<1> cell_data;

              cell_data.vertices[0] = i;
              cell_data.vertices[1] = i + 1;

              cells.push_back(cell_data);
            }

          data[i] = path[i].second;
        }

      triangulation.create_triangulation(vertices, cells, SubCellData());
    }

    // We also need a finite element space and a DoF handler to represent the
    // data to be written.
    const auto fe =
      MeshHandler<1, mesh_spacedim>::get_fe_lagrange(triangulation, 1);

    DoFHandler<1, mesh_spacedim> dof_handler;
    dof_handler.reinit(triangulation);
    dof_handler.distribute_dofs(*fe);

    {
      DataOut<1, mesh_spacedim> data_out;
      data_out.add_data_vector(dof_handler, data, "distance");
      data_out.build_patches();

      std::ofstream out_file(filename);
      data_out.write_vtu(out_file);
    }
  }

  /// @cond DOXYGEN_SKIP

  template class GeodesicDistance<dim, dim>;

#if LIFEX_DIM > 2
  template class GeodesicDistance<2, dim>;
#endif

#if LIFEX_DIM > 1
  template class GeodesicDistance<1, dim>;
#endif

  /// @endcond

} // namespace lifex::utils
