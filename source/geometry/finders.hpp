/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#ifndef LIFEX_UTILS_FINDERS_HPP_
#define LIFEX_UTILS_FINDERS_HPP_

#include "source/lifex.hpp"

#include <deal.II/numerics/rtree.h>

#include <limits>
#include <map>
#include <set>
#include <tuple>
#include <utility>
#include <vector>

namespace lifex::utils
{
  /**
   * @brief DoF locator base class.
   *
   * This class exposes an interface to locate, for a given point, the closest
   * among the DoF support points (or a subset thereof) for a given DoF handler.
   *
   * The class should not be used directly, but only through one of its derived
   * classes. Nonetheless, general information about their usage is reported
   * below.
   *
   * ### Parallel use
   * Even if used in parallel, it returns the global index of the closest DoF,
   * regardless of whether that DoF is owned by the current process or not. To
   * facilitate retrieving information associated to that DoF, the @ref find
   * method also returns the rank of its owner.
   *
   * ### Nearest-neighbor search algorithm
   * By default, this class (and the derived ones) builds an R-tree structure
   * with the DoF support points, and uses it to find the nearest neighbor to
   * the argument of the @ref find method. The R-tree is built by the
   * @ref setup_internal method, which should be called by derived classes
   * (ideally in the constructor) after retrieving the support points.
   *
   * This is advantageous for repeated calls to find. However, building the
   * R-tree may be needlessly expensive for single-query use cases. Therefore,
   * it is possible to replace the R-tree lookup with a simpler linear search by
   * setting the constructor argument use_rtree to false.
   */
  class DoFLocatorBase
  {
  public:
    /// Alias for a pair of a point and the associated DoF index.
    using PointIndex = std::pair<Point<dim>, types::global_dof_index>;

    /// Alias for a pair of a DoF index and the rank that owns it, as returned
    /// by the find method.
    using FindResult = std::pair<types::global_dof_index, unsigned int>;

    /**
     * @brief Locate the closest DoF to a specified point.
     *
     * @return A pair composed of the global index of the closest DoF
     * (regardless of whether it is owned by the current process or not) and the
     * rank of the process owning that DoF.
     */
    FindResult
    find(const Point<dim> &point) const;

    /// Get the point with a given index.
    const Point<dim> &
    support_point(const types::global_dof_index &i) const
    {
      return support_points.at(i);
    }

  protected:
    /// Constructor.
    DoFLocatorBase(const bool &use_rtree_ = true)
      : use_rtree(use_rtree_)
    {}

    /**
     * @brief Internal initialization.
     *
     * This method builds the R-tree structure, if needed. It should be called
     * by derived classes after initializing the support_points and owned_dofs
     * members.
     */
    void
    setup_internal();

    /// Owned DoF indices.
    IndexSet owned_dofs;

    /// Map of the DoF support points.
    std::map<types::global_dof_index, Point<dim>> support_points;

    /// Toggle using an R-tree to locate points.
    const bool use_rtree;

    /// R-tree storing the support points.
    RTree<PointIndex> r_tree;
  };

  /**
   * @brief Helper class to look up the closest DoFs to a given point.
   */
  class DoFLocator : public DoFLocatorBase
  {
  public:
    /**
     * @brief Constructor.
     *
     * @param[in] mapping     Mapping associated to the DoF handler.
     * @param[in] dof_handler The DoF handler containing the DoFs to be located.
     * @param[in] component   For vector-valued elements, this selects the
     *                        vector component of the DoFs to be located.
     * @param[in] use_rtree   Toggle using the R-tree structure for nearest-
     *                        neighbor search.
     */
    DoFLocator(const Mapping<dim>    &mapping,
               const DoFHandler<dim> &dof_handler,
               const unsigned int    &component = 0,
               const bool            &use_rtree = true);

    /**
     * @brief Constructor.
     *
     * Same as the above, assuming a default linear mapping.
     */
    DoFLocator(const DoFHandler<dim> &dof_handler,
               const unsigned int    &component = 0,
               const bool            &use_rtree = true);
  };

  /**
   * @brief Helper class to look up the closes boundary DoFs to a given point.
   */
  class BoundaryDoFLocator : public DoFLocatorBase
  {
  public:
    /**
     * @brief Constructor.
     *
     * @param[in] mapping     Mapping associated to the DoF handler.
     * @param[in] dof_handler The DoF handler containing the DoFs to be located.
     * @param[in] boundary_ids The ids associated to the boundary where the DoFs
     *                        must be located. If empty, the whole boundary is
     *                        considered.
     * @param[in] component   For vector-valued elements, this selects the
     *                        vector component of the DoFs to be located.
     * @param[in] use_rtree   Toggle using the R-tree structure for nearest-
     *                        neighbor search.
     */
    BoundaryDoFLocator(const Mapping<dim>                 &mapping,
                       const DoFHandler<dim>              &dof_handler,
                       const std::set<types::boundary_id> &boundary_ids = {},
                       const unsigned int                 &component    = 0,
                       const bool                         &use_rtree    = true);

    /**
     * @brief Constructor.
     *
     * Same as the above, assuming a default linear mapping.
     */
    BoundaryDoFLocator(const DoFHandler<dim>              &dof_handler,
                       const std::set<types::boundary_id> &boundary_ids = {},
                       const unsigned int                 &component    = 0,
                       const bool                         &use_rtree    = true);
  };

} // namespace lifex::utils

#endif /* LIFEX_UTILS_FINDERS_HPP_ */
