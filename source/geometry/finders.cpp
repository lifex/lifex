/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/core.hpp"

#include "source/geometry/finders.hpp"
#include "source/geometry/mesh_handler.hpp"

#include "source/numerics/numbers.hpp"

#include <algorithm>

namespace lifex::utils
{
  void
  DoFLocatorBase::setup_internal()
  {
    if (use_rtree)
      {
        std::vector<PointIndex> point_indices;
        point_indices.reserve(owned_dofs.n_elements());

        for (const auto &[idx, p] : support_points)
          if (owned_dofs.is_element(idx))
            point_indices.push_back(std::make_pair(p, idx));

        r_tree = pack_rtree(point_indices.begin(), point_indices.end());
      }
  }

  DoFLocatorBase::FindResult
  DoFLocatorBase::find(const Point<dim> &point) const
  {
    double dist_square_min         = std::numeric_limits<double>::max();
    types::global_dof_index id_min = 0;

    if (use_rtree)
      {
        // The tree may be empty if the current process owns no valid point.
        // This may happen e.g. if we look for boundary points with a specific
        // tag, and the current process does not own any point on that boundary
        // (see BoundaryDoFLocator).
        if (r_tree.size() > 0)
          {
            namespace bgi = boost::geometry::index;

            std::vector<PointIndex> query_result;
            r_tree.query(bgi::nearest(point, 1),
                         std::back_inserter(query_result));

            dist_square_min = query_result[0].first.distance_square(point);
            id_min          = query_result[0].second;
          }
      }
    else
      {
        for (const auto &[idx, p] : support_points)
          {
            if (!owned_dofs.is_element(idx))
              continue;

            const double dist_square = point.distance_square(p);

            if (dist_square <= dist_square_min)
              {
                dist_square_min = dist_square;
                id_min          = idx;
              }
          }
      }

    // Gather all distance_min across all the processors.
    const std::vector<double> distance_min_vec =
      Utilities::MPI::all_gather(Core::mpi_comm, dist_square_min);

    const std::vector<types::global_dof_index> id_min_vec =
      Utilities::MPI::all_gather(Core::mpi_comm, id_min);

    // Get rank index of absolute minimum.
    const size_t rank =
      std::min_element(distance_min_vec.begin(), distance_min_vec.end()) -
      distance_min_vec.begin();

    return std::make_pair(id_min_vec[rank], rank);
  }

  DoFLocator::DoFLocator(const Mapping<dim>    &mapping,
                         const DoFHandler<dim> &dof_handler,
                         const unsigned int    &component,
                         const bool            &use_rtree)
    : DoFLocatorBase(use_rtree)
  {
    ComponentMask mask(dof_handler.get_fe().n_components(), false);
    mask.set(component, true);

    support_points =
      DoFTools::map_dofs_to_support_points(mapping, dof_handler, mask);

    owned_dofs = dof_handler.locally_owned_dofs();

    setup_internal();
  }

  DoFLocator::DoFLocator(const DoFHandler<dim> &dof_handler,
                         const unsigned int    &component,
                         const bool            &use_rtree_)
    : DoFLocator(*utils::MeshHandler<dim>::get_linear_mapping(
                   dof_handler.get_triangulation()),
                 dof_handler,
                 component,
                 use_rtree_)
  {}

  BoundaryDoFLocator::BoundaryDoFLocator(
    const Mapping<dim>                 &mapping,
    const DoFHandler<dim>              &dof_handler,
    const std::set<types::boundary_id> &boundary_ids,
    const unsigned int                 &component,
    const bool                         &use_rtree)
    : DoFLocatorBase(use_rtree)
  {
    ComponentMask mask(dof_handler.get_fe().n_components(), false);
    mask.set(component, true);

    DoFTools::map_dofs_to_support_points(mapping,
                                         dof_handler,
                                         support_points,
                                         mask);

    owned_dofs = dof_handler.locally_owned_dofs();

    const IndexSet boundary_dofs =
      DoFTools::extract_boundary_dofs(dof_handler, mask, boundary_ids);

    // We remove elements of support points that are not on the boundary.
    for (auto it = support_points.begin(); it != support_points.end();)
      {
        if (!boundary_dofs.is_element(it->first))
          it = support_points.erase(it);
        else
          ++it;
      }

    setup_internal();
  }

  BoundaryDoFLocator::BoundaryDoFLocator(
    const DoFHandler<dim>              &dof_handler,
    const std::set<types::boundary_id> &boundary_ids,
    const unsigned int                 &component,
    const bool                         &use_rtree_)
    : BoundaryDoFLocator(*utils::MeshHandler<dim>::get_linear_mapping(
                           dof_handler.get_triangulation()),
                         dof_handler,
                         boundary_ids,
                         component,
                         use_rtree_)
  {}
} // namespace lifex::utils
