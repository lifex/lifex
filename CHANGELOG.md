## Changes between v1.6.0 and v1.7.0
- **New**: Added class `VTKImporter` to centralize reading data from VTK files, possibly with serialization. (Michele Bucelli, 2024/07/02).
- **New**: Added class `RestartHandler` to manage serialization and restart for time-dependent simulations. (Michele Bucelli, 2024/06/24).
- **New**: Added `VTKFunction::gradient` to compute the gradient of signed distance functions. (Michele Bucelli, 2024/05/21).
- **New**: Added the possibility of changing `PreconditionerHandler`'s default parameters for every instance. (Michele Bucelli, 2024/03/18).

- **Changed**: `Class`: changed y. (Name Surname, yyyy/mm/dd)
- **Improved**: `Class`: z now does a, b, c. (Name Surname, yyyy/mm/dd)
- **New**: `Class`: added x. (Name Surname, yyyy/mm/dd)

## [Previous versions](doc/changes)
- [Changes between v1.5.0 and v1.6.0](doc/changes/v1.5.0_vs_v1.6.0.md).
- [Changes between v1.4.0 and v1.5.0](doc/changes/v1.4.0_vs_v1.5.0.md).
