#!/bin/bash
## ---------------------------------------------------------------------
## Copyright (C) 2021 - 2022 by the lifex authors.
##
## This file is part of lifex.
##
## lifex is free software; you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## lifex is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with lifex.  If not, see <http://www.gnu.org/licenses/>.
## ---------------------------------------------------------------------

# Author: Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.

############################
### Resource allocation. ###
############################

# No. nodes to allocate.
SLURM_N_NODES=1

# Total no. tasks to allocate.
SLURM_N_TASKS=48

# Walltime, in hh:mm:ss.
SLURM_WALLTIME=10:00:00

# Required amount of memory per node.
SLURM_MEM_PER_NODE=100G


##########################
### Job configuration. ###
##########################

# Slurm account and partition.
SLURM_JOB_ACCOUNT=IscrB_MathBeat
SLURM_JOB_PARTITION=g100_usr_prod


# Executable name.
SLURM_EXEC_NAME=./lifex_electrophysiology

# Executable command-line arguments.
SLURM_EXEC_ARGS="-f lifex_electrophysiology.prm -o output"

# Slurm job name.
SLURM_JOB_NAME=lifex_ep


#################################
### Output and notifications. ###
#################################

# Standard output/error log files.
SLURM_FILENAME_OUTPUT=${SLURM_JOB_NAME}_output.log
SLURM_FILENAME_ERROR=${SLURM_JOB_NAME}_error.log

# Email notification type.
# Valid values are, e.g.: NONE, BEGIN, END, FAIL, ALL.
SLURM_MAIL_TYPE=END

# User email.
SLURM_MAIL_ADDRESS=my@email.com
