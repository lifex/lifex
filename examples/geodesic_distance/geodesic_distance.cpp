/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/core_model.hpp"
#include "source/init.hpp"

#include "source/geometry/geodesic_distance.hpp"
#include "source/geometry/mesh_handler.hpp"

#include "source/io/data_writer.hpp"

namespace lifex::examples
{
  /**
   * @brief Example class for geodesic distance.
   *
   * For a user-provided mesh, evaluates the length of the shortest path
   * traveling between adjacent mesh points from a user-specified "source" point
   * @f$\mathbf{p}_0@f$ to all mesh nodes.
   *
   * If the source point is not a mesh vertex, the shortest path is measured
   * starting from the mesh vertex @f$\hat{\mathbf{p}}_0@f$ closest to
   * @f$\mathbf{p}_0@f$.
   *
   * The shortest path length is evaluated using @ref utils::GeodesicDistance.
   * Refer to the class documentation for further details.
   */
  template <int mesh_dim, int mesh_spacedim>
  class ExampleGeodesicDistance : public CoreModel
  {
  public:
    /// Alias for the mesh handler.
    using MeshHandler = utils::MeshHandler<mesh_dim, mesh_spacedim>;

    /// Constructor.
    ExampleGeodesicDistance(const std::string &subsection)
      : CoreModel(subsection)
      , triangulation(std::make_shared<MeshHandler>(
          prm_subsection_path + " / Mesh",
          mpi_comm,
          std::initializer_list<utils::mesh::GeometryType>(
            {utils::mesh::GeometryType::File,
             utils::mesh::GeometryType::Hypercube})))
      , output_handler(subsection + " / Output",
                       /* enable_time_dependent = */ false,
                       /* standalone = */ true,
                       /* default_filename = */ "geodesic_distance")
    {}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.enter_subsection_path(prm_subsection_path);
      {
        params.declare_entry(
          "Source point",
          utils::PointPattern<dim>::default_value(),
          utils::PointPattern<dim>(),
          "Point from which the geodesic distance is computed.");

        params.declare_entry("Threshold",
                             "1",
                             Patterns::Double(0),
                             "If a point is further from the source than the "
                             "threshold, its distance is not computed.");

        params.declare_entry(
          "Propagation speed",
          "1.0",
          Patterns::Double(0),
          "The output distance is rescaled dividing by this factor.");

        params.enter_subsection("Compute shortest path");
        {
          params.declare_entry(
            "Enable",
            "false",
            Patterns::Bool(),
            "If enabled, also computes and exports the shortest path from the "
            "source to a target point, specified below.");

          params.declare_entry("Destination point",
                               utils::PointPattern<dim>::default_value(),
                               utils::PointPattern<dim>(),
                               "Target point for the shortest path.");
        }
        params.leave_subsection();
      }
      params.leave_subsection_path();

      triangulation->declare_parameters(params);
      output_handler.declare_parameters(params);
    }

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      params.parse();

      params.enter_subsection_path(prm_subsection_path);
      {
        prm_source    = params.get_point<mesh_spacedim>("Source point", ",");
        prm_threshold = params.get_double("Threshold");
        prm_propagation_speed = params.get_double("Propagation speed");

        params.enter_subsection("Compute shortest path");
        {
          prm_compute_shortest_path = params.get_bool("Enable");
          prm_shortest_path_destination =
            params.get_point<mesh_spacedim>("Destination point", ",");
        }
        params.leave_subsection();
      }
      params.leave_subsection_path();

      triangulation->parse_parameters(params);
    }

    /// Run the example.
    virtual void
    run() override
    {
      // Setup.
      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Setup system");

        if constexpr (mesh_dim == mesh_spacedim)
          triangulation
            ->template create_mesh<utils::mesh::ParallelType::Distributed>();
        else
          triangulation
            ->template create_mesh<utils::mesh::ParallelType::Shared>();

        fe = triangulation->get_fe_lagrange(1);

        dof_handler.reinit(triangulation->get());
        dof_handler.distribute_dofs(*fe);

        const IndexSet owned_dofs = dof_handler.locally_owned_dofs();
        const IndexSet relevant_dofs =
          DoFTools::extract_locally_relevant_dofs(dof_handler);

        distance_owned.reinit(owned_dofs, mpi_comm);
        distance.reinit(owned_dofs, relevant_dofs, mpi_comm);

        triangulation->get_info().print(prm_subsection_path,
                                        dof_handler.n_dofs(),
                                        false);

        geodesic_distance.setup_system(triangulation,
                                       prm_compute_shortest_path);
        geodesic_distance.set_source(prm_source);
      }

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Compute distance");

        for (auto idx : distance_owned.locally_owned_elements())
          distance_owned[idx] =
            geodesic_distance(idx, prm_threshold) / prm_propagation_speed;

        distance = distance_owned;
      }

      // Output.
      if (output_handler.is_active())
        {
          TimerOutput::Scope timer_section(timer_output,
                                           prm_subsection_path +
                                             " / Output results");

          pcout << "Writing output" << std::endl;

          output_handler.add_data_vector(dof_handler, distance, "distance");
          output_handler.write();
        }

      // Compute shortest path, if requested.
      if (prm_compute_shortest_path)
        {
          TimerOutput::Scope timer_section(timer_output,
                                           prm_subsection_path +
                                             " / Compute shortest path");

          pcout << "Computing shortest path" << std::flush;

          const auto path =
            geodesic_distance.get_shortest_path(prm_shortest_path_destination);

          pcout << " (" << path.size() << " nodes)" << std::endl;

          geodesic_distance.save_path_to_file(path,
                                              Core::prm_output_directory +
                                                "/shortest_path.vtu");
        }
    }

  protected:
    /// Mesh.
    std::shared_ptr<MeshHandler> triangulation;

    /// Geodesic distance evaluator.
    utils::GeodesicDistance<mesh_dim, mesh_spacedim> geodesic_distance;

    /// DoF handler.
    DoFHandler<mesh_dim, mesh_spacedim> dof_handler;

    /// Finite element space.
    std::unique_ptr<FiniteElement<mesh_dim, mesh_spacedim>> fe;

    /// Distance vector, without ghost elements.
    LinAlg::MPI::Vector distance_owned;

    /// Distance vector, including ghost elements.
    LinAlg::MPI::Vector distance;

    /// Output handler.
    utils::OutputHandler output_handler;

    /// @name Parameters read from file.
    /// @{

    /// Source point.
    Point<mesh_spacedim> prm_source;

    /// Threshold.
    double prm_threshold;

    /// Propagation speed.
    double prm_propagation_speed;

    /// Toggle computation of shortest path to a user-defined destination point.
    bool prm_compute_shortest_path;

    /// Destination of the shortest path.
    Point<mesh_spacedim> prm_shortest_path_destination;

    /// @}
  };
} // namespace lifex::examples


/// Example geodesic distance.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::examples::ExampleGeodesicDistance<lifex::dim, lifex::dim> example(
        "Example geodesic distance");

      example.main_run_generate();
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
