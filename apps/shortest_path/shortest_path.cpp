/********************************************************************************
  Copyright (C) 2020 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/init.hpp"

#include "source/geometry/geodesic_distance.hpp"
#include "source/geometry/mesh_handler.hpp"

#include <boost/io/ios_state.hpp>

#include <fstream>
#include <iostream>
#include <sstream>

namespace
{
  using namespace dealii;
  using namespace lifex;

  template <int mesh_dim>
  void
  run(const std::string &filename_in,
      const double      &scaling_factor,
      const bool        &hex,
      const std::string &filename_out,
      const Point<dim>  &src,
      const Point<dim>  &dst)
  {
    auto triangulation =
      std::make_shared<utils::MeshHandler<mesh_dim, dim>>("Dummy",
                                                          Core::mpi_comm);

    triangulation->initialize_from_file(filename_in, scaling_factor);

    if (hex)
      {
        triangulation->set_element_type(utils::mesh::ElementType::Hex);
        triangulation
          ->template create_mesh<utils::mesh::ParallelType::Shared>();
      }
    else
      {
        triangulation->set_element_type(utils::mesh::ElementType::Tet);
        triangulation
          ->template create_mesh<utils::mesh::ParallelType::Distributed>();
      }

    triangulation->get_info().print("Shortest path mesh",
                                    triangulation->get().n_vertices(),
                                    false);

    utils::GeodesicDistance<mesh_dim, dim> geodesic_distance;

    geodesic_distance.setup_system(triangulation, true);
    geodesic_distance.set_source(src);

    const auto path = geodesic_distance.get_shortest_path(dst);

    {
      // Backup stream flags and manipulators.
      const boost::io::ios_all_saver iostream_backup(std::cout);

      pcout << "Starting point: ";
      for (unsigned int i = 0; i < dim; ++i)
        pcout << std::scientific << std::setprecision(6) << std::setw(9)
              << src[i] << " ";

      pcout << "\nEnding point  : ";
      for (unsigned int i = 0; i < dim; ++i)
        pcout << std::scientific << std::setprecision(6) << std::setw(9)
              << dst[i] << " ";

      pcout << "\nFound path with " << path.size() << " nodes" << std::endl;
    }

    AssertThrow(
      path.size() > 1,
      ExcMessage(
        "The path has no elements. Make sure that the start and end points are "
        "different, and that the mesh scaling factor is correct."));

    geodesic_distance.save_path_to_file(path, filename_out);
  }
} // namespace

/// Compute the shortest path between two points moving along mesh edges.
int
main(int argc, char **argv)
{
  std::string filename_in;
  double      scaling_factor = 1.0;
  std::string element_type;

  std::string filename_out;

  Point<dim> src;
  Point<dim> dst;

  bool hex = false;

  unsigned int mesh_dim = dim;

  std::string valid_mesh_dims = "";
  std::string mesh_dim_doc    = "";

  if constexpr (dim >= 1)
    {
      valid_mesh_dims += "1";
      mesh_dim_doc += "1 for a 1D network mesh";
    }

  if constexpr (dim >= 2)
    {
      valid_mesh_dims += ", 2";
      mesh_dim_doc += ", 2 for a surface mesh";
    }

  if constexpr (dim >= 3)
    {
      valid_mesh_dims += ", 3";
      mesh_dim_doc += ", 3 for a volume mesh";
    }

  using namespace clipp;
  const auto cli =
    (((required("-i", "--input-file") & value("input file", filename_in)) %
      ("Name of the input mesh file in GMSH (*.msh) format.")) &
     ((required("-o", "--output-file") & value("output file", filename_out)) %
      ("Name of the output VTU filename.")) &
     ((required("-s", "--start-point") & value("x", src[0]) &
       value("y", src[1]) & value("z", src[2])) %
      ("Coordinates of the initial endpoint.")) &
     ((required("-e", "--end-point") & value("x", dst[0]) & value("y", dst[1]) &
       value("z", dst[2])) %
      ("Coordinates of the final endpoint.")) &
     ((option("-d", "--mesh-dimension") & value("dim", mesh_dim)) %
      ("Dimension of the input mesh (" + mesh_dim_doc + "). Defaults to " +
       std::to_string(mesh_dim) + ".")) &
     ((option("--scaling") & value("factor", scaling_factor)) %
      ("Scaling factor applied to the input mesh. It will also be applied to "
       "the output path. Defaults to 1.0.")) &
     ((option("--hex").set(hex, true) | option("--tet").set(hex, false)) %
      ("Toggle between hexahedral and tetrahedral elements (or quadrilateral "
       "and triangular). Notice that 1D meshes must be regarded as hexahedral. "
       "Defaults to tetrahedra.")));

  lifex::lifex_init lifex_initializer(argc, argv, 1, false, cli);

  try
    {
      if constexpr (dim >= 3)
        {
          if (mesh_dim == 3)
            run<3>(filename_in, scaling_factor, hex, filename_out, src, dst);
        }

      if constexpr (dim >= 2)
        {
          if (mesh_dim == 2)
            run<2>(filename_in, scaling_factor, hex, filename_out, src, dst);
        }

      if constexpr (dim >= 1)
        {
          if (mesh_dim == 1)
            run<1>(filename_in, scaling_factor, hex, filename_out, src, dst);
        }

      AssertThrow(false,
                  ExcMessage("Mesh dimension must be one of " +
                             valid_mesh_dims + "."));
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
