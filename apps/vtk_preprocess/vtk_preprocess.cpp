/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Matteo Salvador <matteo1.salvador@polimi.it>.
 * @author Pasquale Claudio Africa <pasqualeclaudio.africa@polimi.it>.
 * @author Marco Fedele <marco.fedele@polimi.it>.
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/core_model.hpp"
#include "source/init.hpp"

#include "source/geometry/mesh_handler.hpp"
#include "source/geometry/move_mesh.hpp"

#include "source/io/serialization.hpp"
#include "source/io/vtk_importer.hpp"

#include <deal.II/grid/grid_out.h>

#include <string>

namespace lifex::utils
{
  /**
   * @brief Preprocess and serialize VTK data.
   *
   * This app reads data from VTK file and allows serializing the data vectors
   * it defines, for use in later simulations.
   */
  class VTKPreprocess : public CoreModel
  {
  public:
    /// Constructor.
    VTKPreprocess(const std::string &subsection)
      : CoreModel(subsection)
      , triangulation(prm_subsection_path, mpi_comm)
      , vtk_importer(prm_subsection_path + " / VTK processing", {"Input data"})
    {}

    /// Run VTK preprocess.
    virtual void
    run() override
    {
      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path + " / Read mesh");

        pcout << "Create the mesh..." << std::endl;
        triangulation.create_mesh();
      }

      unsigned int dimension = prm_vtk_data_is_vectorial ? dim : 1;

      std::unique_ptr<FiniteElement<dim>> fe_scalar;

      if (prm_fe_degree > 0)
        {
          fe_scalar = triangulation.get_fe_lagrange(prm_fe_degree);
        }
      else // if (prm_fe_degree == 0)
        {
          fe_scalar = triangulation.get_fe_dg(prm_fe_degree);
        }

      auto fe = std::make_unique<FESystem<dim>>(*fe_scalar, dimension);

      DoFHandler<dim> dof_handler;
      dof_handler.reinit(triangulation.get());
      dof_handler.distribute_dofs(*fe);

      // Define FE vector.
      LinAlg::MPI::Vector fe_vector;       // FE vector.
      LinAlg::MPI::Vector fe_vector_owned; // FE vector, without ghost entries.
      const IndexSet      owned_dofs = dof_handler.locally_owned_dofs();
      const IndexSet      relevant_dofs =
        DoFTools::extract_locally_relevant_dofs(dof_handler);
      fe_vector.reinit(owned_dofs, relevant_dofs, mpi_comm);
      fe_vector_owned.reinit(owned_dofs, mpi_comm);

      // Interpolate VTK file and store the results.
      std::unique_ptr<VTKFunction> vtk_interpolant;
      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Read and interpolate VTK file");

        pcout << "Reading and interpolating VTK file..." << std::endl;

        vtk_importer.initialize(triangulation, dof_handler);
        fe_vector_owned = vtk_importer.get_owned("Input data");
        fe_vector       = fe_vector_owned;
      }

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Store results");

        pcout << "Saving results..." << std::endl;

        DataOut<dim> data_out;

        const std::vector<std::string> vector_names(
          prm_vtk_data_is_vectorial ? dim : 1, "processed_vector");

        const std::vector<
          DataComponentInterpretation::DataComponentInterpretation>
          data_component_interpretation(
            prm_vtk_data_is_vectorial ? dim : 1,
            prm_vtk_data_is_vectorial ?
              DataComponentInterpretation::component_is_part_of_vector :
              DataComponentInterpretation::component_is_scalar);

        data_out.add_data_vector(dof_handler,
                                 fe_vector,
                                 vector_names,
                                 data_component_interpretation);
        data_out.build_patches();
        data_out.write_vtu_in_parallel(prm_output_directory +
                                         prm_output_filename + ".vtu",
                                       mpi_comm);
        data_out.clear();
      }

      if (prm_VTK_move_mesh)
        {
          AssertThrow(
            prm_vtk_data_is_vectorial &&
              vtk_importer.get_array_data_type() == VTKArrayDataType::PointData,
            ExcMessage(
              "move_mesh can only be used with a vectorial PointData."));

          TimerOutput::Scope timer_section(
            timer_output,
            prm_subsection_path + " / Move mesh and store .msh file");

          const MPI_Comm mpi_comm_serial = MPI_COMM_SELF;

          // Load the same triangulation as the original mesh, but in serial.
          utils::MeshHandler<dim> triangulation_serial(prm_subsection_path,
                                                       mpi_comm_serial);

          triangulation_serial.initialize_from_file(
            triangulation.get_filename(), triangulation.get_scaling_factor());
          triangulation_serial.set_element_type(
            triangulation.get_element_type());

          triangulation_serial.set_refinement_from_file(prm_output_directory +
                                                        prm_output_filename);
          triangulation_serial.create_mesh(true);

          if (mpi_rank == 0)
            {
              // Create dof_handler.
              DoFHandler<dim> dof_handler_serial(triangulation_serial.get());
              dof_handler_serial.distribute_dofs(*fe);

              LinAlg::MPI::Vector d_serial;

              IndexSet owned_dofs_serial =
                dof_handler_serial.locally_owned_dofs();

              // Deserialize d_serial.
              d_serial.reinit(owned_dofs_serial, mpi_comm_serial);

              utils::deserialize(prm_output_filename,
                                 d_serial,
                                 triangulation_serial,
                                 dof_handler_serial);

              // Move initial mesh with d_serial and save it.
              utils::move_mesh(triangulation_serial,
                               dof_handler_serial,
                               d_serial);

              GridOut           grid_out;
              GridOutFlags::Msh msh_flags(true, true);
              grid_out.set_flags(msh_flags);

              const std::string filename_msh =
                prm_output_directory + prm_output_filename + ".msh";

              std::ofstream output_mesh(filename_msh);
              grid_out.write_msh(triangulation_serial.get(), output_mesh);
            }

          MPI_Barrier(mpi_comm);
        }
    }

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      // Declare parameters.
      triangulation.declare_parameters(params);

      params.enter_subsection_path(prm_subsection_path);
      params.enter_subsection("Mesh and space discretization");
      {
        params.declare_entry("FE space degree",
                             "1",
                             Patterns::Integer(0),
                             "Degree of the FE space.");
      }
      params.leave_subsection();

      params.enter_subsection("VTK processing");
      {
        params.declare_entry("VTK data is vectorial",
                             "false",
                             Patterns::Bool());

        params.declare_entry("VTK move mesh",
                             "false",
                             Patterns::Bool(),
                             "VTK move mesh.");

        params.declare_entry("Output filename",
                             "",
                             Patterns::FileName(
                               Patterns::FileName::FileType::input));
      }
      params.leave_subsection();
      params.leave_subsection_path();

      vtk_importer.declare_parameters(params);
    }

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      // Parse input file.
      params.parse();

      // Read input parameters.
      triangulation.parse_parameters(params);

      params.enter_subsection_path(prm_subsection_path);
      params.enter_subsection("Mesh and space discretization");
      {
        prm_fe_degree = params.get_integer("FE space degree");
      }
      params.leave_subsection();

      params.enter_subsection("VTK processing");
      {
        prm_vtk_data_is_vectorial = params.get_bool("VTK data is vectorial");
        prm_VTK_move_mesh         = params.get_bool("VTK move mesh");
        prm_output_filename       = params.get("Output filename");
      }
      params.leave_subsection();
      params.leave_subsection_path();

      vtk_importer.parse_parameters(params);
    }

  private:
    /// Mesh.
    MeshHandler<dim> triangulation;

    /// Importer for the VTK data.
    VTKImporter vtk_importer;

    /// @name Parameters read from file.
    /// @{

    /// Finite element degree.
    unsigned int prm_fe_degree;

    /// Toggle between scalar and vectorial imported data. Vectorial data is
    /// assumed to have as many components as there are physical dimensions.
    bool prm_vtk_data_is_vectorial;

    /// Output filename.
    std::string prm_output_filename;

    /// Toggle warping the mesh by the imported vector.
    bool prm_VTK_move_mesh;

    /// @}
  };

} // namespace lifex::utils

/// Pre-processing of a generic VTK file.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::utils::VTKPreprocess app("VTK preprocess");

      app.main_run_generate();
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
