/********************************************************************************
  Copyright (C) 2019 - 2023 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

/// @page appimage Building portable executables
///
/// @lifex applications can be packaged into portable binary executables. This
/// is achieved by building an [AppImage
/// package](https://en.wikipedia.org/wiki/AppImage) through
/// [Linux Deploy](https://github.com/meefik/linuxdeploy), following the steps
/// described in this page.
///
/// > **Important**: in order for the executable to be portable, @lifex
/// > dependencies should be installed using `lifex-env`. We recommend using the
/// > Docker environment to this end (see @ref download-and-install).
///
/// ## 1. Run the building script.
/// From within the build directory, run
/// @code{.sh}
/// cmake .. -DCMAKE_CONFIGURE_APPIMAGE=ON -DCMAKE_INSTALL_PREFIX=/usr
/// .AppImage/build.sh <app name> <comment>
/// @endcode
/// where
/// - `<app name>` is the name of the app that should be packaged (such as
/// `vtk_preprocess`);
/// - `<comment>` is a comment string that will be used in the description of
/// the generated binary package.
///
/// > **Warning**: the generated package will contain all the data present in
/// > the folder `AppDir`. If other binaries were previously installed to
/// > that folder (e.g. by running the above command multiple times), the
/// > package will contain those binaries as well, unless the folder is cleared
/// > in advance. It is advisable to install in an empty folder, to avoid
/// > including unwanted files.
///
/// ## 2. Share the binary package!
/// You will obtain a file named `lifex_<app name>-<version>-x86_64.AppImage`,
/// that bundles binary files for the desired @lifex app and its dependencies,
/// and can be copied and executed on other systems.
///
/// The binary package relies on the userspace filesystem framework
/// [FUSE](https://www.kernel.org/doc/html/latest/filesystems/fuse.html),
/// which is assumed to be installed on the target system. In case of issues,
/// the contents of the package can be extracted by running
/// @code{.sh}
/// lifex_<app name>-<version>-x86_64.AppImage --appimage-extract
/// @endcode
///
