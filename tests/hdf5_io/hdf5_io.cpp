/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/core_model.hpp"
#include "source/init.hpp"

#include "source/geometry/mesh_handler.hpp"

#include "source/io/hdf5_io.hpp"

#include "source/numerics/numbers.hpp"
#include "source/numerics/tools.hpp"

namespace lifex::tests
{
  /// Test class for HDF5 I/O.
  class TestHDF5IO : public CoreModel
  {
  public:
    /// Constructor.
    TestHDF5IO(const std::string &subsection)
      : CoreModel(subsection)
      , triangulation("Dummy", mpi_comm)
    {}

    /// Declare parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.enter_subsection_path(prm_subsection_path);
      params.declare_entry("Number of refinements", "5", Patterns::Integer(0));
      params.declare_entry("FE space degree", "1", Patterns::Integer(1));
      params.leave_subsection_path();
    }

    /// Parse parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      params.parse();

      params.enter_subsection_path(prm_subsection_path);
      prm_n_refinements = params.get_integer("Number of refinements");
      prm_fe_degree     = params.get_integer("FE space degree");
      params.leave_subsection_path();
    }

    /// Run the test.
    virtual void
    run() override
    {
      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Create mesh");

        pcout << "Create mesh" << std::endl;

        triangulation.initialize_hypercube(0, 1.0, true);
        triangulation.set_refinement_global(prm_n_refinements);
        triangulation.create_mesh();
      }

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Distribute DoFs");

        pcout << "Distribute DoFs" << std::endl;

        const std::unique_ptr<FiniteElement<dim>> fe =
          triangulation.get_fe_lagrange(prm_fe_degree);
        dof_handler.reinit(triangulation.get());
        dof_handler.distribute_dofs(*fe);

        owned_dofs    = dof_handler.locally_owned_dofs();
        relevant_dofs = DoFTools::extract_locally_relevant_dofs(dof_handler);
      }

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Initialize matrix");

        pcout << "Initialize matrix" << std::endl;

        DynamicSparsityPattern dsp(relevant_dofs);
        DoFTools::make_sparsity_pattern(dof_handler,
                                        dsp,
                                        AffineConstraints<double>(),
                                        false);
        SparsityTools::distribute_sparsity_pattern(dsp,
                                                   owned_dofs,
                                                   mpi_comm,
                                                   relevant_dofs);

        utils::initialize_matrix(matrix, owned_dofs, dsp);

        // Assemble a mass matrix just as an example.
        {
          const auto &fe  = dof_handler.get_fe();
          auto quadrature = triangulation.get_quadrature_gauss(fe.degree + 1);

          FEValues<dim> fe_values(fe,
                                  *quadrature,
                                  update_values | update_JxW_values);

          const unsigned int                   dofs_per_cell = fe.dofs_per_cell;
          std::vector<types::global_dof_index> dof_indices(dofs_per_cell);
          FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);

          for (const auto &cell : dof_handler.active_cell_iterators())
            {
              if (!cell->is_locally_owned())
                continue;

              cell_matrix = 0.0;
              fe_values.reinit(cell);
              cell->get_dof_indices(dof_indices);

              for (unsigned int i = 0; i < dofs_per_cell; ++i)
                for (unsigned int j = 0; j < dofs_per_cell; ++j)
                  for (unsigned int q = 0; q < quadrature->size(); ++q)
                    cell_matrix(i, j) += fe_values.shape_value(i, q) *
                                         fe_values.shape_value(j, q) *
                                         fe_values.JxW(q);

              matrix.add(dof_indices, cell_matrix);
            }
        }

        matrix.compress(VectorOperation::add);
      }

      const double l1_norm_before        = matrix.l1_norm();
      const double linfty_norm_before    = matrix.linfty_norm();
      const double frobenius_norm_before = matrix.frobenius_norm();
      pcout << "Norms before serialization (L1, Linfty, Frobenius): "
            << l1_norm_before << ", " << linfty_norm_before << ", "
            << frobenius_norm_before << std::endl;

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Serialize matrix");

        HDF5::File file(prm_output_directory + "/matrix.h5",
                        HDF5::File::FileAccessMode::create,
                        mpi_comm);

        HDF5::Group group = file.create_group("matrix");
        utils::hdf5::write_matrix(group, matrix);
      }

      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Deserialize matrix");

        HDF5::File file(prm_output_directory + "/matrix.h5",
                        HDF5::File::FileAccessMode::open,
                        mpi_comm);

        HDF5::Group group = file.open_group("matrix");

        utils::hdf5::read_matrix(
          group, matrix, owned_dofs, relevant_dofs, owned_dofs, mpi_comm);
      }

      const double l1_norm_after        = matrix.l1_norm();
      const double linfty_norm_after    = matrix.linfty_norm();
      const double frobenius_norm_after = matrix.frobenius_norm();
      pcout << "Norms after serialization (L1, Linfty, Frobenius): "
            << l1_norm_after << ", " << linfty_norm_after << ", "
            << frobenius_norm_after << std::endl;

      AssertThrow(
        utils::is_equal(frobenius_norm_before, frobenius_norm_after) &&
          utils::is_equal(linfty_norm_before, linfty_norm_after) &&
          utils::is_equal(l1_norm_before, l1_norm_after),
        ExcMessage(
          "Matrix norms before and after serialization are not equal."));
    }

  protected:
    /// Test triangulation.
    utils::MeshHandler<dim> triangulation;

    /// Test DoF handler.
    DoFHandler<dim> dof_handler;

    /// Owned DoF indices.
    IndexSet owned_dofs;

    /// Relevant DoF indices.
    IndexSet relevant_dofs;

    /// Test matrix.
    LinAlg::MPI::SparseMatrix matrix;

    /// @name Parameters read from file.
    /// @{

    /// Number of mesh refinements.
    unsigned int prm_n_refinements;

    /// FE degree.
    unsigned int prm_fe_degree;

    /// @}
  };
}; // namespace lifex::tests

/// Run HDF5 I/O test.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::tests::TestHDF5IO test("Test HDF5 IO");
      test.main_run_generate();
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
