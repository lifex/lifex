/********************************************************************************
  Copyright (C) 2021 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>
 */

#include "source/init.hpp"

#include "source/geometry/finders.hpp"
#include "source/geometry/mesh_handler.hpp"

#include "source/numerics/numbers.hpp"

/// Namespace for all the tests in @lifex.
namespace lifex::tests
{
  /**
   * @brief Auxiliary class to test the classes @ref utils::DoFLocator and
   * @ref utils::BoundaryDoFLocator.
   */
  class TestDoFLocator : public Core
  {
  public:
    /// Class constructor.
    TestDoFLocator(const std::string &subsection);

    /// Run test.
    void
    run();

  protected:
    /// Triangulation.
    utils::MeshHandler<dim> triangulation; ///< Triangulation.

    /// DoF handler.
    DoFHandler<dim> dof_handler;

    /// Finite element space.
    std::unique_ptr<FiniteElement<dim>> fe;

    /// Domain size.
    static constexpr double domain_size = 1.0;
  };

  TestDoFLocator::TestDoFLocator(const std::string &subsection)
    : triangulation(subsection, mpi_comm)
  {}

  void
  TestDoFLocator::run()
  {
    triangulation.initialize_hypercube(0, domain_size, true);
    triangulation.set_refinement_global(3);
    triangulation.create_mesh();

    fe = triangulation.get_fe_lagrange(1);

    dof_handler.reinit(triangulation.get());
    dof_handler.distribute_dofs(*fe);

    {
      // We find the closest DoF to a point outside of the domain. This should
      // yield the top corner (i.e. (1, 1, 1) in 3D).
      pcout << "Testing DoFLocator with RTree" << std::endl;

      utils::DoFLocator dof_locator(dof_handler);

      Point<dim> point_to_search;
      Point<dim> expected_point;
      for (unsigned int i = 0; i < dim; ++i)
        {
          point_to_search[i] = domain_size * 2;
          expected_point[i]  = domain_size;
        }

      const auto [index, owner] = dof_locator.find(point_to_search);

      Point<dim> found_point;
      if (mpi_rank == owner)
        found_point = dof_locator.support_point(index);
      found_point = Utilities::MPI::broadcast(mpi_comm, found_point, owner);

      const double dist = found_point.distance(expected_point);

      pcout << "Distance between expected and found point: " << dist
            << std::endl
            << std::endl;

      AssertThrow(
        utils::is_zero(dist),
        ExcMessage(
          "The distance between the expected and found point is not zero."));
    }

    {
      // We repeat the previous but disabling the R-tree.
      pcout << "Testing DoFLocator without RTree" << std::endl;

      utils::DoFLocator dof_locator(dof_handler, 0, false);

      Point<dim> point_to_search;
      Point<dim> expected_point;
      for (unsigned int i = 0; i < dim; ++i)
        {
          point_to_search[i] = domain_size * 2;
          expected_point[i]  = domain_size;
        }

      const auto [index, owner] = dof_locator.find(point_to_search);

      Point<dim> found_point;
      if (mpi_rank == owner)
        found_point = dof_locator.support_point(index);
      found_point = Utilities::MPI::broadcast(mpi_comm, found_point, owner);

      const double dist = found_point.distance(expected_point);

      pcout << "Distance between expected and found point: " << dist
            << std::endl
            << std::endl;

      AssertThrow(
        utils::is_zero(dist),
        ExcMessage(
          "The distance between the expected and found point is not zero."));
    }


    {
      // We find the closest boundary DoF to a point inside of the domain. This
      // should yield the centerpoint of one side (i.e. (0, L/2, L/2) in 3D).
      pcout << "Testing BoundaryDoFLocator with RTree" << std::endl;

      utils::BoundaryDoFLocator dof_locator(dof_handler, {0});

      Point<dim> point_to_search;
      Point<dim> expected_point;

      point_to_search[0] =
        0.5 * domain_size + triangulation.get_info().get_diameter_max();
      expected_point[0] = 0.0;

      for (unsigned int i = 1; i < dim; ++i)
        {
          point_to_search[i] = 0.5 * domain_size;
          expected_point[i]  = 0.5 * domain_size;
        }

      const auto [index, owner] = dof_locator.find(point_to_search);

      Point<dim> found_point;
      if (mpi_rank == owner)
        found_point = dof_locator.support_point(index);
      found_point = Utilities::MPI::broadcast(mpi_comm, found_point, owner);

      const double dist = found_point.distance(expected_point);

      pcout << "Distance between expected and found point: " << dist
            << std::endl;

      AssertThrow(
        utils::is_zero(dist),
        ExcMessage(
          "The distance between the expected and found point is not zero."));
    }
  }
} // namespace lifex::tests

/// Test for serialization.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::tests::TestDoFLocator test("Test DoF locator");

      test.run();
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
