/********************************************************************************
  Copyright (C) 2019 - 2024 by the lifex authors.

  This file is part of lifex.

  lifex is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  lifex is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with lifex.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************************/

/**
 * @file
 *
 * @author Michele Bucelli <michele.bucelli@polimi.it>.
 */

#include "source/core_model.hpp"
#include "source/init.hpp"
#include "source/quadrature_evaluation.hpp"

#include "source/geometry/mesh_handler.hpp"

#include "source/io/data_writer.hpp"

#include "source/numerics/rbf_interpolation.hpp"

namespace lifex::tests
{
  /**
   * @brief Test for RBF interpolation.
   *
   * This test performs RBF interpolation on a cubic domain between a
   * structured, hexahedral mesh (the source mesh) and an unstructured,
   * tetrahedral mesh (the destination mesh). It first evaluates the function
   * @f$f(x) = \sin(10\pi x)\sin(20\pi y)\sin(30\pi x)@f$ on the nodes of the
   * source mesh, then computes the RBF interpolant
   * with @ref utils::RBFInterpolation on the destination mesh.
   *
   * Denoting by @f$\mathbf{x}_i@f$ the vertices of the destination mesh, the
   * test computes the following error:
   * @f[
   * e_{\infty} = \frac{\max_i|f(\mathbf{x}_i) -
   * f_i|}{\max_i|f(\mathbf{x}_i)|}\;,
   * @f]
   * where @f$f_i@f$ is the evaluation of the RBF interpolant at
   * @f$\mathbf{x}_i@f$. The test passes if @f$e_{\infty} < \epsilon@f$, where
   * @f$\epsilon@f$ is a user-specified threshold, which defaults to @f$\epsilon
   * = 0.05@f$.
   */
  class TestRBFInterpolation : public CoreModel
  {
  public:
    /// Interpolated field.
    static double
    interpolated_field(const Point<dim> &p)
    {
      double result = 0.0;

      for (unsigned int i = 0; i < dim; ++i)
        result += std::sin(10 * (i + 1) * M_PI * p[i]);

      return result;
    }

    /// Interpolated field wrapped as a Function.
    class InterpolatedScalar : public Function<dim>
    {
    public:
      /// Constructor.
      InterpolatedScalar() = default;

      /// Evaluation.
      virtual double
      value(const Point<dim> &p, unsigned int /*component*/) const override
      {
        return interpolated_field(p);
      }
    };

    /// Interpolated field wrapped as a QuadratureEvaluation.
    class InterpolatedScalarQuadrature : public QuadratureEvaluationFEMScalar
    {
    public:
      /// Constructor.
      InterpolatedScalarQuadrature(const DoFHandler<dim> &dof_handler,
                                   const Quadrature<dim> &quadrature)
        : QuadratureEvaluationFEMScalar(dof_handler,
                                        quadrature,
                                        update_quadrature_points)
      {}

      /// Evaluation.
      virtual double
      operator()(const unsigned int &q,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        return interpolated_field(fe_values->quadrature_point(q));
      }

    protected:
    };

    /// RBF interpolation on quadrature points.
    class RBFInterpolationQuadrature
      : public utils::RBFInterpolationQuadrature<double>
    {
    public:
      /// Constructor.
      RBFInterpolationQuadrature(const std::string &subsection)
        : utils::RBFInterpolationQuadrature<double>(subsection)
      {}

      virtual double
      operator()(const unsigned int &q,
                 const double & /*t*/       = 0,
                 const Point<dim> & /*x_q*/ = Point<dim>()) override
      {
        return interpolated_vector[0][offset_dst + cell_offset + q];
      }

    protected:
    };

    /// Constructor.
    TestRBFInterpolation(const std::string &subsection)
      : CoreModel(subsection)
      , mesh_src(std::make_shared<utils::MeshHandler<dim>>(
          subsection + " / Source",
          mpi_comm,
          std::initializer_list<utils::mesh::GeometryType>(
            {utils::mesh::GeometryType::File,
             utils::mesh::GeometryType::Hypercube})))
      , mesh_dst(std::make_shared<utils::MeshHandler<dim>>(
          subsection + " / Destination",
          mpi_comm,
          std::initializer_list<utils::mesh::GeometryType>(
            {utils::mesh::GeometryType::File,
             utils::mesh::GeometryType::Hypercube})))
      , rbf_interpolation_dofs(prm_subsection_path + " / RBF interpolation")
      , rbf_interpolation_quadrature(prm_subsection_path +
                                     " / RBF interpolation")
      , output_handler_src(prm_subsection_path + " / Source / Output",
                           /* enable_time_dependent = */ false,
                           /* default_filename = */ "data_src")
      , output_handler_dst(prm_subsection_path + " / Destination / Output",
                           /* enable_time_dependent = */ false,
                           /* default_filename = */ "data_dst")
    {}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override
    {
      params.enter_subsection_path(prm_subsection_path);
      {
        params.declare_entry_selection("Interpolation points",
                                       "DoFs",
                                       "DoFs | Quadrature points",
                                       "Choose whether to interpolate between "
                                       "DoFs or between quadrature points.");

        params.declare_entry("Interpolation error tolerance",
                             "5e-2",
                             Patterns::Double(0),
                             "The test passes if the interpolation error falls "
                             "below this tolerance.");

        params.enter_subsection("Source");
        params.enter_subsection("Mesh and space discretization");
        params.declare_entry("FE space degree",
                             "1",
                             Patterns::Integer(1),
                             "Finite element degree used on the source mesh.");
        params.leave_subsection();
        params.leave_subsection();

        params.enter_subsection("Destination");
        params.enter_subsection("Mesh and space discretization");
        params.declare_entry(
          "FE space degree",
          "1",
          Patterns::Integer(1),
          "Finite element degree used on the destination mesh.");
        params.leave_subsection();
        params.leave_subsection();
      }
      params.leave_subsection_path();

      mesh_src->declare_parameters(params);
      mesh_dst->declare_parameters(params);
      rbf_interpolation_dofs.declare_parameters(params);
      rbf_interpolation_quadrature.declare_parameters(params);
      output_handler_src.declare_parameters(params);
      output_handler_dst.declare_parameters(params);
    }

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override
    {
      params.parse();

      params.enter_subsection_path(prm_subsection_path);
      {
        prm_interpolation_points = params.get("Interpolation points");

        prm_interpolation_tolerance =
          params.get_double("Interpolation error tolerance");

        params.enter_subsection("Source");
        params.enter_subsection("Mesh and space discretization");
        prm_degree_src = params.get_integer("FE space degree");
        params.leave_subsection();
        params.leave_subsection();

        params.enter_subsection("Destination");
        params.enter_subsection("Mesh and space discretization");
        prm_degree_dst = params.get_integer("FE space degree");
        params.leave_subsection();
        params.leave_subsection();
      }
      params.leave_subsection_path();

      mesh_src->parse_parameters(params);
      mesh_dst->parse_parameters(params);
      rbf_interpolation_dofs.parse_parameters(params);
      rbf_interpolation_quadrature.parse_parameters(params);
      output_handler_src.parse_parameters(params);
      output_handler_dst.parse_parameters(params);
    }

    /// Run the example.
    virtual void
    run() override
    {
      // Create meshes.
      {
        TimerOutput::Scope timer_section(timer_output,
                                         prm_subsection_path +
                                           " / Create mesh");

        mesh_src->create_mesh();
        mesh_dst->create_mesh();
      }

      // Initialize the DoF handlers.
      {
        fe_src = mesh_src->get_fe_lagrange(prm_degree_src);
        dof_handler_src.reinit(mesh_src->get());
        dof_handler_src.distribute_dofs(*fe_src);

        mesh_src->get_info().print("Source mesh",
                                   dof_handler_src.n_dofs(),
                                   false);

        fe_dst = mesh_dst->get_fe_lagrange(prm_degree_dst);
        dof_handler_dst.reinit(mesh_dst->get());
        dof_handler_dst.distribute_dofs(*fe_dst);

        mesh_dst->get_info().print("Destination mesh",
                                   dof_handler_dst.n_dofs(),
                                   false);
      }

      if (prm_interpolation_points == "Quadrature points")
        {
          quadrature_src = mesh_src->get_quadrature_gauss(2);
          quadrature_dst = mesh_dst->get_quadrature_gauss(prm_degree_dst + 1);
        }

      // Initialize vectors to store data on the two meshes.
      {
        IndexSet owned_dofs_src = dof_handler_src.locally_owned_dofs();
        IndexSet relevant_dofs_src;
        DoFTools::extract_locally_relevant_dofs(dof_handler_src,
                                                relevant_dofs_src);
        data_src_owned.reinit(owned_dofs_src, mpi_comm);
        data_src.reinit(owned_dofs_src, relevant_dofs_src, mpi_comm);

        IndexSet owned_dofs_dst = dof_handler_dst.locally_owned_dofs();
        IndexSet relevant_dofs_dst;
        DoFTools::extract_locally_relevant_dofs(dof_handler_dst,
                                                relevant_dofs_dst);
        data_dst_owned.reinit(owned_dofs_dst, mpi_comm);
        data_dst.reinit(owned_dofs_dst, relevant_dofs_dst, mpi_comm);
      }

      // Interpolate a function onto the fine mesh and write it to file.
      VectorTools::interpolate(dof_handler_src,
                               InterpolatedScalar(),
                               data_src_owned);
      data_src = data_src_owned;

      if (output_handler_src.is_active())
        {
          TimerOutput::Scope timer_section(timer_output,
                                           prm_subsection_path + " / Output");

          output_handler_src.add_data_vector(dof_handler_src,
                                             data_src,
                                             "data_src");
          output_handler_src.write();
        }

      run_interpolation();

      if (prm_interpolation_points == "DoFs" && output_handler_dst.is_active())
        {
          TimerOutput::Scope timer_section(timer_output,
                                           prm_subsection_path + " / Output");

          output_handler_dst.add_data_vector(dof_handler_dst,
                                             data_dst,
                                             "data_dst");
          output_handler_dst.write();
        }

      compute_error();
    }

  protected:
    /// Run the interpolation.
    void
    run_interpolation()
    {
      if (prm_interpolation_points == "DoFs")
        {
          rbf_interpolation_dofs.set_geodesic_reference_mesh(mesh_src);
          rbf_interpolation_dofs.setup_system(dof_handler_src, dof_handler_dst);

          pcout << utils::log::separator_section << std::endl;

          pcout << "RBF interpolation:" << std::flush;
          rbf_interpolation_dofs.interpolate(data_dst_owned, data_src_owned);
          data_dst = data_dst_owned;
        }
      else // if (prm_interpolation_points == "Quadrature points")
        {
          rbf_interpolation_quadrature.set_geodesic_reference_mesh(mesh_src);
          rbf_interpolation_quadrature.setup_system(1,
                                                    dof_handler_src,
                                                    *quadrature_src,
                                                    dof_handler_dst,
                                                    *quadrature_dst);

          pcout << utils::log::separator_section << std::endl;

          pcout << "RBF interpolation:" << std::flush;
          InterpolatedScalarQuadrature quadrature_eval(dof_handler_src,
                                                       *quadrature_src);
          rbf_interpolation_quadrature.interpolate(quadrature_eval);
        }
    }

    /// Compute the interpolation error against the original function.
    void
    compute_error()
    {
      double error     = 0.0;
      double reference = 0.0;

      auto process_point = [&error, &reference](const Point<dim> &p,
                                                const double     &val_interp) {
        const double val_exact = interpolated_field(p);

        error     = std::max(error, std::abs(val_exact - val_interp));
        reference = std::max(reference, std::abs(val_exact));
      };

      if (prm_interpolation_points == "DoFs")
        {
          auto mapping = mesh_dst->get_linear_mapping();
          std::map<types::global_dof_index, Point<dim>> dst_points;
          DoFTools::map_dofs_to_support_points(*mapping,
                                               dof_handler_dst,
                                               dst_points);

          for (const auto &[i, p] : dst_points)
            {
              if (!dof_handler_dst.locally_owned_dofs().is_element(i))
                continue;

              process_point(p, data_dst[i]);
            }
        }
      else // if (prm_interpolation_points == "Quadrature")
        {
          FEValues<dim> fe_values(*fe_dst,
                                  *quadrature_dst,
                                  update_quadrature_points);

          rbf_interpolation_quadrature.init();

          for (const auto &cell : dof_handler_dst.active_cell_iterators())
            {
              if (!cell->is_locally_owned())
                continue;

              fe_values.reinit(cell);
              rbf_interpolation_quadrature.reinit(cell);

              for (unsigned int q = 0; q < quadrature_dst->size(); ++q)
                process_point(fe_values.quadrature_point(q),
                              rbf_interpolation_quadrature(q));
            }
        }

      error     = Utilities::MPI::max(error, mpi_comm);
      reference = Utilities::MPI::max(reference, mpi_comm);

      error = error / reference;

      pcout << "Error = " << error << std::endl;

      AssertThrow(error < prm_interpolation_tolerance,
                  ExcMessage(
                    "The interpolation error is greater than the tolerance."));
    }

    /// Source mesh.
    std::shared_ptr<utils::MeshHandler<dim>> mesh_src;

    /// Source DoF handler.
    DoFHandler<dim> dof_handler_src;

    /// Finite element space on the source mesh.
    std::unique_ptr<FiniteElement<dim>> fe_src;

    /// Data vector on the source mesh, without ghost elements.
    LinAlg::MPI::Vector data_src_owned;

    /// Data vector on the source mesh, including ghost elements.
    LinAlg::MPI::Vector data_src;

    /// Destination mesh.
    std::shared_ptr<utils::MeshHandler<dim>> mesh_dst;

    /// Destination DoF handler.
    DoFHandler<dim> dof_handler_dst;

    /// Finite element space on the destination mesh.
    std::unique_ptr<FiniteElement<dim>> fe_dst;

    /// Data vector on the destination mesh, without ghost elements.
    LinAlg::MPI::Vector data_dst_owned;

    /// Data vector on the source mesh, including ghost elements.
    LinAlg::MPI::Vector data_dst;

    /// RBF interpolation on DoFs.
    utils::RBFInterpolationDoFs rbf_interpolation_dofs;

    /// RBF interpolation on quadrature points.
    RBFInterpolationQuadrature rbf_interpolation_quadrature;

    /// Quadrature formula on the source mesh (for quadrature-based
    /// interpolation).
    std::unique_ptr<Quadrature<dim>> quadrature_src;

    /// Quadrature formula on the destination mesh (for quadrature-based
    /// interpolation).
    std::unique_ptr<Quadrature<dim>> quadrature_dst;

    /// Output handler for source data.
    utils::OutputHandler output_handler_src;

    /// Output handler for destination data.
    utils::OutputHandler output_handler_dst;

    /// @name Parameters read from file.
    /// @{

    /// Type of interpolation points (DoFs or quadrature).
    std::string prm_interpolation_points;

    /// Tolerance on the interpolation error. The test passes if the error falls
    /// below this value.
    double prm_interpolation_tolerance;

    /// Finite element degree used on the source mesh.
    unsigned int prm_degree_src;

    /// Finite element degree used on the destination mesh.
    unsigned int prm_degree_dst;

    /// @}
  };
} // namespace lifex::tests


/// Example RBF interpolation.
int
main(int argc, char **argv)
{
  lifex::lifex_init lifex_initializer(argc, argv, 1);

  try
    {
      lifex::tests::TestRBFInterpolation test("Test RBF interpolation");
      test.main_run_generate([&test]() {
        test.generate_parameters_from_json({"common", "constant_support"},
                                           "constant_support");
        test.generate_parameters_from_json({"common",
                                            "constant_support_deserialize"},
                                           "constant_support_deserialize");

        test.generate_parameters_from_json({"common",
                                            "constant_support_quadrature"},
                                           "constant_support_quadrature");

        test.generate_parameters_from_json({"common", "adaptive_support"},
                                           "adaptive_support");

        test.generate_parameters_from_json({"common", "geodesic_thresholding"},
                                           "geodesic_thresholding");
      });
    }
  LIFEX_CATCH_EXC();

  return EXIT_SUCCESS;
}
